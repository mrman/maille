#!/usr/bin/env node
/* global require, module */

const { parse, stringify } = require("svgson");
const fs = require("fs-extra");
const download = require("download");
const process = require("process");
const program = require("commander");
const decamelize = require("decamelize");

const SCRIPT_VERSION = "0.0.1";

// Set up program
program
  .version(SCRIPT_VERSION)
  .option("-c, --icon-class-name <name>", "Icon Typescript class name")
  .option("-i, --input <path>", "Input SVG file path")
  .option("-o, --output <path>", "Output Typescript file path")
  .option("-f, --force", "Force generation")
  .parse(process.argv);

/**
 * Generate the TS file that represents the icon
 *
 * @param {string} dPath - the path details
 * @returns {string}
 */
function generateTSFileForIcon(options) {
  const {svgson, iconClassName} = options;
  const iconClassNameDashified = decamelize(iconClassName, "-");

  if (!iconClassName) { throw new Error("iconClassName not provided"); }
  if (!svgson) { throw new Error("svgson not provided"); }

  const svgsonAttributes = Object.assign({}, svgson.attributes);
  if (options && options.svgsonOverrides) {
    Object.assign(svgsonAttributes, options.svgsonOverrides);
  }

  return `// This file was built automatically via the convert-ux-wing-svg.js script (v${SCRIPT_VERSION})
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = ${JSON.stringify(svgsonAttributes)};

export default class ${iconClassName} implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('${stringify(svgson.children)}'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon ${iconClassNameDashified}";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
`;
}

// Get the file either from the network or locally with simple prefix check
const getFile = (path) => {
  if (path.startsWith("http")) { return download(path); }
  return fs.readFile(path);
};

// Convert a UXWing SVG from (from local or the internet) to a TS file that can be used
function convertUXWingSVG(opts) {
  const {svgFilePath, tsFilePath, iconClassName} = opts;

  // Skip if the file already exists locally
  fs
    .pathExists(tsFilePath)
    .then(exists => {
      if (exists && !opts.force) {
        console.log(`WARNING: Skipping existing file [${tsFilePath}]`);
        return Promise.resolve();
      }

      // Get the file
      return getFile(svgFilePath)
        .then(res => {
          console.log(`Successfully read SVG from path [${svgFilePath}]`);
          return res.toString();
        })
      // Parse the SVG into the SVGSON structure
        .then(parse)
        .then(svgson => {
          console.log("INFO: parsed SVG into JSON structure...");
          // Build the typescript for the given icon
          return generateTSFileForIcon({iconClassName, svgson, ...opts});
        })
      // Write the code out to the output file, if the file does not exist already
        .then(code => fs.writeFile(tsFilePath, code))
        .then(() => console.log(`SUCCESS: Successfully wrote out typescript code to file [${tsFilePath}]`));
    })
  // Handle errors
    .catch(err => console.log("ERROR:", err));
};

// Run the conversion if the file was run as a module
if (require.main === module) {
  const iconClassName = program.iconClassName;
  const svgFilePath = program.input;
  const tsFilePath = program.output;
  const force = program.force;

  // Ensure input SVG file is provided
  if (!svgFilePath) {
    console.error("ERROR: Invalid SVG file (please specify one with -i/--input)");
    process.exit(1);
  }

  // Ensure output file path is provided
  if (!tsFilePath) {
    console.error("ERROR: Invalid output file path (please specify one with -o/--output)");
    process.exit(1);
  }

  // Ensure output file path is provided
  if (!iconClassName) {
    console.error("ERROR: Invalid class name (please specify one with -c/--ts-class-name, ex. 'CheckMarkThin')");
    process.exit(1);
  }

  // Run a single conversion
  convertUXWingSVG({svgFilePath, tsFilePath, iconClassName, force});
}

module.exports = convertUXWingSVG;
