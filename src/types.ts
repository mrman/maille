export enum Size  {
  Mini = "mini",
  Small = "sm",
  Medium = "md",
  Large = "lg",
  XLarge = "xlg",
}

export enum ButtonType {
  Default = "default",
  Primary = "primary",
  Success = "success",
  Info    = "info",
  Warning = "warning",
  Error   = "error",
}

export enum TagType {
  Default = "default",
  Primary = "primary",
  Success = "success",
  Info    = "info",
  Warning = "warning",
  Error   = "error",
}

export enum AlertType {
  Default = "default",
  Primary = "primary",
  Success = "success",
  Info    = "info",
  Warning = "warning",
  Error   = "error",
}

// Interface for general mithril component tweaks/optimizations
export interface ComponentSettings {
  redrawChildrenOnly: boolean;
}

export type MithrilEvent = Event & {redraw: boolean};

export enum GeneralIconAttrName {
  Height = "height",
  Width = "width",
  Fill = "fill",
  Style = "style",
  ClassName = "className",
}

export type GeneralIconAttrs = {
  [key in GeneralIconAttrName]?: any;
};

export enum ToolTipActivation {
  Hover = "hover",
}

export enum ToolTipPosition {
  Top = "top",
  Bottom = "bottom",
  Left = "left",
  Right = "right",
}

export enum CardImagePosition {
  Top = "top",
  Bottom = "bottom",
  Left = "left",
  Right = "right",
}

export type CSSColor = string;
export type CSSSize = string;

export class CardImageOptions {
  public src: string;
  public position?: CardImagePosition = CardImagePosition.Top;
  public fullWidth?: boolean = true;

  constructor(opts: Partial<CardImageOptions>) {
    if (opts) {
      if (opts.src) { this.src = opts.src; }
      if (opts.position) { this.position = opts.position; }
      if (opts.fullWidth !== undefined) { this.fullWidth = opts.fullWidth; }
    }
  }
}

export enum KeyboardKeyCode {
  Enter = 13,
}

export const NoOpFn = () => undefined;

export type InputUpdateFn = (value: string, e: any) => any;
export type KeyupFn = (e: any) => any;
export type ButtonClickFn = (e: any) => any;
