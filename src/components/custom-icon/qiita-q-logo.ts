// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 130 130"};

export default class QiitaQLogo implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="m 0.00230174,61.702269 c -0.07386223,-8.333734 1.63072886,-16.587364 4.99999996,-24.21 3.2673212,-7.356129 7.9300433,-14.008913 13.7300003,-19.59 5.858746,-5.625223 12.738913,-10.0784931 20.27,-13.12 7.936367,-3.2270782 16.432922,-4.8516195 25,-4.78000005 8.516493,-0.05004169 16.959807,1.57406465 24.85,4.78000005 7.564301,3.0405304 14.480718,7.493102 20.379998,13.12 5.81937,5.573175 10.49658,12.227253 13.77,19.59 3.36927,7.622636 5.07386,15.876266 5,24.21 0.0288,7.341292 -1.32831,14.622057 -4,21.46 -2.58203,6.627485 -6.27118,12.768176 -10.91,18.160001 l 11.1,11.1 c 3.99157,4.01704 3.99157,10.50296 0,14.52 -4.02832,4.07734 -10.61168,4.07734 -14.64,0 l -12.219998,-12.41 c -4.94372,2.90286 -10.249753,5.13893 -15.78,6.65 -5.720955,1.54738 -11.623511,2.32093 -17.55,2.3 -16.846433,0.1243 -33.075568,-6.33434 -45.23,-18 C 12.960305,99.871934 8.2849196,93.193297 5.0023017,85.812269 1.6381448,78.223228 -0.0665367,70.003254 0.00230174,61.702269 Z m 21.64000026,0.08 c -0.09164,11.454179 4.376869,22.474347 12.42,30.63 3.847377,3.869177 8.408123,6.956085 13.43,9.090001 5.218442,2.22171 10.838481,3.34504 16.51,3.3 11.256282,0.057 22.064364,-4.40672 30,-12.390001 3.856684,-3.919575 6.913528,-8.552395 8.999998,-13.64 2.23007,-5.387677 3.35238,-11.16926 3.3,-17 0.0432,-5.829891 -1.0788,-11.60966 -3.3,-17 -4.160357,-10.271614 -12.238292,-18.46469 -22.449998,-22.77 -5.233326,-2.221153 -10.864908,-3.350873 -16.55,-3.32 -5.672918,-0.03872 -11.293013,1.091424 -16.51,3.32 -5.019363,2.134049 -9.579484,5.217139 -13.43,9.08 -8.037896,8.187594 -12.503866,19.226664 -12.42,30.7 z" id="path3771"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";

    const additionalClasses = "maille maille-uxwing-icon qiita-q";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
