export { default as QiitaQLogo } from "./qiita-q-logo";
export { default as MicrosoftLogo } from "./microsoft-logo";
export { default as GitlabLogo } from "./gitlab-logo";
