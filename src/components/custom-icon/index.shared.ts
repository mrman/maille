// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

export { default as QiitaQLogo } from "./qiita-q-logo";
export { default as MicrosoftLogo } from "./microsoft-logo";
export { default as GitlabLogo } from "./gitlab-logo";
