import m from "mithril";
import { ChildArray, Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { CSSColor, NoOpFn } from "../../types";

interface SwitchAttrs {
  className?: string;
  disabled?: boolean;
  on: boolean;
  onclick?: SwitchCallbackFn;
  outlined?: boolean;
}

type SwitchCallbackFn = (state: boolean) => void;

class Switch implements ClassComponent<SwitchAttrs> {
  protected on: boolean = false;

  constructor(vnode?: CVnode<SwitchAttrs>) {
    if (vnode) {
      if ("on" in vnode.attrs) { this.on = vnode.attrs.on; }
    }
  }

  public view(vnode: Vnode<SwitchAttrs>) {
    const disabled = vnode.attrs.disabled;
    const outlined = vnode.attrs.outlined;
    const onclick = vnode.attrs.onclick || NoOpFn;

    const classes: Set<string> = new Set(["maille", "maille-switch"]);

    // Add classnames from the vnode if present
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    // Add attribute controlled styling classes
    if (outlined) { classes.add("outlined"); }
    if (disabled) { classes.add("disabled"); }

    // Add class to represent state of switch
    if (this.on) { classes.add("on"); }

    const className = [...classes].join(" ");

    // Create onclick handler
    const wrappedOnClick = () => {
      // Return early if button is disabled
      if (disabled) { return; }

      this.on = !this.on;
      onclick(this.on);
    };

    return m("span", {className}, [
      m(".maille-switch-bg", {onclick: wrappedOnClick}, m(".maille-switch-knob")),
      vnode.children,
    ]);
  }

}

export default Switch;
