// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode, VnodeDOM, ChildArray, Children } from "mithril";

import Button from "../button";
import ButtonGroup from "../button-group";
import RadioInputGroup from "../radio-input-group";
import { RadioInputGroupAttrs, RadioInputOption } from "../radio-input-group/component";

import { MithrilEvent, ButtonType } from "../../types";

export type RadioInputGroupOnChangeFn = (value: any, e: MithrilEvent) => any;

class RadioInputButtonGroup extends RadioInputGroup {
  private selectedIdx: number = -1;
  private valuesByIdx: {[key: number]: any} = {};

  protected buildChild(option: RadioInputOption, checked: boolean, idx: number): Children {
    let buttonType = ButtonType.Info;
    if (this.selectedIdx === -1 && checked) { buttonType = ButtonType.Primary; }
    if (this.selectedIdx === idx) { buttonType = ButtonType.Primary; }

    this.valuesByIdx[idx] = option.value;

    return m(
      Button,
      {
        type: buttonType,
        rounded: this.rounded,
        disabled: this.disabled,
        //  When any radio input we assume it's when it's checked
        onclick: (event: MithrilEvent) => {
          this.selectedIdx = idx;
          this.onchange(event, option.value);
        },
      },
      option.label,
    );
  }

  protected buildContainer(classes: string[], vnode: Vnode<RadioInputGroupAttrs>): Children {
    return m(
      ButtonGroup,
      {
        id: vnode.attrs.id,
        className: classes.join(" "),
      },
      this.children,
    );
  }

  protected getSelectedValue(e?: MithrilEvent): any {
    return this.valuesByIdx[this.selectedIdx];
  }
}

export default RadioInputButtonGroup;
