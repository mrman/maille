// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3161 3241","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class MagnifyingGlassPlusThin implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2076 2296c-6-6-11-12-15-19-217 170-490 271-786 271-352 0-671-143-901-373C143 1944 1 1626 1 1274s143-671 373-901C605 142 923 0 1275 0s671 143 901 373c231 231 373 549 373 901 0 338-131 645-346 873 4 3 8 6 11 10l918 916c38 38 39 100 0 139-38 38-100 39-139 0l-918-916zM777 1373c-54 0-98-44-98-98s44-98 98-98h399V778c0-54 44-98 98-98s98 44 98 98v399h399c54 0 98 44 98 98s-44 98-98 98h-399v399c0 54-44 98-98 98s-98-44-98-98v-399H777zm1256-857c-194-194-462-314-759-314-296 0-565 120-759 314s-314 462-314 759c0 296 120 565 314 759s462 314 759 314c296 0 565-120 759-314s314-462 314-759c0-296-120-565-314-759z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon magnifying-glass-plus-thin";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
