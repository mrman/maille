// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class ContactBook implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M50.138 0H545.24c15.355 0 27.922 12.567 27.922 27.922v78.556h40.56c2.232 0 4.063 1.819 4.063 4.05v86.4a2.092 2.092 0 0 1-2.09 2.078H573.16v74.73h40.56c2.232 0 4.063 1.819 4.063 4.05v86.4a2.092 2.092 0 0 1-2.09 2.078H573.16v74.718h40.56a4.07 4.07 0 0 1 4.063 4.063v86.399a2.092 2.092 0 0 1-2.09 2.078H573.16v78.557c0 15.354-12.567 27.921-27.921 27.921H50.139c-15.355 0-27.922-12.567-27.922-27.921V27.922C22.217 12.567 34.784 0 50.139 0zm46.395 518.806v-64.288c34.394-15.307 139.596-43.642 144.628-85.678 1.063-9.58-21.308-46.501-26.457-63.887-6.437-10.3-9.945-13.973-9.792-26.705.095-7.122.26-14.138 1.276-21.036 1.275-8.764 1.04-9.035 5.622-16.134 4.795-7.335 2.717-34.146 2.717-44.304 0-100.737 176.565-100.797 176.565 0 0 12.792-2.941 36.202 3.98 46.182 3.413 4.83 2.764 5.48 3.91 11.457 1.452 7.783 1.618 15.768 1.724 23.835.13 12.732-3.307 16.405-9.792 26.705-6.248 18.236-30.07 52.737-28.004 63.107 7.571 38.434 105.178 64.23 135.935 77.895l-.012 72.851h-402.3z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon contact-book";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
