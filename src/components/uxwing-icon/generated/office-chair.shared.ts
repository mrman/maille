// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class OfficeChair implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M134.482 546.467c-9.662 4.665-21.284.602-25.95-9.06-4.665-9.66-.613-21.283 9.06-25.948 23.433-11.304 46.749-20.646 69.934-27.957 23.35-7.37 46.843-12.804 70.465-16.24a414.839 414.839 0 0 1 36.083-3.674v-19.311l-57.248-42.627c-2.079-2.764-3.579-5.894-4.488-9.165h-90.533v-.095c-19.477 0-37.087-7.89-49.796-20.598-12.709-12.71-20.587-30.32-20.599-49.773h-.082v-85.182H53.079v-.082a24.148 24.148 0 0 1-17.09-7.087 24.006 24.006 0 0 1-7.016-16.926l-.071-.082v-.024h.07v-19.878h-.07.07a24.136 24.136 0 0 1 7.1-17.103c4.31-4.31 10.287-6.98 16.925-7.016l.07-.07h86.954v.07c6.733.012 12.78 2.705 17.091 7.028v.071c4.3 4.311 6.98 10.3 7.016 16.925l.07.083v.012h-.07v19.878h.07v.012h-.07c-.012 6.732-2.705 12.791-7.028 17.102h-.07a24.025 24.025 0 0 1-16.926 7.005l-.083.082h-23.244v85.182h-.094c0 6.78 2.858 13.04 7.464 17.634 4.595 4.607 10.855 7.465 17.658 7.465v-.083h14.882v-41.079c0-17.445 15.343-31.701 34.087-31.701h250.113c18.756 0 34.087 14.268 34.087 31.701v41.08h14.894v.082c6.791 0 13.051-2.858 17.646-7.453 4.606-4.606 7.464-10.866 7.464-17.657h-.094v-85.171H499.966v-.082a24.169 24.169 0 0 1-17.09-7.087 24.006 24.006 0 0 1-7.016-16.926l-.071-.082v-.024h.07v-19.878h-.07.07a24.186 24.186 0 0 1 7.1-17.103c4.31-4.31 10.299-6.98 16.925-7.016l.082-.07h86.942v.07c6.733.012 12.78 2.705 17.091 7.028v.071c4.311 4.311 6.992 10.3 7.016 16.925l.071.083v.012h-.07v19.878h.07v.012h-.07a24.157 24.157 0 0 1-7.1 17.103 24.025 24.025 0 0 1-16.925 7.004l-.07.082h-26.576v85.171h-.094c0 19.464-7.89 37.087-20.599 49.795-12.709 12.697-30.32 20.587-49.784 20.587v.095h-88.242c-.897 4.004-2.633 7.83-5.22 11.102l-56.882 38.824v21.094c15.697.85 31.405 2.61 47.114 5.304 42.792 7.334 85.478 21.508 128.057 42.732 9.615 4.784 13.536 16.477 8.74 26.091-4.783 9.626-16.476 13.548-26.09 8.752-39.19-19.535-78.273-32.551-117.25-39.225a373.75 373.75 0 0 0-40.57-4.689v37.347c0 12.544-10.17 22.725-22.725 22.725-12.544 0-22.725-10.181-22.725-22.725v-37.252a378.54 378.54 0 0 0-30.603 3.2c-21.272 3.095-42.685 8.068-64.229 14.871-21.709 6.85-43.3 15.473-64.76 25.831zM173.908 0h283.846c9.401 0 17.94 7.748 17.09 17.09L451.4 237.784c-.992 9.319-7.724 17.09-17.09 17.09H198.51c-9.378 0-16.252-7.724-17.09-17.09L156.817 17.09C155.978 7.736 164.507 0 173.907 0zm116.564 392.485h52.855-52.855zm217.385 180.947c18.378 0 33.284 14.906 33.284 33.284S526.235 640 507.857 640c-18.379 0-33.284-14.906-33.284-33.284s14.905-33.284 33.284-33.284zm-189.983 0c18.378 0 33.272 14.906 33.272 33.284S336.252 640 317.874 640c-18.379 0-33.296-14.906-33.296-33.284s14.918-33.284 33.296-33.284zm-189.994 0c18.378 0 33.284 14.906 33.284 33.284S146.258 640 127.88 640c-18.39 0-33.296-14.906-33.296-33.284s14.906-33.284 33.296-33.284z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon office-chair";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
