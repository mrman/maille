// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class SpeechBubblesOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M433.848 137.576h132.651c40.43 0 73.5 33.083 73.5 73.513v129.71c0 40.43-33.07 73.512-73.5 73.512h-57.107c8.173 32.328 18.06 61.36 48.296 88.17-57.804-14.775-102.426-44.669-135.167-88.17h-9.874c-3.839 0-7.866-.295-12-.874 21.85-17.693 37.016-39.898 37.016-63.934v-54.047a29.708 29.708 0 0 0 17.008 5.315c16.477 0 29.835-13.359 29.835-29.835 0-16.477-13.358-29.847-29.835-29.847a29.708 29.708 0 0 0-17.008 5.315v-80.942c0-9.65-1.335-18.992-3.815-27.886zM297.157 245.388c-16.666 0-30.19 13.524-30.19 30.19 0 16.677 13.525 30.189 30.19 30.189 16.677 0 30.19-13.512 30.19-30.19 0-16.665-13.513-30.189-30.19-30.189zm-185.235 0c-16.666 0-30.19 13.524-30.19 30.19 0 16.677 13.525 30.189 30.19 30.189 16.677 0 30.19-13.512 30.19-30.19 0-16.665-13.513-30.189-30.19-30.189zm92.623 0c-16.665 0-30.19 13.524-30.19 30.19 0 16.677 13.525 30.189 30.19 30.189 16.678 0 30.178-13.512 30.178-30.19 0-16.665-13.5-30.189-30.178-30.189zM307.29 113.575H101.787c-45.744 0-83.174 37.43-83.174 83.174v146.79c0 45.744 37.43 83.174 83.174 83.174h64.62c-9.237 36.579-20.434 69.45-54.651 99.78 65.41-16.724 115.89-50.551 152.955-99.78h11.161c45.757 0 114.604-37.43 114.604-83.175V196.75c0-45.744-37.43-83.174-83.186-83.174zM101.787 94.972h205.527v.071c28.051 0 53.493 11.422 71.883 29.8l-.048.047c18.426 18.426 29.859 43.855 29.859 71.847h.07v.012h-.07v146.79h.07v.011h-.07c-.012 29.93-20.8 55.997-48.497 74.21-25.795 16.96-58.359 27.484-84.627 27.484v.083h-.012v-.083h-2.102c-18.543 23.103-40.087 42.84-64.725 59.032-27.272 17.906-58.111 31.395-92.706 40.241l-.012-.059c-6.508 1.642-13.689-.307-18.449-5.681-6.78-7.666-6.059-19.382 1.595-26.162 15.094-13.382 25.087-27.295 32.339-41.882 4.027-8.126 7.275-16.607 10.098-25.406h-40.146v-.083c-28.04 0-53.48-11.421-71.87-29.8C11.503 397.044.081 371.603.081 343.55h-.083v-.012h.083V196.75h-.083v-.023h.083c0-27.473 10.996-52.454 28.783-70.761.32-.378.662-.756 1.016-1.11 18.39-18.39 43.843-29.812 71.895-29.812v-.07h.012zm448.07 146.116c-16.477 0-29.848 13.37-29.848 29.847 0 16.477 13.37 29.835 29.847 29.835 16.477 0 29.835-13.358 29.835-29.835 0-16.476-13.358-29.847-29.835-29.847z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon speech-bubbles-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
