// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Snowflake implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M346.516 0v80.871l54.934-52.134 37.347 39.248-92.281 87.592v117.167l102.474-59.163 29.469-122.694 52.666 12.709-17.445 72.638 69.426-40.087 27.153 47.044-70.04 40.442 72.615 21.496-15.319 51.969-121.997-36.118-101.198 58.429 102.202 59.009 120.993-35.823 15.319 51.969-71.611 21.201 69.414 40.087-27.153 47.044-70.041-40.441 17.682 73.642-52.666 12.709-29.717-123.698-102.226-59.021v117.734l92.281 87.592-37.347 39.248-54.934-52.134V640h-54.319v-80.765l-54.167 51.426-37.346-39.248 91.513-86.872V366.819l-100.939 58.289-29.717 123.698-52.666-12.709 17.682-73.642-70.56 40.736-27.166-47.032 69.946-40.394-71.611-21.201 15.319-51.969 120.993 35.823 102.202-59.009-101.198-58.429-121.997 36.118-15.32-51.969 72.616-21.496-70.572-40.749 27.166-47.044 69.945 40.394-17.445-72.638 52.666-12.71 29.469 122.695 101.187 58.418V154.856l-91.513-86.87 37.346-39.25 54.167 51.426V0z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon snowflake";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
