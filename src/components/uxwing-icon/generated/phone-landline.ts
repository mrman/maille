// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PhoneLandline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M130.242 44.823h58.82V0h18.2v44.823h36.414v529.365H130.242V44.823zm-37.82 39.213h16.288v487.447c0 16.536 13.536 30.072 30.071 30.072h97.549c16.547 0 30.071-13.536 30.071-30.071V84.037h281.165c13.099 0 23.8 10.712 23.8 23.81v508.354c0 13.098-10.701 23.8-23.8 23.8H92.423c-13.099 0-23.8-10.702-23.8-23.8V107.848c0-13.099 10.701-23.811 23.8-23.811zm234.31 35.08h180.795c10.145 0 18.425 8.28 18.425 18.401v83.08c0 10.146-8.28 18.426-18.425 18.426H326.733c-10.134 0-18.414-8.28-18.414-18.426v-83.08c0-10.122 8.28-18.401 18.414-18.401zm45.993 217.278c12.083 0 21.875 9.803 21.875 21.886 0 12.083-9.792 21.886-21.875 21.886s-21.874-9.803-21.874-21.886c0-12.083 9.791-21.886 21.874-21.886zm66.39 198.05c12.084 0 21.875 9.803 21.875 21.886 0 12.083-9.791 21.886-21.874 21.886-12.083 0-21.886-9.803-21.886-21.886 0-12.083 9.803-21.886 21.886-21.886zm66.392-66.013c12.082 0 21.886 9.803 21.886 21.886 0 12.083-9.804 21.875-21.887 21.875-12.082 0-21.886-9.792-21.886-21.875s9.804-21.886 21.887-21.886zm-66.391 0c12.083 0 21.874 9.803 21.874 21.886 0 12.083-9.791 21.875-21.874 21.875-12.083 0-21.886-9.792-21.886-21.875s9.803-21.886 21.886-21.886zm-66.39 0c12.082 0 21.874 9.803 21.874 21.886 0 12.083-9.792 21.875-21.875 21.875s-21.874-9.792-21.874-21.875 9.791-21.886 21.874-21.886zm132.78-66.012c12.083 0 21.886 9.791 21.886 21.874 0 12.083-9.803 21.886-21.886 21.886-12.083 0-21.886-9.803-21.886-21.886 0-12.083 9.803-21.875 21.886-21.875zm-66.39 0c12.083 0 21.874 9.791 21.874 21.874 0 12.083-9.791 21.886-21.874 21.886-12.083 0-21.886-9.803-21.886-21.886 0-12.083 9.803-21.875 21.886-21.875zm-66.39 0c12.082 0 21.873 9.791 21.873 21.874 0 12.083-9.791 21.886-21.874 21.886-12.083 0-21.874-9.803-21.874-21.886 0-12.083 9.791-21.875 21.874-21.875zm132.78-66.025c12.083 0 21.886 9.803 21.886 21.886 0 12.083-9.803 21.886-21.886 21.886-12.083 0-21.886-9.803-21.886-21.886 0-12.083 9.803-21.886 21.886-21.886zm-66.39 0c12.082 0 21.874 9.803 21.874 21.886 0 12.083-9.792 21.886-21.875 21.886-12.082 0-21.886-9.803-21.886-21.886 0-12.083 9.804-21.886 21.887-21.886z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon phone-landline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
