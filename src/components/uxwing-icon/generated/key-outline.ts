// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 3290","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class KeyOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2138 1608c16-32 31-65 44-100s25-72 34-111c9-37 17-76 21-115 5-39 7-80 7-121 0-70-7-137-20-201s-34-126-61-186c-1-1-1-3-2-4-27-60-59-116-95-168-37-53-79-102-126-147l-1-1c-47-46-98-86-153-122s-115-67-179-94c-63-26-129-46-196-59-68-13-138-20-210-20-73 0-143 7-210 20-68 13-133 33-196 59-64 27-124 58-179 94s-106 77-153 122c-47 46-90 95-127 148s-69 110-97 171c-27 60-47 123-61 188-13 64-20 131-20 200 0 71 7 138 20 203 14 65 34 128 61 188 27 61 60 118 97 171s79 102 127 148c47 46 98 86 153 122s115 67 179 94c63 26 129 46 196 59 68 13 138 20 210 20 56 0 110-4 163-12s105-20 155-36 98-35 145-57c46-22 90-47 133-75 36-24 86-14 110 22 5 7 8 15 11 23l96 310h332c44 0 79 36 79 79v333h333c44 0 79 36 79 79v296h337v-387L2149 1701c-26-26-29-65-12-95zm192-44c-9 23-18 46-28 68l1005 1020c17 15 27 36 27 60v499c0 44-36 79-79 79h-496c-44 0-79-36-79-79v-296h-333c-44 0-79-36-79-79v-333h-311c-34 0-65-22-76-56l-85-273c-21 11-42 22-63 32-53 25-108 47-165 65-58 18-118 32-179 41s-123 14-186 14c-82 0-163-8-241-23s-153-38-227-68c-72-30-140-66-204-108s-123-89-177-141-103-109-146-171-80-127-111-197c-32-71-56-145-72-221S1 1242 1 1162c0-79 8-157 24-232 16-76 40-149 72-220 31-70 68-136 111-197 43-62 92-119 146-171 54-53 113-100 177-141 64-42 132-77 204-108 73-30 149-53 227-68s158-23 241-23c82 0 163 8 241 23s153 38 227 68c72 30 141 66 204 108 64 42 123 88 177 141 54 52 103 109 146 171 42 60 79 125 110 194 1 1 1 3 2 4 32 70 56 144 72 219 16 76 24 154 24 233 0 47-3 94-8 140s-14 91-25 135-24 86-40 128zM1040 666c48 0 92 9 134 26s80 43 113 76 59 71 76 113 26 87 26 134-9 92-26 134-43 79-76 113c-34 34-71 59-113 76s-87 26-134 26c-48 0-92-9-134-26-41-17-79-43-112-77s-58-72-75-113-25-86-25-133 9-92 25-133c17-42 42-80 75-113 33-34 71-60 112-77s86-26 134-26zm74 172c-22-9-47-14-74-14s-51 5-73 14-41 23-59 41c-18 19-32 40-41 62s-13 47-13 75c0 27 5 52 14 74 9 23 23 43 41 62 18 18 38 32 59 41 22 9 46 14 73 14 28 0 52-5 74-14s43-23 61-42c19-19 33-39 42-61s14-46 14-74-5-52-14-74-23-42-42-61-40-33-61-42z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon key-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
