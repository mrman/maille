// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ChainLinkBreaking implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M573.409 32.292l34.288 34.264c43.075 43.099 43.075 113.588 0 156.663l-90.828 90.78c-33.98 33.993-85.029 41.103-126.226 21.461l160.715-160.715c15.862-15.874 15.862-41.823 0-57.685l-31.11-31.11c-15.863-15.863-41.789-15.875-57.651.01l-159.722 159.7c-17.551-40.524-9.886-89.588 23.09-122.565L416.77 32.29c43.064-43.052 113.553-43.052 156.64 0zM416.285 608.358l33.213-6.874-21.473-103.608-33.19 6.992 21.45 103.489zM189.499 11.185l32.882-8.138 25.772 102.403-32.87 8.327-25.784-102.592zm346.233 539.794l23.599-24.331-75.899-73.643-23.587 24.366 75.887 73.608zm76.879-109.537l8.752-32.705-102.084-27.379-8.8 32.753 102.132 27.33zM13.218 210.782l9.402-32.564 101.564 28.89-9.213 32.635-101.753-28.961zm56.47-121.324l24-23.965 74.705 74.74-23.93 24.001-74.776-74.776zm-37.383 483.95l34.264 34.265c43.075 43.099 113.564 43.099 156.652 0l90.792-90.78c33.993-33.981 41.103-85.053 21.45-126.25L174.748 551.38c-15.863 15.863-41.812 15.863-57.674 0l-31.11-31.11c-15.863-15.886-15.863-41.836 0-57.698l159.721-159.687c-40.56-17.551-89.6-9.898-122.588 23.103l-90.792 90.78c-43.076 43.076-43.076 113.564 0 156.64z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon chain-link-breaking";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
