// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Fax implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M325.575 218.778h192.534v29.445H325.575v-29.445zM30.308 118.241h20.717v444.608c0 21.047 17.22 38.28 38.28 38.28h124.194c21.06 0 38.28-17.22 38.28-38.28V118.241h31.512V-10.088H537.55l8.657 3.603 57.013 56.233 3.673 3.626v64.867h2.8c16.689 0 30.319 13.642 30.319 30.33v471.184c0 16.677-13.63 30.33-30.32 30.33H30.309c-16.69 0-30.32-13.653-30.32-30.33V148.572c0-16.69 13.63-30.331 30.32-30.331zm277.763 156.947h274.137V84.485h-67.666v-69.78H308.07v260.484zM578.11 59.693l-38.776-38.256v38.256h38.776zm-252.535 43.678h236.023v29.445H325.575V103.37zm0 57.827h123.061v29.445H325.575v-29.445zM78.438 68.351h74.883V11.279h23.173v57.072h46.347v497.935H78.438V68.35zM360.571 347.65c15.39 0 27.862 12.46 27.862 27.85 0 15.378-12.472 27.851-27.862 27.851-15.378 0-27.863-12.473-27.863-27.85 0-15.39 12.485-27.851 27.863-27.851zm84.532 168.073c15.367 0 27.851 12.46 27.851 27.85 0 15.378-12.484 27.851-27.85 27.851-15.39 0-27.863-12.473-27.863-27.85 0-15.39 12.473-27.851 27.862-27.851zm84.51-84.049c15.377 0 27.862 12.485 27.862 27.851 0 15.39-12.485 27.863-27.863 27.863-15.39 0-27.85-12.473-27.85-27.863 0-15.366 12.46-27.85 27.85-27.85zm-84.51 0c15.367 0 27.851 12.485 27.851 27.851 0 15.39-12.484 27.863-27.85 27.863-15.39 0-27.863-12.473-27.863-27.863 0-15.366 12.473-27.85 27.863-27.85zm-84.532 0c15.39 0 27.863 12.485 27.863 27.851 0 15.39-12.473 27.863-27.863 27.863-15.378 0-27.863-12.473-27.863-27.863 0-15.366 12.485-27.85 27.863-27.85zm169.041-84.024c15.378 0 27.863 12.46 27.863 27.85 0 15.378-12.485 27.851-27.863 27.851-15.39 0-27.85-12.473-27.85-27.85 0-15.39 12.46-27.851 27.85-27.851zm-84.509 0c15.366 0 27.85 12.46 27.85 27.85 0 15.378-12.484 27.851-27.85 27.851-15.39 0-27.862-12.473-27.862-27.85 0-15.39 12.472-27.851 27.862-27.851z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon fax";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
