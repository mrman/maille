// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class CreditCards implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M457.884 77.233L239.744 209.79h62.009l161.045-97.855 23.622-14.363 68.186 112.218h39.685c8.622 0 16.453 3.52 22.11 9.178l-.023.023c5.681 5.693 9.201 13.512 9.201 22.075v367.658c0 8.575-3.52 16.382-9.177 22.052v.047c-5.658 5.67-13.489 9.177-22.11 9.177H45.721c-8.623 0-16.454-3.508-22.111-9.177v-.047c-5.658-5.67-9.177-13.477-9.177-22.052V241.066c0-8.61 3.52-16.44 9.177-22.098 5.657-5.658 13.488-9.178 22.11-9.178H58.75L397.422 4.004v-.011A27.42 27.42 0 0 1 418.174.792a27.452 27.452 0 0 1 17.008 12.449v.011l34.512 56.8-11.81 7.182zM45.107 237.617a16.82 16.82 0 0 0-2.906 4.359v45.272h555.61v-46.182c0-.98-.379-1.854-.993-2.469h-.047c-.65-.637-1.524-1.039-2.48-1.039H45.72c-.213 0-.414.024-.614.06zm52.548 203.565h99.226v37.123H97.655v-37.123zm0 87.958h277.338v33.473H97.655V529.14zm365.804-87.958h99.225v37.123H463.46v-37.123zm-121.94 0h99.227v37.123H341.52v-37.123zm-121.926 0h99.225v37.123h-99.225v-37.123zm378.217-49.82H42.2v217.362c0 .968.39 1.854 1.017 2.492l.035-.024a3.494 3.494 0 0 0 2.469 1.04h548.57c.956 0 1.83-.402 2.48-1.04l.024.024a3.558 3.558 0 0 0 1.015-2.492V391.363z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon credit-cards";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
