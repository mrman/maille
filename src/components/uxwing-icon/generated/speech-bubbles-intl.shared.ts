// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class SpeechBubblesIntl implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M616.118 155.896c12.012 12.012 20.327 27.815 22.95 45.402 1.003 6.792.909 12.567.909 19.288V330.43c0 5.94.141 11.834-.39 17.74-2.292 24.58-16.028 47.245-36.757 60.627-26.256 17.02-56.22 13.122-87.686 13.122 10.359 28.524 31.264 54.875 53.8 74.847l-8.433 15.863-.272-.071c-54.614-14.044-112.23-46.37-147.356-90.639-21.72-.224-39.473.06-61.122-2.858-23.859 14.48-52.206 23.93-80.28 23.93-18.178 22.665-39.332 42.023-63.498 57.91-26.102 17.15-55.56 30.153-88.454 38.823-1.641.413-3.236.862-4.913 1.015a18.171 18.171 0 0 1-15.673-6c-6.662-7.523-5.953-19.016 1.547-25.665 21.248-18.827 33.107-39.426 41.622-66.013-41.469 0-76.146 4.406-109.867-29.303C-9.885 371.592.58 273.512 2.682 214.622c.661-18.875-.65-31.371 6.921-51.072 9.945-25.796 30.237-46.524 55.903-56.883 23.988-9.673 50.103-7.263 75.426-7.263h153.91c11.304 0 20.517-.048 32.104 2.633 19.476 4.489 37.11 14.835 50.941 29.883l177.734.059c23.457 0 43.63 6.992 60.497 23.917zm-95.635 166.1H473.77l-6.45 23.256h-41.893c14.02-39.59 28.063-79.206 42.095-118.844 5.043-14.291 10.795-38.008 29.173-38.008 19.04 0 25.359 21.72 30.674 36.733l42.58 120.12h-42.934l-6.532-23.257zm-8.599-30.45L497.215 240.9l-14.575 50.647h29.244zm-78.78 180.842L402.702 447.3l-37.937 46.831-55.052-47.752-25.973 29.386 59.658 47.433 48.343-4.157 41.363-46.654zM399.23 150.475c17.81 69.32 36.815 191.069-11.02 252.759 11.327.165 22.453.153 33.768.153 25.63 34.064 58.56 57.863 97.312 75.25a260.723 260.723 0 0 0 11.055 4.7c-13.18-15.945-24.768-34.3-32.09-53.8-3.39-9-5.197-16.949-7.512-26.15 10.641 0 21.059.13 31.7.508 23.588.839 48.45 1.488 68.08-9.508 8.469-4.748 15.331-11.457 20.658-19.535 4.748-7.24 8.067-15.509 9.508-24.355 1.063-6.697.756-14.032.756-20.8v-112.69c0-2.952.07-5.692-.154-8.645-1.193-15.272-7.984-29.091-18.295-39.426-10.004-10.028-23.386-16.725-38.186-18.19-3.826-.377-7.559-.318-11.386-.318l-154.195.047zm-288.783 18.921h69.98v-19.713h31.277v19.713h70.24v32.375h-14.409c-1.157 9.106-3.26 18.2-6.212 27.106l.035.024c-2.965 9.095-7.004 18.26-11.87 27.33-8.209 15.344-18.827 30.131-31.146 44.281 19.665 23.54 44.09 43.997 73.158 62.658l-16.087 25.064c-30.52-19.583-56.445-41.139-77.74-65.99-19.064 18.426-40.702 35.611-63.38 51.297L117.38 349.08c22.453-15.532 43.76-32.481 62.103-50.564-16.914-24.744-29.847-52.5-38.812-84.414l28.69-8.056c7.346 26.186 17.646 49.23 30.886 69.864 9.035-10.938 16.843-22.146 23.008-33.674 3.898-7.288 7.217-14.87 9.78-22.677l.07.023v-.023a119.45 119.45 0 0 0 4.335-17.788H110.446v-32.374z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon speech-bubbles-intl";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
