// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 2560 3333","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PadlockOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M237 1240h78v-237c0-275 108-525 282-707C772 113 1014 0 1280 0s508 113 683 296c174 182 282 432 282 707v237h78c65 0 124 27 167 69 43 43 69 102 69 167v1620c0 65-27 124-69 167-43 43-102 69-167 69H236c-65 0-124-27-167-69-43-43-69-102-69-167V1476c0-65 27-124 69-167 43-43 102-69 167-69zm957 1128l-114 298h400l-105-302c67-34 113-104 113-185 0-115-93-207-207-207-115 0-207 93-207 207 0 84 50 156 121 189zM482 1240h1597v-237c0-231-90-440-236-592-145-151-344-245-563-245s-418 94-563 245c-145 152-236 361-236 592v237zm1842 166H237c-19 0-37 8-49 21-13 13-21 30-21 49v1620c0 19 8 37 21 49 13 13 30 21 49 21h2087c19 0 37-8 49-21 13-13 21-30 21-49V1476c0-19-8-37-21-49-13-13-30-21-49-21z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon padlock-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
