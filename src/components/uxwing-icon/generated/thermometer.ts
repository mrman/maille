// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Thermometer implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M480.077 489.856c0 41.422-15.709 76.796-47.127 106.135-31.228 29.35-69.024 44.02-113.21 44.02-44.185 0-81.804-14.67-113.045-44.02-31.063-29.339-46.772-64.713-46.772-106.135 0-47.634 19.855-86.292 59.198-116.328V99.58c0-27.614 10.016-51.095 29.858-70.595C268.834 9.662 292.822-.012 320.78-.012c27.603 0 51.25 9.839 70.761 29.34 19.5 19.511 29.34 43.157 29.34 70.772v273.429c39.52 30.035 59.197 68.693 59.197 116.328zm-78.355-5.185c0-24.154-10.181-44.174-30.379-59.54V97.5c0-14.15-5.008-26.233-14.846-36.414-10.004-10.004-22.087-15.012-36.237-15.012-14.15 0-26.41 4.83-36.59 14.847-10.36 10.004-15.533 21.921-15.533 36.07v328.139c-20.185 15.19-30.2 35.044-30.2 59.54 0 21.236 7.936 39.355 23.988 54.202 15.874 14.834 35.209 22.264 57.815 22.264 22.607 0 41.942-7.43 57.993-22.265 15.874-14.846 23.989-32.965 23.989-54.201zm-45.568-240.298v197.329c15.532 11.906 23.304 26.233 23.304 42.969 0 15.543-5.87 28.831-17.434 39.697-11.74 11.044-25.713 16.571-42.284 16.571-16.57 0-30.543-5.527-42.106-16.57-11.74-10.867-17.611-24.155-17.611-39.698 0-16.736 7.949-31.063 23.823-42.97V244.374h72.308z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon thermometer";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
