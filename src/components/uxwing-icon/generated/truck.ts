// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Truck implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M405.466 178.916h96.97c29.055 0 22.24-3.449 38.54 20.67l81.686 120.804C639.752 345.666 640 340.221 640 370.564v99.864c0 32.374-26.48 58.867-58.867 58.867h-12.012a83.504 83.504 0 0 0 1.571-16.134c0-46.147-37.418-83.564-83.564-83.564-46.146 0-83.564 37.417-83.564 83.564 0 5.515.544 10.913 1.571 16.134H299.46a84.443 84.443 0 0 0 1.56-16.134c0-46.147-37.419-83.564-83.565-83.564-46.134 0-83.564 37.417-83.564 83.564 0 5.515.544 10.913 1.571 16.134H121.55c-29.765 0-54.12-24.355-54.12-54.12V387.82h338.037V178.916zm81.662 268.562c36.26 0 65.67 29.41 65.67 65.682 0 36.26-29.41 65.67-65.67 65.67-36.272 0-65.681-29.41-65.681-65.67 0-36.272 29.41-65.682 65.681-65.682zm-269.684 0c36.272 0 65.682 29.41 65.682 65.682 0 36.26-29.41 65.67-65.682 65.67-36.26 0-65.67-29.41-65.67-65.67 0-36.272 29.41-65.682 65.67-65.682zm0 46.24c10.737 0 19.442 8.706 19.442 19.442 0 10.724-8.705 19.43-19.442 19.43-10.724 0-19.429-8.706-19.429-19.43 0-10.736 8.705-19.441 19.43-19.441zm269.684-.484c10.997 0 19.914 8.918 19.914 19.926 0 10.996-8.917 19.913-19.914 19.913-10.996 0-19.925-8.917-19.925-19.913 0-11.008 8.93-19.926 19.925-19.926zm-25.3-266.838h18.65l79.761 111.108v32.28c0 7.831-6.401 14.233-14.244 14.233H461.83c-8.032 0-14.587-6.544-14.587-14.575V240.984c0-8.02 6.555-14.587 14.587-14.587zM29.447 61.181H367.47c16.193 0 29.445 13.253 29.445 29.446v257.318c0 16.193-13.252 29.433-29.445 29.433H29.446C13.253 377.378 0 364.138 0 347.945V90.627C0 74.434 13.253 61.18 29.446 61.18z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon truck";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
