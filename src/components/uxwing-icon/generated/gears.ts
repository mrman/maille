// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 2579","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Gears implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M1807 262c-21-21-47-32-76-32s-55 11-76 32l-115 115c-31-20-64-38-98-53-34-16-69-29-104-41V106c0-30-10-55-31-76s-46-31-76-31h-222c-28 0-53 10-75 31-21 21-32 46-32 76v162c-37 9-73 20-108 33s-68 29-99 46L567 221c-20-21-44-32-74-32-29 0-54 11-76 32L262 377c-21 21-32 47-32 76s11 55 32 76l115 115c-20 31-37 64-53 98s-29 69-41 105H106c-30 0-55 10-76 31s-31 46-31 76v222c0 28 10 53 31 75 21 21 46 32 76 32h162c9 37 20 73 33 108s29 69 46 102l-126 126c-21 20-32 44-32 74 0 29 11 54 32 76l156 158c21 20 47 30 76 30s55-10 76-30l115-117c31 20 64 37 98 53s69 29 104 41v177c0 30 10 55 31 76s46 31 76 31h222c28 0 53-10 75-31 21-21 32-46 32-76v-162c37-9 73-20 108-33s69-29 102-46l126 126c20 21 45 32 75 32s55-11 75-32l158-156c20-22 30-47 30-76s-10-55-30-76l-117-115c20-31 37-64 53-98s29-69 41-104h177c30 0 55-10 76-31s31-46 31-76v-222c0-28-10-53-31-75-21-21-46-32-76-32h-162c-9-36-20-71-33-107s-29-69-46-101l126-128c21-20 32-44 32-74 0-29-11-54-32-76l-156-155zm1229 1193c-14-10-28-13-44-10s-29 11-38 25l-52 73c-19-8-38-14-58-20-20-5-40-9-61-12l-17-96c-3-16-11-29-24-38s-28-12-44-10l-120 21c-15 3-28 11-38 24-9 13-13 28-10 44l15 88c-19 8-38 18-55 28-18 11-34 22-50 34l-81-56c-13-10-27-13-43-10s-28 11-38 25l-70 99c-10 14-13 28-10 44s11 29 25 38l73 52c-8 19-14 38-20 58-5 20-9 40-12 61l-96 17c-16 3-29 11-38 24s-12 28-10 44l21 120c3 15 11 28 24 38 13 9 28 13 44 10l88-15c8 19 18 38 28 55 11 18 22 35 34 51l-56 80c-10 13-13 27-10 43s11 28 25 38l99 71c14 9 28 12 44 9s29-11 38-23l52-74c19 8 38 14 58 20 20 5 40 9 61 12l17 96c3 16 11 29 24 38s28 12 44 10l120-21c15-3 28-11 38-24 9-13 13-28 10-44l-15-88c19-8 38-18 55-28 18-11 35-22 51-34l80 56c13 10 27 13 44 10 16-3 29-11 38-24l71-99c9-14 12-28 9-44s-10-29-23-38l-75-52c8-19 14-38 20-58 5-20 9-40 12-61l96-17c16-3 29-11 38-24s12-28 10-44l-21-120c-3-15-11-28-24-38s-28-13-44-10l-88 15c-8-19-17-37-28-55s-22-35-34-50l56-81c10-13 13-27 10-43s-11-28-25-38l-99-70zm-346 306c29-5 57-4 85 2 28 7 53 18 75 33 22 16 41 36 57 60s26 51 31 80 4 57-2 85c-7 28-18 53-33 75-16 22-36 41-60 57s-51 26-80 31-57 4-85-2c-28-7-53-18-75-33-22-16-41-36-57-60s-26-51-31-80-4-57 2-85c7-28 18-53 33-75 16-22 36-41 60-57s51-26 80-31zM1092 657c60 0 116 11 170 34 53 23 99 54 139 93 39 39 70 86 93 139 23 54 34 110 34 170s-11 116-34 170c-23 53-54 99-93 139-39 39-86 70-139 93s-110 34-170 34-116-11-170-34c-53-23-99-54-139-93-39-39-70-86-93-139s-34-110-34-170 11-116 34-170c23-53 54-99 93-139 39-39 86-70 139-93 54-23 110-34 170-34z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon gears";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
