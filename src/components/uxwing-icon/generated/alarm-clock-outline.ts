// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 2272 2316","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class AlarmClockOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M597 1960l-197 338c-10 17-31 23-48 13s-23-31-13-48l202-347c-94-81-172-182-226-296-57-119-89-251-89-391 0-251 102-478 266-642s391-266 642-266 478 102 642 266 266 391 266 642-102 478-266 642c-16 16-32 31-49 45l202 347c10 17 4 39-13 48-17 10-39 4-48-13l-197-338c-150 111-336 176-537 176-105 0-206-18-301-51-85-30-165-72-236-125zm526-1312c0-20 16-35 35-35 20 0 35 16 35 35v545h525c20 0 35 16 35 35 0 20-16 35-35 35h-560c-20 0-35-16-35-35V647zm602-11c-151-151-361-245-592-245s-440 94-592 245c-151 151-245 361-245 592 0 129 29 252 81 361 54 113 134 213 231 291l1 1c73 59 156 105 246 137 87 30 180 47 277 47 231 0 440-94 592-245 151-151 245-361 245-592s-94-440-245-592zm546-179c0 64-16 124-44 179-29 56-72 106-124 146-15 12-38 9-50-6-3-3-4-7-6-11-44-135-118-257-215-358-96-101-215-180-348-231-18-7-28-27-21-46 3-8 8-14 15-18 85-57 191-85 297-85 83 0 167 17 241 51 75 34 140 85 186 152 42 62 67 138 67 226zm-107 147c23-45 36-94 36-147 0-73-20-136-55-186-38-56-93-99-157-128-65-29-138-44-211-44-69 0-138 13-198 40 115 54 219 129 306 220 91 96 164 209 213 335 27-27 50-57 67-90zM740 221c-135 44-257 118-358 215-101 96-180 215-231 348-7 18-27 28-46 21-8-3-14-8-18-15-28-42-51-89-66-140C6 600-2 547-2 491c0-122 41-230 107-314 52-67 120-119 195-148C375-1 458-8 537 13s155 70 219 153c12 15 9 38-6 50-3 3-7 4-11 6zM333 384c96-91 209-164 335-213-46-47-97-77-149-91-64-17-131-11-193 13-63 25-120 68-165 126-56 72-92 166-92 271 0 49 7 95 20 138 7 22 14 43 24 63 54-116 129-220 220-307z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon alarm-clock-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
