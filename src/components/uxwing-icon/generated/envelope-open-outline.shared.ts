// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3198 3333","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class EnvelopeOpenOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2116 156v681h660l-660-681zM338 1249l928 829c1 1 3 2 4 4l313 280 1264-1104V986h-806c-41 0-74-33-74-74V149H338v1100zm2658-121c20-17 43-25 66-25 20 0 38 6 56 16 13 8 24 18 34 29 26 30 46 77 46 116v1916c0 42-17 80-45 108s-66 45-108 45H155c-42 0-80-17-108-45s-45-66-45-108V1264c0-39 20-87 47-117 10-11 21-21 34-28 17-10 35-16 55-16 18 0 36 5 53 15V75c0-41 33-74 74-74h1877c23 0 44 10 57 27l777 803c14 14 21 33 21 52v21c0 3 1 6 1 9s0 6-1 9v207zM1533 2516l-315-282-1069 937v8c0 1 0 3 1 3 1 1 2 1 3 1h2890c1 0 3 0 3-1 1-1 1-2 1-3v-5l-1084-950-334 291c-28 24-70 25-98-1zm-427-381l-958-856v1695l958-839zm1942 843V1279l-971 848 971 851z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon envelope-open-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
