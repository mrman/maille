// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Paperclip implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M46.217 73.135c-61.607 61.595-61.619 162.356-.012 223.952L278.33 529.212c25.382 1.429 39.154-9.213 36.745-36.745L84.84 262.232c-41.387-41.386-41.387-109.076 0-150.462 41.374-41.374 109.088-41.386 150.474-.012a4528161.684 4528161.684 0 0 0 332.354 332.366c26.22 26.21 27.697 71.316 1.453 97.56-26.233 26.233-71.14 24.592-97.372-1.641-84.095-84.095-167.54-167.553-251.637-251.637-11.646-11.646-12.236-31.288-.59-42.945 11.634-11.646 31.3-11.044 42.945.59l149.506 149.506c27.083 2.138 38.717-11.138 37.406-37.406L299.873 208.645c-32.233-32.233-84.946-32.233-117.155-.012-32.244 32.233-32.209 84.982-.012 117.18l102.415 102.414 48.272 48.284 100.005 100.005c46.76 46.76 126.214 49.69 172.975 2.918 46.784-46.784 43.867-126.214-2.905-172.986L455.178 258.17v.012L270.146 73.137c-61.583-61.607-162.356-61.584-223.928 0z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon paperclip";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
