// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ConnectedNodesSquare implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M579.999 0H60C27 0 0 27 0 60v520c0 33 27 60 60 60h519.999c33 0 60-27 60-60V60c0-33-27-60-60-60zM239.967 267.547l136.927-67.584c-.862-4.677-1.287-9.531-1.287-14.48 0-43.418 35.22-78.627 78.603-78.627 43.489 0 78.71 35.21 78.71 78.627 0 43.477-35.221 78.662-78.71 78.662-17.126 0-32.989-5.48-45.946-14.799l-143.966 70.997a81.05 81.05 0 0 1 .107 4.157c0 2.41-.142 4.784-.343 7.146l132.415 65.351c14.067-13.122 32.953-21.153 53.67-21.153 43.442 0 78.663 35.197 78.663 78.674 0 43.418-35.221 78.627-78.663 78.627-43.441 0-78.65-35.21-78.65-78.627 0-1.677.07-3.32.153-4.95l-133.915-66.106c-13.819 12.248-32.055 19.713-51.933 19.713-43.49 0-78.71-35.257-78.71-78.675 0-43.453 35.22-78.697 78.71-78.697 20.964 0 40.075 8.303 54.166 21.744z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon connected-nodes-square";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
