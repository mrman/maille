// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 2721 2719","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class FourCornerArrowDiagonalOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2544 1989c0-49 40-89 89-89s89 40 89 89v637c0 49-40 89-89 89h-647c-49 0-89-40-89-89s40-89 89-89h425l-761-719c-36-33-37-89-4-125s89-37 125-4l774 731v-432zM1989 177c-49 0-89-40-89-89s40-89 89-89h637c49 0 89 40 89 89v647c0 49-40 89-89 89s-89-40-89-89V310l-719 761c-33 36-89 37-125 4s-37-89-4-125l731-774h-432zM177 725c0 49-40 89-89 89s-89-40-89-89V88C-1 39 39-1 88-1h647c49 0 89 40 89 89s-40 89-89 89H310l761 719c36 33 37 89 4 125s-89 37-125 4L176 294v432zm553 1816c49 0 89 40 89 89s-40 89-89 89H93c-49 0-89-40-89-89v-647c0-49 40-89 89-89s89 40 89 89v425l719-761c33-36 89-37 125-4s37 89 4 125l-731 774h432z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon four-corner-arrow-diagonal-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
