// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class SpeechBubbles implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M297.157 245.389c-16.666 0-30.19 13.524-30.19 30.19 0 16.677 13.524 30.189 30.19 30.189 16.677 0 30.19-13.512 30.19-30.19 0-16.665-13.513-30.189-30.19-30.189zm-185.235 0c-16.666 0-30.19 13.524-30.19 30.19 0 16.677 13.524 30.189 30.19 30.189 16.677 0 30.19-13.512 30.19-30.19 0-16.665-13.513-30.189-30.19-30.189zm92.623 0c-16.665 0-30.19 13.524-30.19 30.19 0 16.677 13.525 30.189 30.19 30.189 16.678 0 30.178-13.512 30.178-30.19 0-16.665-13.5-30.189-30.178-30.189zm229.302-107.801h132.651c40.43 0 73.501 33.071 73.501 73.5v129.71c0 40.43-33.071 73.514-73.5 73.514H509.39c8.174 32.327 18.06 61.359 48.296 88.17-57.804-14.776-102.426-44.67-135.167-88.17h-9.874c-3.839 0-7.854-.307-11.988-.874 21.839-17.694 37.004-39.899 37.004-63.934v-54.048a29.708 29.708 0 0 0 17.008 5.315c16.477 0 29.835-13.359 29.835-29.835 0-16.477-13.358-29.847-29.835-29.847a29.708 29.708 0 0 0-17.008 5.315v-80.942c0-9.65-1.335-18.992-3.815-27.874zm116.01 103.501c-16.477 0-29.848 13.37-29.848 29.847 0 16.476 13.37 29.835 29.847 29.835 16.477 0 29.835-13.359 29.835-29.835 0-16.477-13.358-29.847-29.835-29.847zM101.786 94.973h205.527v.07c28.051 0 53.493 11.422 71.883 29.8l-.048.048c18.426 18.425 29.859 43.855 29.859 71.847h.07v.012h-.07v146.789h.07v.012h-.07c-.012 29.93-20.8 55.996-48.497 74.21-25.795 16.96-58.359 27.484-84.627 27.484v.083h-.012v-.083h-2.102c-18.543 23.102-40.087 42.839-64.725 59.032-27.272 17.906-58.111 31.394-92.706 40.24l-.012-.058c-6.508 1.641-13.689-.307-18.449-5.681-6.78-7.666-6.059-19.383 1.595-26.162 15.094-13.382 25.087-27.296 32.339-41.883 4.027-8.126 7.275-16.606 10.098-25.405h-40.146v-.083c-28.04 0-53.48-11.421-71.87-29.8C11.503 397.044.081 371.602.081 343.551h-.083v-.012h.083v-146.79h-.083v-.023h.083c0-27.473 10.996-52.453 28.783-70.76.32-.378.662-.756 1.016-1.11 18.39-18.39 43.843-29.812 71.895-29.812v-.07h.012z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon speech-bubbles";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
