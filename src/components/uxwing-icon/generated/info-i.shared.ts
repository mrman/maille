// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class InfoI implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M340.079 0c23.103 0 41.28 6.673 54.52 19.89 13.099 13.181 19.713 31.288 19.713 54.591 0 23.634-11.268 44.646-33.957 63.072-22.82 18.425-49.288 27.697-79.454 27.697-22.665 0-40.69-6.343-54.508-19.063-13.807-12.863-20.705-29.505-20.705-50.198 0-26.079 11.268-48.555 33.673-67.642C281.755 9.46 308.65 0 340.08 0zm129.108 640H170.813v-50.21h56.068V283.127h-56.068v-39.97h139.454c35.22 0 69.887-4.228 104.127-12.862v359.497h54.792v50.21z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon info-i";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
