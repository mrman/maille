// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Gear implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M529.117 76.572l45.922 45.933c12.094 12.095 12.094 31.867 0 43.962l-36.981 36.98a246.212 246.212 0 0 1 22.95 61.206h47.905c17.103 0 31.087 13.996 31.087 31.087v64.95c0 17.09-13.984 31.075-31.087 31.075h-52.288a245.952 245.952 0 0 1-27.095 59.469l33.91 33.91c12.083 12.106 12.083 31.878 0 43.961l-45.933 45.934c-12.083 12.094-31.867 12.094-43.95 0l-36.992-36.981a245.75 245.75 0 0 1-61.206 22.949v47.906c0 17.103-13.996 31.087-31.087 31.087h-64.95c-17.09 0-31.075-13.984-31.075-31.087v-52.288a245.986 245.986 0 0 1-59.469-27.107l-33.91 33.922c-12.106 12.083-31.878 12.083-43.973 0l-45.921-45.934c-12.095-12.094-12.095-31.866 0-43.96l36.98-36.981a245.75 245.75 0 0 1-22.949-61.206H31.087C13.997 375.359 0 361.363 0 344.272v-64.95c0-17.09 13.997-31.075 31.087-31.075h52.288a246.273 246.273 0 0 1 27.107-59.48l-33.91-33.899c-12.083-12.106-12.083-31.878 0-43.961l45.934-45.934c12.082-12.094 31.866-12.094 43.949 0l36.993 36.981c19.051-10.205 39.579-18 61.205-22.949V31.099c0-17.103 13.985-31.087 31.087-31.087h64.938c17.103 0 31.087 13.984 31.087 31.087v52.276a245.907 245.907 0 0 1 59.493 27.107l33.886-33.91c12.106-12.083 31.878-12.083 43.973 0zM320 192.31c70.513 0 127.702 57.19 127.702 127.69 0 70.514-57.19 127.703-127.702 127.703-70.5 0-127.691-57.19-127.691-127.702 0-70.501 57.19-127.691 127.691-127.691z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon gear";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
