// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3016 2973","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class FourSquareGridOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M1826 1615h1017c48 0 91 19 122 51 31 31 51 75 51 122v1012c0 48-19 91-51 122-31 31-75 51-122 51H1826c-48 0-91-19-122-51-31-31-51-75-51-122V1788c0-48 19-91 51-122 31-31 75-51 122-51zM173 0h1017c48 0 91 19 122 51 31 31 51 75 51 122v1012c0 48-19 91-51 122-31 31-75 51-122 51H173c-47 0-91-19-122-51-31-31-51-75-51-122V173c0-48 19-91 51-122 2-2 5-4 7-6C89 17 129 1 173 1zm1008 182H181v995h1000V182zM1826 0h1017c48 0 91 19 122 51 31 31 51 75 51 122v1012c0 48-19 91-51 122-31 31-75 51-122 51H1826c-48 0-91-19-122-51-31-31-51-75-51-122V173c0-48 19-91 51-122 31-31 75-51 122-51zm1008 182H1834v995h1000V182zM173 1615h1017c48 0 91 19 122 51 31 31 51 75 51 122v1012c0 48-19 91-51 122-31 31-75 51-122 51H173c-47 0-91-19-122-51-31-31-51-75-51-122V1788c0-48 19-91 51-122 2-2 5-4 7-6 31-28 71-44 115-44zm1008 182H181v995h1000v-995zm1653 0H1834v995h1000v-995z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon four-square-grid-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
