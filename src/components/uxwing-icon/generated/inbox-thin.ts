// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3172 3335","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class InboxThin implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M187 0h2797c52 0 98 21 132 55s55 81 55 132v2960c0 52-21 98-55 132s-81 55-132 55H187c-52 0-98-21-132-55s-55-81-55-132V187c0-52 21-98 55-132S136 0 187 0zm885 1830c-36 0-65-36-65-80s29-80 65-80h1027c36 0 65 36 65 80s-29 80-65 80H1072zm-51 309c41 0 75 31 79 70 1 6 2 12 2 18v238h940v-238-5-4c0-44 36-80 80-80h403V559H645v1579h375zm-78 160H616c-35 0-68-15-92-38-2-2-4-4-6-7-20-23-32-52-32-85V530c0-36 15-68 38-92 2-2 5-4 7-6 23-20 52-32 84-32h1940c36 0 68 15 92 38 24 24 38 56 38 92v1639c0 36-15 68-38 92-23 23-56 38-92 38h-354v184c0 39-16 75-42 101s-62 42-101 42h-973c-36 0-69-14-95-36l-6-6c-26-26-42-62-42-101v-184zm130-1265c-36 0-65-36-65-80s29-80 65-80h1027c36 0 65 36 65 80s-29 80-65 80H1073zm0 386c-36 0-65-36-65-80s29-80 65-80h1027c36 0 65 36 65 80s-29 80-65 80H1073zM2985 159H188c-8 0-14 3-19 8s-8 12-8 19v2960c0 7 3 14 8 19s12 8 19 8h2797c7 0 14-3 19-8s8-12 8-19V186c0-7-3-14-8-19s-12-8-19-8z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon inbox-thin";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
