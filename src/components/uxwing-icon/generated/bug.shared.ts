// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Bug implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M492.124 349.209c.177 4.441.26 8.93.26 13.43 0 12.377-.65 24.531-1.914 36.413l77.718 39.438c8.256 4.169 13.63 11.775 15.272 20.208l33.118 146.246c3.485 15.473-6.236 30.84-21.72 34.324-15.473 3.484-30.84-6.248-34.312-21.72l-30.331-133.88-51.461-26.115c-25.926 86.091-86.033 146.435-156.037 146.435-70.407 0-130.81-61.052-156.474-147.96l-56.233 25.536-25.784 135.084c-2.988 15.603-18.048 25.831-33.65 22.843-15.603-2.988-25.831-18.047-22.843-33.65L56.21 456.666c1.57-9.095 7.488-17.268 16.535-21.343l82.135-37.3a344.472 344.472 0 0 1-1.819-35.386c0-4.547.083-9.082.26-13.57l-86.73-29.93c-9.082-3.13-15.59-10.382-18.141-18.934l-.083.024-31.76-105.249c-4.536-15.224 4.134-31.252 19.358-35.788 15.225-4.535 31.253 4.134 35.788 19.359l27.508 91.17 69.438 23.965h308.45l63.59-21.945 27.509-91.17c4.535-15.225 20.563-23.894 35.788-19.359 15.224 4.547 23.894 20.563 19.358 35.788l-31.76 105.249-.083-.024c-2.55 8.563-9.059 15.803-18.142 18.933l-81.284 28.052zM320.909 133.088c10.795 0 21.307 2.008 31.394 5.776 3.697-8.197 7.051-15.827 10.122-22.82C394.611 42.84 398.792 33.307 509.687.791c10.37-3.047 21.26 2.894 24.308 13.264 3.047 10.37-2.894 21.26-13.264 24.308-93.368 27.378-96.71 34.996-122.411 93.473-3.579 8.126-7.536 17.138-12.26 27.473 29.244 25.217 52.17 66.863 63.97 117.238H191.777c11.811-50.374 34.737-92.02 63.981-117.238-4.724-10.335-8.693-19.347-12.26-27.473-25.7-58.477-29.055-66.095-122.41-93.473-10.371-3.048-16.312-13.937-13.265-24.308 3.048-10.37 13.926-16.31 24.308-13.264 110.883 32.517 115.076 42.048 147.25 115.253 3.082 6.993 6.437 14.623 10.134 22.82 10.086-3.768 20.587-5.776 31.394-5.776zm48.627 57.544c14.433 0 26.138 11.705 26.138 26.138 0 14.422-11.705 26.126-26.138 26.126-14.422 0-26.127-11.704-26.127-26.126 0-14.433 11.705-26.138 26.127-26.138zm-97.265 0c14.433 0 26.138 11.705 26.138 26.138 0 14.422-11.705 26.126-26.138 26.126-14.434 0-26.127-11.704-26.127-26.126 0-14.433 11.693-26.138 26.127-26.138z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon bug";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
