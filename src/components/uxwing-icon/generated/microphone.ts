// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Microphone implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M551.83 283.763c0-17.362-14.067-31.43-31.441-31.43-17.363 0-31.43 14.068-31.43 31.43 0 61.926-14.823 113.045-44.93 147.203-27.248 30.898-68.41 48.686-124.028 48.686-55.631 0-96.817-17.764-124.041-48.662-30.095-34.146-44.918-85.253-44.918-147.226 0-17.363-14.067-31.43-31.43-31.43-17.374 0-31.441 14.067-31.441 31.43 0 77.21 20.008 142.632 60.638 188.73 29.918 33.934 70.182 56.86 120.946 65.824v51.58h-77.02c-13.784 0-25.052 11.28-25.052 25.04v25.074h304.634V614.95c0-13.772-11.268-25.051-25.051-25.051h-77.02v-51.58c50.752-8.976 91.016-31.902 120.934-65.847 40.642-46.11 60.65-111.556 60.65-188.707zM320.012-.012c64.123 0 116.588 52.477 116.588 116.6v3.012h-62.717v61.453H436.6v37.82h-62.717v61.477H436.6v26.705c0 64.111-52.465 116.576-116.588 116.576-64.135 0-116.588-52.465-116.588-116.576V280.35h62.706v-61.477h-62.706v-37.82h62.706V119.6h-62.706v-3.012c0-64.123 52.453-116.6 116.588-116.6z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon microphone";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
