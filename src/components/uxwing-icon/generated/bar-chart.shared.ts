// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class BarChart implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M640 595.992H49.973v-.13c-13.784 0-26.28-5.61-35.315-14.646C5.705 572.252.142 559.814.13 546.043H0V43.997h63.851v488.156h576.15v63.84zM434.214 281.52h101.8c5.421 0 9.85 4.441 9.85 9.85v200.307c0 5.409-4.44 9.85-9.85 9.85h-101.8c-5.41 0-9.85-4.43-9.85-9.85V291.37c0-5.41 4.429-9.851 9.85-9.851zM132.108 158.932h101.8c5.421 0 9.85 4.43 9.85 9.85v322.895c0 5.42-4.429 9.85-9.85 9.85h-101.8c-5.421 0-9.85-4.43-9.85-9.85V168.783c0-5.422 4.429-9.851 9.85-9.851zm151.053-68.965H384.95c5.421 0 9.85 4.429 9.85 9.85v391.86c0 5.409-4.44 9.85-9.85 9.85H283.161c-5.41 0-9.85-4.43-9.85-9.85V99.816c0-5.421 4.429-9.85 9.85-9.85z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon bar-chart";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
