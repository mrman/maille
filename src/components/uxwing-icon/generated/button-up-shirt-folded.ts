// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ButtonUpShirtFolded implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M167.494 23.894L321.901 187.88 472.316 23.776a14.198 14.198 0 0 0-7.642-2.232H357.76l-186.239.52a14.47 14.47 0 0 0-4.027 1.83zM61.961 126.899h77.883V35.788a35.378 35.378 0 0 1 5.28-18.627c2.22-9.354 10.618-16.31 20.645-16.31l3.461-.013c1.96-.342 3.992-.52 6.06-.52V.237h214.207L474.655 0v.023a21.097 21.097 0 0 1 14.315 5.552 21.106 21.106 0 0 1 6.72 12.957 35.595 35.595 0 0 1 4.465 17.232h.095v.036h-.095v91.099h77.883c10.701 0 19.453 8.93 19.453 19.866v473.368c0 10.926-8.752 19.867-19.453 19.867H61.961c-10.7 0-19.453-8.941-19.453-19.867V146.765c0-10.937 8.752-19.866 19.453-19.866zm240.452 71.292L161.069 48.071v194.802c0 7.843 7.158 17.54 14.256 14.256l127.088-58.937zm49.064 28.347l-9.71-4.642-20.22-7.795-26.693 11.54v412.256h56.623V226.538zM478.93 47.941L341.26 198.132l123.415 58.997c7.063 3.378 14.256-6.425 14.256-14.256V47.94zM139.75 35.787zm372.5 354.111c5.93 0 10.749 4.82 10.749 10.748 0 5.93-4.82 10.749-10.748 10.749-5.93 0-10.749-4.82-10.749-10.749s4.82-10.748 10.749-10.748zm-129.591-7.464h154.455v37.217H382.659v-37.217z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon button-up-shirt-folded";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
