// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 2361 2771","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PageTimes implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2219 1994l-221 236m-97 104l-221 236m-398 59c39 0 71 32 71 71s-32 71-71 71H165c-46 0-87-19-117-49s-49-71-49-117V165c0-46 19-87 49-117S119-1 165-1h2030c46 0 87 19 117 49s49 71 49 117v1472c0 39-32 71-71 71s-71-32-71-71V165c0-6-3-12-7-16s-10-7-16-7H166c-6 0-12 3-16 7s-7 10-7 16v2440c0 6 3 12 7 16s10 7 16 7h1117zm-571-668c-39 0-71-32-71-72s32-72 71-72h462c39 0 71 32 71 72s-32 72-71 72H711zm0-1008c-39 0-71-32-71-71s32-71 71-71h938c39 0 71 32 71 71s-32 71-71 71H711zm0 503c-39 0-71-32-71-71s32-71 71-71h938c39 0 71 32 71 71s-32 71-71 71H711zm902 608c-29-27-30-72-4-100 27-29 72-30 100-4l236 221 221-236c27-29 72-30 100-4 29 27 30 72 4 100l-221 236 236 221c29 27 30 72 4 100-27 29-72 30-100 4l-236-221-221 236c-27 29-72 30-100 4-29-27-30-72-4-100l221-236-236-221z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon page-times";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
