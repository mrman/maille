// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Cube implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill="#1F1A17" fill-rule="nonzero" d="M638.37 153.97L640 481.458c.07 17.103-10.69 31.796-26.02 37.985l.035.07-284.59 115.077a43.54 43.54 0 0 1-17.964 3.106 41.238 41.238 0 0 1-23.457-2.575L25.713 524.864l.023-.07C10.582 518.426-.059 503.284 0 485.673l1.547-323.342c-.2-3.437 0-6.922.65-10.382a39.743 39.743 0 0 1 3.035-14.351c4.394-10.642 12.886-18.496 23.008-22.571l-.023-.047L301.976 4.77c11.15-4.489 23.126-4.004 33.343.39l273.275 98.824c16.358 5.905 26.693 20.622 27.425 36.531a39.716 39.716 0 0 1 2.35 13.453zM268.279 566.522l2.043-285.263-219.168-94.737-4.63 286.42 221.755 93.58zm323.284-379.989l-244.928 94.785 1.937 284.944 244.468-91.336-1.476-288.393z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon cube";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
