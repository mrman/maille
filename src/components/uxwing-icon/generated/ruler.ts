// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Ruler implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M466.482-.012l173.506 173.506-466.494 466.494L0 466.482l12.827-12.815.118.13 32.091 32.079 4.606 4.606 4.607-4.606 12.342-12.343 4.619-4.606-4.619-4.618-32.079-32.079-.13-.118 17.28-17.28.118.13 89.446 89.458 4.618 4.594 4.618-4.618 12.343-12.343 4.583-4.594-4.583-4.607-89.458-89.457-.13-.119 17.268-17.279.13.13 48.119 48.118 4.606 4.619 4.619-4.619 12.33-12.33 4.619-4.619-4.619-4.606-48.118-48.131-.118-.118 17.256-17.268.13.119 32.091 32.09 4.582 4.595 4.63-4.618 12.331-12.343 4.618-4.606-4.618-4.595-32.067-32.067-.118-.13 17.268-17.268.118.118 89.458 89.458 4.618 4.606 4.606-4.606 12.343-12.343 4.618-4.606-4.618-4.618-89.47-89.446-.118-.13 17.28-17.268.106.118 48.142 48.131 4.583 4.606 4.63-4.63 12.343-12.343 4.606-4.594-4.606-4.607-48.119-48.118-.142-.118 17.28-17.28.118.13 32.091 32.079 4.606 4.618 4.607-4.618 12.342-12.343 4.619-4.606-4.619-4.606-32.079-32.091-.118-.119 17.268-17.267.13.118 89.434 89.457 4.618 4.619 4.619-4.619 12.342-12.33 4.607-4.619-4.607-4.606-89.469-89.458-.118-.13 17.267-17.268.119.119 48.13 48.13 4.606 4.606 4.619-4.606 12.33-12.343 4.619-4.606-4.619-4.618-48.118-48.119-.13-.13 17.268-17.268.118.118 32.103 32.091 4.594 4.607 4.618-4.607 12.331-12.342 4.63-4.607-4.63-4.618-32.067-32.079-.13-.13 17.28-17.268.118.13 89.458 89.458 4.618 4.606 4.606-4.606 12.343-12.343 4.594-4.606-4.594-4.618-89.482-89.458-.106-.118 17.28-17.28.118.13 48.13 48.119 4.595 4.618 4.618-4.618 12.343-12.343 4.618-4.606-4.618-4.607-48.119-48.13-.142-.118z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon ruler";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
