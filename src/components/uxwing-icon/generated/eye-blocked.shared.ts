// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class EyeBlocked implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M544.483 190.443c35.504 32.717 66.639 73.04 92.033 119.127L640 315.9l-3.484 6.32c-35.836 65.044-83.104 118.584-137.943 155.518-46.017 30.98-97.36 50.339-151.81 55.146l35.068-60.733c27.32-7.406 53.351-19.606 77.576-35.918 42.615-28.689 79.643-70.028 108.308-120.332-15.508-27.213-33.473-51.792-53.422-73.182l30.19-52.276zm-86.02-121.277l92.894 53.635-258.665 448.02L199.81 517.2l3.603-6.236c-21.532-8.788-42.25-19.938-61.985-33.225C86.575 440.804 39.307 387.264 3.484 322.22l-3.485-6.32 3.485-6.33c35.823-65.044 83.091-118.584 137.943-155.518 53.48-36.012 114.179-56.328 178.56-56.328 38.269 0 75.214 7.17 110.128 20.54l28.346-49.099zm-227.4 393.902l26.066-45.154c-30.957-19.701-51.508-54.296-51.508-93.698 0-61.3 49.701-111.001 111.001-111.001 18.543 0 36.036 4.547 51.402 12.602l34.406-59.599c-26.268-9.661-53.918-14.835-82.442-14.835-50.067 0-97.454 15.934-139.395 44.162-42.615 28.702-79.654 70.052-108.32 120.356 28.666 50.304 65.706 91.643 108.32 120.332 16.075 10.82 32.953 19.831 50.47 26.835zm101.894-176.506l35.056-60.733a110.622 110.622 0 0 0-30.142-10.571c-7.571 11.362-12.012 25.122-12.012 39.969 0 11.256 2.563 21.886 7.098 31.335z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon eye-blocked";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
