// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class IdCard implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M22.382 111.344h595.236c12.32 0 22.382 10.146 22.382 22.547v394.765H0V133.893c0-12.402 10.064-22.548 22.383-22.548zm36.84 52.465h430.433v62.741H59.221v-62.74zm375.3 196.113h146.257v34.749H434.521v-34.749zm-111.723 0h81.863v34.749H322.8v-34.749zm0 78.273h257.98v34.748H322.8v-34.748zm-6.118-156.533h264.098v34.748H316.681v-34.748zm-257.46 194.53v-35.623c19.063-8.48 77.422-24.213 80.186-47.552.614-5.267-11.811-25.748-14.658-35.374-3.578-5.717-5.504-7.748-5.41-14.788.048-3.968.107-7.866.674-11.68.732-4.867.579-5.02 3.13-8.942 2.657-4.075 1.512-18.957 1.512-24.567 0-55.867 97.89-55.879 97.89 0 0 7.063-1.617 20.044 2.221 25.595 1.866 2.705 1.524 3.023 2.15 6.33.827 4.323.886 8.753.957 13.264.094 7.04-1.831 9.071-5.41 14.788-3.472 10.122-16.665 29.245-15.543 34.973 4.193 21.307 58.335 35.634 75.379 43.217v40.359H59.22zM516.042 163.81h64.737v62.74h-64.737v-62.74z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon id-card";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
