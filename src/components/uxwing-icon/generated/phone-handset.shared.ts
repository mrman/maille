// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PhoneHandset implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M17.776 107.907c49.11 33.733 97.832 67.666 138.958 111.356-23.575 104.115 70.512 187.302 156.32 236.318 33.58 19.181 48.427 32.634 85.82 26.174L546.138 626.04C269.012 719.112-42.013 328.623 17.776 107.908zm419.06 336.748l32.303-32.811c9.32-9.473 24.697-9.591 34.17-.272l119.104 117.26c9.473 9.32 9.59 24.698.272 34.17l-32.316 32.812c-9.319 9.473-24.697 9.59-34.17.272L437.097 478.825c-9.473-9.32-9.591-24.697-.26-34.17zM46.808 38.894l23.445-28.465c10.051-12.2 28.264-13.96 40.465-3.91l121.927 100.443c12.201 10.063 13.95 28.264 3.91 40.476l-23.457 28.454c-10.052 12.212-28.264 13.96-40.465 3.91L50.718 79.358c-12.201-10.051-13.973-28.264-3.91-40.465z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon phone-handset";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
