// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class SpeechBubble implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M309.583 485.368c-14.327 56.682-31.69 107.623-84.71 154.644 101.375-25.925 179.636-78.344 237.074-154.644M479.263-.012H160.739c-70.915 0-128.931 58.016-128.931 128.931v227.519c0 70.914 58.016 128.931 128.93 128.931h318.525c70.914 0 128.93-58.017 128.93-128.931V128.919c0-70.914-58.016-128.931-128.93-128.931zM168.38 182.896c29.161 0 52.819 23.658 52.819 52.82 0 29.161-23.658 52.819-52.82 52.819-29.161 0-52.819-23.658-52.819-52.82 0-29.161 23.658-52.819 52.82-52.819zm303.24 0c29.161 0 52.819 23.658 52.819 52.82 0 29.161-23.658 52.819-52.82 52.819-29.161 0-52.819-23.658-52.819-52.82 0-29.161 23.658-52.819 52.82-52.819zm-151.62 0c29.161 0 52.819 23.658 52.819 52.82 0 29.161-23.658 52.819-52.82 52.819-29.161 0-52.819-23.658-52.819-52.82 0-29.161 23.658-52.819 52.82-52.819z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon speech-bubble";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
