// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class MaleFemaleCouple implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M-.012 568.672L0 491.155c37.087-16.488 182.211-52.902 191.341-99.273 4.098-20.87-5.327-30.945-15.614-47.421l-33.154-52.844c-7.784-12.425-13.619-16.854-13.418-32.173.118-8.622.248-17.115 1.488-25.418 1.571-10.594 2.882-10.913 8.433-19.453 3.733-5.728 3.308-7.44 3.308-20.622v-32.823c0-37.926 13.972-61.655 37.512-75.084 34.477-19.69 104.363-19.666 138.64.402 23.103 13.523 36.792 37.17 36.792 74.682v32.823c0 14.764-.52 15.154 4.819 22.878 4.063 5.87 4.972 6.579 6.342 13.76 1.796 9.402 1.925 19.028 2.055 28.855.213 15.319-5.61 19.748-13.405 32.173l24.06 15.036 3.991-5.847c8.008-11.657 13.524-19.736 13.76-38.776.095-.956.13-1.937.118-2.917h-.118c-.059-5.74-.153-11.362-.401-15.815-.343-6.06-.957-12.083-2.103-18.06-2.126-11.161-3.685-14.102-8.397-20.94-.414-.721-.874-1.43-1.347-2.127l-.012.012-.638-.91-.437-.649-.011.012-.06-.083-.047.024c-.023-.154.225-.366.225-3.012l.012-3.614h.118v-32.823c0-10.985-1.016-21.166-2.965-30.556 66.544-45.284 158.494-.508 177.416 134.387 18.154 129.923 96.084 125.257-25.536 125.257h-48.555c-.177 33.071-5.35 49.985 28.984 68.564 34.253 18.532 126.805 28.087 126.805 75.072v30.095c0 2.397-1.984 4.381-4.405 4.381H513.858v-86.942c0-29.93-41.505-35.575-63.39-42.343-19.997-6.165-43.572-13.429-64.242-21.142.461-8.102-.212-17.173-.283-27.685h-49.961c-.839-1.287-1.394-2.54-1.642-3.803-1.193-6.107 1.819-11.457 6.035-18 .496-.65.969-1.323 1.418-2.02l-.898-.579v-.012l.862.544 3.237-5.044 1.157-1.689-.07-.047 31.948-50.93 1.276-1.866-24.166-15.154-33.154 52.844c-10.287 16.476-19.713 26.551-15.602 47.421 9.118 46.371 141.864 72.462 178.95 88.962v87.828H-.01z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon male-female-couple";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
