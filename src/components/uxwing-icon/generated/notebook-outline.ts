// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 4480 5341","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class NotebookOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M588 1015c0 69-56 125-125 125s-125-56-125-125V290c0-80 33-153 85-205C476 32 548 0 628 0h3562c80 0 152 33 205 85 53 53 85 125 85 205v4760c0 80-33 152-85 205-53 53-125 85-205 85H628c-80 0-152-33-205-85-52-52-85-125-85-205v-691c0-69 56-124 124-124 69 0 124 56 124 124v691c0 11 5 21 12 29 7 7 18 12 29 12h2777V249H627c-11 0-21 5-29 12-7 7-12 18-12 29v725zM93 3899c-51 0-92-56-92-125s41-125 92-125h670c51 0 92 56 92 125s-41 125-92 125H93zm0-2208c-51 0-92-56-92-125s41-125 92-125h670c51 0 92 56 92 125s-41 125-92 125H93zm0 736c-51 0-92-56-92-125s41-125 92-125h670c51 0 92 56 92 125s-41 125-92 125H93zm0 736c-51 0-92-56-92-125s41-125 92-125h670c51 0 92 56 92 125s-41 125-92 125H93zm3563 1928h535c11 0 21-5 29-12 8-8 12-18 12-29V290c0-11-5-21-12-29-7-7-18-12-29-12h-535v4842z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon notebook-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
