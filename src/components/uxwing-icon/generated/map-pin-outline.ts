// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 4066 5416","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class MapPinOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2094 5150c278-177 528-391 746-630 473-518 792-1151 920-1772 127-613 67-1214-214-1678-80-133-180-255-299-364-341-311-732-459-1125-463-413-3-832 152-1202 447-158 126-288 271-389 431-260 409-339 917-253 1448 87 539 343 1103 751 1614 284 356 641 687 1066 966zm-60-4218c311 0 593 126 796 330 204 204 330 485 330 796s-126 593-330 796c-204 204-485 330-796 330s-593-126-796-330c-204-204-330-485-330-796s126-593 330-796c204-204 485-330 796-330zm634 492c-162-162-387-263-634-263-248 0-472 100-634 263-162 162-263 387-263 634 0 248 100 472 263 634 162 162 387 263 634 263 248 0 472-100 634-263 162-162 263-387 263-634 0-248-100-472-263-634zm353 3260c-247 270-533 512-856 708-39 29-94 33-138 5-476-303-875-667-1190-1062-433-544-707-1147-800-1728-95-589-6-1155 287-1617 116-182 263-348 444-491C1181 170 1654-4 2124-1c453 4 900 172 1288 526 136 124 250 264 343 418 315 519 383 1182 245 1852-137 663-476 1337-978 1887z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon map-pin-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
