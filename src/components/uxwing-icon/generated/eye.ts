// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Eye implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M498.573 158.152c54.84 36.934 102.108 90.474 137.943 155.518L640 320l-3.484 6.32c-35.835 65.044-83.103 118.584-137.943 155.518-53.493 36.012-114.179 56.327-178.585 56.327-64.383 0-125.08-20.315-178.561-56.327C86.576 444.904 39.308 391.364 3.484 326.32L0 320l3.484-6.33c35.823-65.044 83.092-118.584 137.943-155.518 53.481-36.012 114.179-56.328 178.561-56.328 64.406 0 125.092 20.316 178.585 56.328zm-181.951 59.162c37.028 0 69.828 18.154 89.99 46.04-16.206-22.418-40.572-38.528-68.742-43.997-7.57 11.363-12.012 25.123-12.012 39.97 0 38.953 30.544 70.524 68.222 70.524 12.082 0 23.433-3.248 33.283-8.941.154 2.445.249 4.913.249 7.405 0 61.3-49.702 110.99-110.99 110.99-61.3 0-111.001-49.69-111.001-110.99s49.7-111 111-111zm90.06 46.123c.402.567.803 1.145 1.205 1.712-.402-.567-.803-1.145-1.205-1.712zm1.311 1.878zm8.055 13.653zm.993 2.044zm4.346 10.606c.213.602.425 1.217.626 1.819-.2-.614-.413-1.217-.626-1.819zm.72 2.114zm3.509 13.595zM567.716 320c-28.666-50.303-65.694-91.654-108.308-120.356-41.954-28.228-89.328-44.162-139.42-44.162-50.067 0-97.453 15.934-139.394 44.162-42.615 28.702-79.655 70.052-108.32 120.356 28.665 50.304 65.705 91.655 108.32 120.332 41.941 28.24 89.327 44.174 139.395 44.174 50.09 0 97.465-15.933 139.419-44.174C502.022 411.655 539.05 370.304 567.716 320z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon eye";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
