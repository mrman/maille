// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Padlock implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M84.769 291.854h46.666v-98.541c0-53.15 21.756-101.482 56.764-136.502v-.047C223.22 21.744 271.562-.012 324.75-.012c53.15 0 101.481 21.756 136.501 56.776l.047.047c35.009 35.02 56.765 83.352 56.765 136.502v98.541h37.17c8.28 0 15.059 6.78 15.059 15.06v318.038c0 8.28-6.78 15.06-15.06 15.06H84.77c-8.28 0-15.06-6.78-15.06-15.06V306.913c0-8.28 6.78-15.059 15.06-15.059zm121.773 0h236.4v-99.615c0-32.505-13.299-62.068-34.7-83.494l-.024.024c-21.414-21.425-50.977-34.737-83.47-34.737-32.504 0-62.067 13.3-83.505 34.713-21.401 21.426-34.7 50.99-34.7 83.494v99.615zM325.705 465.88l23.858 110.104-65.398.272 19.193-111.58c-17.87-5.729-30.827-22.477-30.827-42.249 0-24.485 19.843-44.327 44.339-44.327 24.473 0 44.327 19.843 44.327 44.327 0 21.461-15.248 39.367-35.492 43.453z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon padlock";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
