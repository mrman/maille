// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class CheckMark implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M23.055 388.257C8.115 373.907.39 354.82 0 335.591c-.378-19.217 6.567-38.61 20.906-53.552 14.339-14.93 33.425-22.665 52.642-23.043 19.229-.402 38.61 6.531 53.564 20.894L256.22 404.12 513.255 77.612l9.213 7.015-9.225-7.05a11.617 11.617 0 0 1 2.504-2.434c15.284-13.24 34.477-19.229 53.245-18.2v-.025l.91.083c18.873 1.252 37.31 9.638 50.917 24.815 13.855 15.45 20.115 35.068 19.075 54.237h.024l-.083.922c-1.228 18.52-9.33 36.638-24.012 50.162L307.351 562.626l.024.023a11.41 11.41 0 0 1-1.937 1.914c-14.446 12.685-32.682 18.91-50.824 18.59-18.284-.318-36.532-7.263-50.74-20.905L23.055 388.258z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon check-mark";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
