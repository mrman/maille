// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Processor implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M149.494 640.012h57.426v-69.78h-57.426v69.78zm-31.453-538.424h389.367c13.335 0 24.26 10.913 24.26 24.248v390.666c0 13.335-10.925 24.249-24.26 24.249H118.04c-13.347 0-24.272-10.914-24.272-24.249V125.836c0-13.335 10.925-24.248 24.272-24.248zm102.533 113.458h184.29c8.055 0 14.646 6.602 14.646 14.658v182.943c0 8.055-6.59 14.646-14.646 14.646h-184.29c-8.055 0-14.634-6.591-14.634-14.646V229.704c0-8.056 6.579-14.658 14.634-14.658zm419.438 126.746v47.705h-69.78v-47.705h69.78zm0-91.288v47.705h-69.78v-47.706h69.78zm0 182.576v57.426h-69.78v-57.425h69.78zm0-283.586v57.426h-69.78v-57.426h69.78zM-.012 341.792v47.706h69.78v-47.706H-.012zm0-91.288v47.705h69.78v-47.705H-.012zm0 182.577v57.426h69.78V433.08H-.012zm0-283.586v57.426h69.78v-57.426H-.012zM341.79-.011h47.705v69.78h-47.705V-.01zm-91.289 0h47.706v69.78h-47.706V-.01zm182.577 0h57.426v69.78H433.08V-.01zm-283.586 0h57.426v69.78h-57.426V-.01zm192.298 640.024h47.706v-69.78H341.79v69.78zm-91.288 0h47.705v-69.78h-47.705v69.78zm182.577 0h57.426v-69.78H433.08v69.78z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon processor";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
