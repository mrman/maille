// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 319896 333333","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class ProgressBars implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M21966 0h275981c7810 0 14198 6394 14198 14198v77069c0 7804-6394 14198-14198 14198h-41409v14516h41409c7810 0 14198 6397 14198 14198v77069c0 7801-6397 14198-14198 14198h-41409v41191h21289v21235h42070v24413h-42070v21046h-66974v-21046h-97388v21046H46491v-21046H1v-24413h46490v-21235h21272v-41191H21966c-7802 0-14198-6389-14198-14198v-77069c0-7810 6388-14198 14198-14198h45797v-14516H21966c-7804 0-14198-6389-14198-14198V14198C7768 6388 14156 0 21966 0zm21018 151733h16211v41963H42984v-41963zM265808 39884c7096 0 12849 5754 12849 12849s-5753 12849-12849 12849-12849-5754-12849-12849c0-7096 5753-12849 12849-12849zm-48955 0c7095 0 12849 5754 12849 12849s-5754 12849-12849 12849c-7096 0-12849-5754-12849-12849 0-7096 5754-12849 12849-12849zm-84226-8133h16211v41963h-16211V31751zm-44821 0h16211v41963H87806V31751zm-44822 0h16211v41963H42984V31751zm222824 128115c7096 0 12849 5754 12849 12849 0 7096-5753 12849-12849 12849s-12849-5754-12849-12849 5753-12849 12849-12849zm-48955 0c7095 0 12849 5754 12849 12849 0 7096-5754 12849-12849 12849-7096 0-12849-5754-12849-12849s5754-12849 12849-12849zm-84226-8133h16211v41963h-16211v-41963zm-44821 0h16211v41963H87806v-41963zm156534 133041c8400 0 15212 6811 15212 15212 0 8400-6811 15212-15212 15212-8400 0-15212-6811-15212-15212s6812-15212 15212-15212zm-164362 0c8401 0 15212 6811 15212 15212 0 8400-6811 15212-15212 15212-8400 0-15212-6811-15212-15212s6811-15212 15212-15212zm33487 3099h97388v-21235h21272v-41191H92177v41191h21289v21235zM92176 119981h139948v-14516H92176v14516z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon progress-bars";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
