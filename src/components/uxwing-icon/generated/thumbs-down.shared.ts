// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ThumbsDown implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M20.965 364.15h142.502c11.527 0 20.953-9.414 20.953-20.953V64.501c0-11.528-9.426-20.965-20.953-20.965H20.965C9.437 43.536 0 52.973 0 64.501v278.696c0 11.54 9.437 20.953 20.965 20.953zm307.48 209.07c11.067 55.973 102.71 4.406 108.734-85.82 1.842-27.626-1.04-59.753-7.807-95.647h130.986c54.473-2.126 102.025-41.162 68.434-105.214 7.677-27.91 8.822-60.662-11.989-73.595 2.599-44.08-9.602-71.363-32.374-92.93-1.524-22.028-6.213-41.635-16.843-56.647-17.598-24.839-31.89-18.886-59.587-18.886H286.835c-35.068 0-54.154 9.65-77.08 38.363v246.393c65.93 17.823 100.974 108.037 118.69 167.258v76.725z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon thumbs-down";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
