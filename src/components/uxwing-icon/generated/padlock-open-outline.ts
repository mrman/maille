// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 3045","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PadlockOpenOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M1091 2132l-104 271h365l-96-275c61-31 103-95 103-168 0-104-85-189-189-189s-189 85-189 189c0 76 45 142 110 172zM219 1130h1343V917c0-252 99-481 258-647C1980 103 2201-1 2444-1c242 0 459 102 617 268 159 166 259 396 266 648l6 242c1 44-33 80-77 81s-80-33-81-77l-6-242c-6-212-89-404-222-542-129-135-306-219-503-219-199 0-380 85-511 222-132 138-214 328-214 537v213h401c60 0 115 25 155 64 40 40 64 94 64 155v1477c0 60-25 115-64 155-40 40-94 64-155 64H218c-60 0-115-25-155-64-40-40-64-94-64-155V1349c0-60 25-115 64-155 40-40 94-64 155-64zm1902 158H219c-17 0-32 7-43 18s-18 26-18 43v1477c0 17 7 32 18 43s26 18 43 18h1902c17 0 32-7 43-18s18-26 18-43V1349c0-17-7-32-18-43s-26-18-43-18z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon padlock-open-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
