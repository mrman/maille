// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 4159 4098","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Rocket implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M520 2919c39-30 94-23 125 16 30 39 23 94-16 125l-448 348-4 469 550-31 299-475c26-41 81-54 122-28s54 81 28 122l-321 511c-15 25-41 42-72 44l-690 39h-5c-49 0-88-40-88-89l5-599c-2-29 10-58 34-77l481-374zm837-1093c223-668 524-1141 948-1439C2745 77 3310-40 4050 14c51 4 90 46 91 96 72 802-84 1370-416 1801-319 414-798 695-1392 930v783c0 38-22 72-54 88l-605 374c-46 28-106 14-135-32-8-12-12-25-14-39-134-1024-276-1073-1114-1363-100-35-208-72-281-98-51-18-78-74-60-125 4-11 9-21 16-29l399-569c20-29 52-43 85-42l786 38zm1713-754c-54-54-129-87-211-87s-157 33-211 87-87 129-87 211 33 157 87 211 129 87 211 87 157-33 211-87 87-129 87-211-33-157-87-211z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon rocket";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
