// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Sliders implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M640 62.977v55.17c0 6.756-6.354 12.296-14.102 12.296h-218.26c-14.882 30.992-46.56 52.394-83.233 52.394-36.65 0-68.327-21.402-83.21-52.394H14.104C6.366 130.443 0 124.89 0 118.147v-55.17c0-6.756 6.354-12.295 14.103-12.295h227.093c14.882-30.993 46.56-52.383 83.21-52.383 36.673 0 68.339 21.39 83.22 52.383h218.27c7.76 0 14.103 5.527 14.103 12.295zm-380.45 526.33c-14.882 30.992-46.571 52.382-83.233 52.382-36.662 0-68.34-21.39-83.222-52.383H14.102C6.366 589.306 0 583.766 0 577.011v-55.17c0-6.744 6.354-12.296 14.102-12.296h78.993c14.882-30.992 46.56-52.394 83.222-52.394 36.662 0 68.35 21.402 83.221 52.394h366.36c7.76 0 14.102 5.54 14.102 12.296v55.17c0 6.768-6.367 12.295-14.103 12.295H259.55zm285.606-225.913c-14.87 30.993-46.56 52.394-83.222 52.394-36.673 0-68.35-21.401-83.233-52.394H14.102C6.366 363.394 0 357.854 0 351.1V295.94c0-6.745 6.354-12.296 14.102-12.296h364.611c14.87-30.98 46.548-52.382 83.222-52.382 36.662 0 68.34 21.401 83.221 52.382h80.741c7.76 0 14.103 5.54 14.103 12.296v55.158c0 6.768-6.355 12.295-14.103 12.295h-80.74z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon sliders";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
