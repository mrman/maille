// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ConnectedNodes implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M199.715 241.137l205.833-101.564a118.488 118.488 0 0 1-1.984-21.72C403.564 52.571 456.5-.367 521.782-.367c65.292 0 118.23 52.938 118.23 118.218 0 65.292-52.938 118.218-118.23 118.218-25.808 0-49.702-8.256-69.155-22.3L236.27 320.533c.107 2.055.166 4.146.166 6.224 0 3.626-.177 7.217-.508 10.772l199.018 98.198c21.118-19.736 49.489-31.796 80.67-31.796 65.28 0 118.218 52.926 118.218 118.218 0 65.28-52.938 118.218-118.218 118.218S397.398 587.43 397.398 522.148c0-2.492.083-4.972.225-7.417L196.36 415.423c-20.835 18.378-48.178 29.551-78.143 29.551-65.292 0-118.23-52.937-118.23-118.218 0-65.292 52.938-118.218 118.23-118.218 31.595 0 60.296 12.402 81.497 32.6z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon connected-nodes";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
