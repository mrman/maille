// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class LifeSaver implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M319.988.012c176.707 0 320 143.293 320 319.988 0 176.707-143.293 320-320 320C143.293 640 0 496.707 0 320 0 143.305 143.293.012 319.988.012zm0 150.91c20.374 0 39.922 3.615 58.005 10.218l74.48-74.47C413.392 64.418 368.167 51.71 319.989 51.71c-48.686 0-94.36 12.98-133.738 35.682l74.304 74.292c18.496-6.945 38.528-10.76 59.434-10.76zm161.057 117.499c5.209 16.264 8.032 33.603 8.032 51.58 0 20.917-3.815 40.949-10.772 59.445l74.304 74.304c22.69-39.378 35.681-85.052 35.681-133.75 0-45.91-11.55-89.139-31.89-126.935l-75.355 75.355zM370.079 481.54c-15.827 4.902-32.658 7.548-50.091 7.548-17.977 0-35.316-2.823-51.58-8.031L193.053 556.4c37.796 20.35 81.025 31.902 126.935 31.902 45.402 0 88.159-11.303 125.647-31.217l-75.556-75.544zm-208.94-103.536c-6.614-18.095-10.229-37.63-10.229-58.005 0-17.421 2.646-34.252 7.548-50.08l-75.556-75.555C63 231.853 51.697 274.61 51.697 320c0 48.178 12.72 93.39 34.96 132.474l74.482-74.47zM437.048 320c0 5.764-.425 11.433-1.228 16.973a85.09 85.09 0 0 1-.39 2.504l-.012.07c-4.854 28.879-20.292 54.167-42.166 71.742l-.165.13c-.567.46-1.146.909-1.713 1.358l-.342.26a117.45 117.45 0 0 1-14.375 9.401l-.011.012c-.685.378-1.37.756-2.067 1.11l-.248.142c-.697.354-1.394.72-2.09 1.063l-.036.024a116.62 116.62 0 0 1-16.17 6.614l-.2.059c-.757.248-1.524.484-2.292.709l-.118.035c-.78.236-1.571.46-2.362.685l-.06.012a116.466 116.466 0 0 1-14.822 3.035l-.402.06c-.732.106-1.476.188-2.22.283l-.426.047c-.732.083-1.464.154-2.208.225l-.508.047-2.209.177-.378.024c-.78.047-1.559.094-2.35.13-1.82.094-3.65.13-5.493.13-2.468 0-4.925-.083-7.358-.237-28.063-1.748-53.445-13.382-72.71-31.453l-.011-.012a117.206 117.206 0 0 1-13.122-14.563l-.154-.2c-.472-.615-.945-1.253-1.406-1.879l-.07-.106a113.04 113.04 0 0 1-1.37-1.925l-.071-.118a117.413 117.413 0 0 1-7.595-12.556l-.118-.224c-.33-.638-.65-1.287-.969-1.925l-.177-.366a69.505 69.505 0 0 1-.91-1.902l-.224-.46c-.295-.638-.578-1.288-.862-1.926l-.153-.354c-.296-.662-.58-1.335-.863-2.008l-.165-.413c-.272-.662-.543-1.335-.815-2.008l-.035-.107a117.421 117.421 0 0 1-3.142-9.212l-.106-.378c-.201-.697-.39-1.382-.58-2.079l-.13-.449a132.2 132.2 0 0 1-.519-2.067l-.13-.52a60.113 60.113 0 0 1-.46-1.984l-.142-.65a105.04 105.04 0 0 1-.437-2.055l-.047-.26c-.473-2.409-.874-4.842-1.193-7.31A117.64 117.64 0 0 1 202.939 320c0-1.843.059-3.661.13-5.469 1.7-36.992 20.587-69.508 48.85-89.753 39.946-25.772 96.085-25.795 136.054-.059 27.827 19.89 46.595 51.674 48.839 87.923l.047-.036c.118 2.433.19 4.89.19 7.394z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon life-saver";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
