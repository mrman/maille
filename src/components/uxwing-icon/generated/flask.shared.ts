// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Flask implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M400.54 308.496l114.427 134.517.024.024.023-.024c16.737 19.772 39.355 51.686 52.584 83.54 7.925 19.1 12.59 38.446 10.96 55.832-1.795 19.323-10.854 35.764-30.65 46.536-9.85 5.362-22.287 9.165-37.748 10.984-.496.06-.992.083-1.477.083v.024H131.575c-.78 0-1.535-.071-2.267-.201-15.26-1.866-27.508-5.622-37.182-10.867-20.09-10.89-29.138-27.472-30.756-46.96-1.453-17.47 3.496-36.97 11.74-56.305 14.126-33.083 38.21-66.272 55.17-86.375l110.482-130.82v-159.25a106.4 106.4 0 0 0-1.287-5.245c-.614-2.232-1.3-4.453-2.067-6.65-3.957-11.41-10.063-22.736-18.32-33.661-4.358-5.776-3.2-13.997 2.564-18.355a13.114 13.114 0 0 1 7.901-2.646v-.023h43.713c-10.417-6.461-17.35-17.989-17.35-31.146 0-20.233 16.406-36.639 36.626-36.639 20.233 0 36.627 16.406 36.627 36.639 0 13.157-6.934 24.685-17.351 31.146h102.887c7.24 0 13.122 5.882 13.122 13.122 0 3.555-1.405 6.768-3.697 9.13-5.244 5.988-9.567 12.768-13.098 20.232-3.52 7.43-6.307 15.638-8.493 24.497v158.86zm-69.072 80.068h86.14l-59.293-65.895a13.08 13.08 0 0 1-4.028-9.449V148.088c0-1.003.119-2.02.355-3.023 2.61-11.008 6.118-21.426 10.7-31.11.816-1.713 1.666-3.39 2.54-5.056h-96.45a142.28 142.28 0 0 1 8.693 19.925c.957 2.776 1.82 5.54 2.575 8.304a138.246 138.246 0 0 1 1.772 7.193c.354 1.193.543 2.456.543 3.767v165.156h-.024c0 2.989-1.015 6-3.106 8.457L169.844 456.218c-15.791 18.72-38.15 49.465-51.06 79.714-5.374 12.614-20.008 44.823-2.68 54.213 6.85 3.72 15.956 6.425 27.685 7.89h364.245c11.634-1.453 20.622-4.134 27.378-7.807 14.504-7.902-4.618-43.619-8.776-53.623a202.348 202.348 0 0 0-5.433-11.965c-.744.166-1.547.26-2.315.26H402.17c-6.614 0-12.071-6.142-12.071-13.63v-.012c0-7.488 5.433-13.63 12.071-13.63h103.749c-6.366-10.004-13.051-19.335-19.323-27.413-.579.094-1.205.153-1.784.153h-115.04c-6.627 0-2.08-6.142-2.08-13.642 0-7.5 5.434-13.63 12.083-13.63h84.226l-24.248-28.5a10.84 10.84 0 0 1-4.985 1.228h-103.3c-6.614 0-12.083-6.13-12.083-13.63 0-7.5 5.434-13.63 12.083-13.63zM196.68 133.914c13.902 0 25.182 11.28 25.182 25.182 0 13.914-11.28 25.182-25.182 25.182-13.901 0-25.181-11.268-25.181-25.182 0-13.901 11.28-25.181 25.181-25.181zM177.215-.011c24.661 0 44.646 19.984 44.646 44.634 0 24.662-19.985 44.646-44.646 44.646-24.65 0-44.635-19.984-44.635-44.646 0-24.65 19.985-44.634 44.635-44.634z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon flask";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
