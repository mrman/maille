// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class AntennaTransmitting implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M358.989 370.044v264.015h-77.977V370.044c-25.619-13.866-43.017-40.973-43.017-72.154 0-45.284 36.721-82.005 82.005-82.005s82.005 36.72 82.005 82.005c0 31.181-17.398 58.288-43.016 72.154zM204.794 64.087c15.19-7.488 21.414-25.878 13.925-41.056-7.488-15.177-25.866-21.413-41.043-13.925C115.69 39.686 67.49 90.26 37.029 149.824 14.529 193.809 1.7 242.754.154 292.149c-1.559 49.808 8.268 100.123 31.11 146.435C59.4 495.632 107.093 546.373 177.31 582.29c15.047 7.69 33.484 1.724 41.173-13.323 7.69-15.047 1.725-33.485-13.323-41.174-57.485-29.398-96.284-70.441-118.927-116.328-18.26-37.04-26.114-77.386-24.862-117.39 1.264-40.407 11.752-80.435 30.154-116.4 24.638-48.19 63.473-89.032 113.269-113.588zM462.325 9.106c-15.177-7.488-33.556-1.252-41.044 13.925-7.488 15.178-1.264 33.568 13.926 41.056 49.796 24.555 88.63 65.398 113.269 113.588 18.401 35.965 28.89 75.993 30.153 116.399 1.252 40.004-6.602 80.351-24.862 117.391-22.642 45.886-61.442 86.93-118.927 116.328-15.047 7.69-21.012 26.126-13.323 41.174 7.69 15.047 26.126 21.012 41.174 13.323 70.217-35.918 117.91-86.659 146.045-143.706 22.843-46.312 32.67-96.628 31.11-146.435-1.547-49.395-14.374-98.34-36.874-142.325C572.51 90.26 524.309 39.684 462.325 9.106zm-31.11 134.884c-11.434-8.232-27.379-5.634-35.611 5.8-8.233 11.432-5.634 27.378 5.799 35.61 4.606 3.319 9.13 7.04 13.56 11.114 25.89 23.87 41.244 55.702 44.468 89.375 3.26 33.981-5.787 69.982-28.736 101.836-3.898 5.41-8.386 10.89-13.465 16.406-9.555 10.382-8.894 26.552 1.5 36.107 10.382 9.555 26.552 8.882 36.107-1.5a225.907 225.907 0 0 0 17.268-21.201c30.531-42.39 42.532-90.64 38.138-136.454-4.417-46.111-25.37-89.623-60.674-122.175-5.551-5.114-11.67-10.099-18.355-14.918zm-192.618 41.422c11.434-8.233 14.032-24.178 5.8-35.61-8.233-11.434-24.178-14.033-35.611-5.8-6.685 4.819-12.803 9.803-18.355 14.917-35.303 32.552-56.256 76.064-60.674 122.175-4.393 45.816 7.607 94.064 38.139 136.454 5.374 7.453 11.126 14.54 17.268 21.201 9.555 10.382 25.724 11.056 36.106 1.5 10.394-9.555 11.056-25.724 1.5-36.106-5.078-5.516-9.567-10.997-13.464-16.406-22.95-31.855-31.997-67.855-28.737-101.836 3.224-33.674 18.579-65.505 44.47-89.375 4.428-4.075 8.952-7.796 13.558-11.114z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon antenna-transmitting";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
