// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 2766 2766","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class CalendarBlankOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M1837 106c0-59 58-106 130-106s130 48 130 106v466c0 59-58 106-130 106s-130-48-130-106V106zm-1171 0C666 47 724 0 796 0s130 48 130 106v466c0 59-58 106-130 106s-130-48-130-106V106zm-522 914h2478V483c0-18-7-35-19-46-12-12-28-19-46-19h-237c-40 0-72-32-72-72s32-72 72-72h237c58 0 110 24 148 62s62 91 62 148v2073c0 58-24 110-62 148s-91 62-148 62H211c-58 0-110-24-148-62s-62-91-62-148V484c0-58 24-110 62-148s91-62 148-62h254c40 0 72 32 72 72s-32 72-72 72H211c-18 0-35 7-47 19s-19 28-19 47v537zm2478 144H144v1392c0 18 7 35 19 47s28 19 47 19h2346c18 0 35-7 47-19s19-28 19-47V1164zM1135 417c-40 0-72-32-72-72s32-72 72-72h484c40 0 72 32 72 72s-32 72-72 72h-484z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon calendar-blank-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
