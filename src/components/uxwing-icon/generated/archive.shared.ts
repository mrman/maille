// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Archive implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M5.976 33.426h628.06c3.283 0 5.976 2.704 5.976 5.988v147.84c0 3.295-2.693 5.976-5.977 5.976H5.976c-3.283 0-5.988-2.68-5.988-5.976V39.414c0-3.284 2.705-5.988 5.988-5.988zM37.62 225.039h564.775c2.74 0 5.007 2.232 5.007 5.008v371.52c0 2.763-2.267 5.02-5.007 5.02H37.619c-2.752 0-5.008-2.257-5.008-5.02v-371.52a5.01 5.01 0 0 1 5.008-5.008zm196.786 61.146h171.203c18.378 0 33.437 15.036 33.437 33.414 0 18.39-15.06 33.425-33.437 33.425H234.405c-18.39 0-33.438-15.035-33.438-33.425 0-18.378 15.048-33.414 33.438-33.414z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon archive";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
