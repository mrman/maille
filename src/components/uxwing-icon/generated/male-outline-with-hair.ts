// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 4362 5192","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class MaleOutlineWithHair implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M518 906C1262 85 2121-361 2765 369c169 8 320 64 446 183 265 249 306 715 185 1085v323c82 54 135 155 159 270 16 74 20 156 12 234-8 81-28 158-60 222-46 91-116 155-211 169-93 99-184 205-265 307-89 112-165 218-218 305-41 67-28 111-13 161 7 25 15 51 20 79 163 35 310 63 442 88 839 157 1100 206 1100 1298 0 56-45 101-101 101H101c-56 0-101-45-101-101 0-1092 260-1141 1100-1298 133-25 282-53 445-88 6-22 13-42 20-62 17-48 34-96-16-177-53-87-129-193-218-305-81-102-172-208-265-307-95-14-165-78-211-169-32-63-52-141-60-222-7-78-3-160 12-234 24-115 76-215 157-269l-109-65c-29-323 56-884-338-990zm2677 645c-18-133-53-245-102-338-63-79-155-146-274-233l-13-10c-25-18-50-36-76-52-398 258-591 198-912 251-291 48-527 156-651 354v502c0 48-33 88-78 98-40 12-70 73-86 149-12 55-15 116-9 174 5 56 18 108 39 149 19 37 43 62 71 62h6c29 0 56 12 74 31 107 112 212 233 304 349 93 116 174 230 232 326 99 162 66 255 34 347-9 27-19 53-19 79 0 51-38 93-86 100-194 44-363 75-512 103-687 129-918 172-934 1000h3958c-16-828-247-871-934-1000-151-28-323-60-520-105-47-10-78-52-78-98 0-39-10-73-19-106-28-94-52-178 34-320 58-96 140-210 232-326s197-237 304-348c21-23 51-33 80-31h1c28-1 52-25 71-62 21-41 34-93 39-149 6-58 2-119-9-174-16-77-46-139-88-150-46-12-76-53-76-98v-473z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon male-outline-with-hair";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
