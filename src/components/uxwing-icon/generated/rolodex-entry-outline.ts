// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 2536","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class RolodexEntryOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M229 146c-22 0-43 9-58 24s-24 36-24 58v2088c0 16 10 31 25 44 22 18 51 29 76 29h260v-204c0-40 33-73 73-73h392c40 0 73 33 73 73v204h1289v-204c0-40 33-73 73-73h389c40 0 73 33 73 73v204h245c19 0 37-9 51-22 14-14 22-32 22-51V520c0-20-8-38-21-52-13-13-31-21-52-21H1761c-40 0-73-33-73-73 0-58 0-127-18-173-12-32-38-56-90-56H229zm1632 1435h1067v142H1861v-142zm0-741h1067v142H1861V840zm0 371h1067v142H1861v-142zM716 757v122c-62 16-59 184 10 184 21 0 119 141 133 165 28 46 0 63 0 94-299 68-386 98-386 401h1046c0-303-86-333-386-401 0-37-25-53 0-94 28-46 79-110 131-165 72 1 74-168 11-184V757c0-294-560-294-560 0zM67 68c41-41 99-67 161-67h1351c128 0 193 64 226 151 18 48 24 101 27 152h1282c61 0 115 25 155 64 40 40 64 94 64 155v1796c0 60-25 114-65 154s-95 65-154 65h-318c-40 0-73-33-73-73v-204h-243v204c0 40-33 73-73 73H972c-40 0-73-33-73-73v-204H653v204c0 40-33 73-73 73H247c-58 0-122-24-170-63-46-38-78-92-78-156V231c0-63 26-120 67-161z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon rolodex-entry-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
