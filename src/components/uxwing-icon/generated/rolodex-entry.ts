// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class RolodexEntry implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M31.359 80.08h270.948c20.197 0 36.709 16.512 36.709 36.709v27.366h271.62c16.159 0 29.375 13.205 29.375 29.363v357.04c0 16.146-13.216 29.363-29.374 29.363h-63.639v-55.537H468.49v55.537H180.627v-55.537H102.12v55.537h-67.43c-16.146 0-34.702-13.217-34.702-29.363V111.45c0-17.256 14.115-31.37 31.37-31.37zm337.315 145.69h225.664v33.285H368.674V225.77zm0 86.86h225.664v33.284H368.674V312.63zm0 86.86h225.664v33.271H368.674V399.49zM47.316 432.76v-42.685c22.842-10.158 92.765-29.008 96.06-56.966.756-6.307-14.138-30.85-17.552-42.39-4.287-6.838-6.602-9.283-6.484-17.717.06-4.748.13-9.425.815-13.996.862-5.835.697-6.012 3.744-10.712 3.177-4.89 1.82-22.702 1.82-29.434 0-66.934 117.284-66.957 117.284 0 0 8.469-1.949 24.012 2.646 30.674 2.244 3.236 1.842 3.614 2.587 7.582.992 5.174 1.063 10.477 1.145 15.886.107 8.434-2.197 10.879-6.484 17.717-4.158 12.119-19.973 35.032-18.626 41.906 5.031 25.536 69.898 42.686 90.32 51.769v48.366H47.316zm472.872 127.16h-24.249v-26.174h24.249v26.173zm-366.371 0h-24.26v-26.174h24.26v26.173z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon rolodex-entry";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
