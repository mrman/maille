// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Table implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M0 53.008h640v533.983H0V53.01zm44.15 122.814h154.301c4.252 0 7.736 3.507 7.736 7.736v50.811c0 4.217-3.508 7.725-7.736 7.725h-154.3c-4.217 0-7.737-3.473-7.737-7.725v-50.811c0-4.252 3.484-7.736 7.736-7.736zm397.4 306.086h154.3c4.252 0 7.725 3.496 7.725 7.736v50.811c0 4.24-3.485 7.737-7.725 7.737h-154.3c-4.241 0-7.737-3.485-7.737-7.737v-50.811c0-4.252 3.484-7.737 7.736-7.737zm-197.803 0h152.506c4.264 0 7.736 3.496 7.736 7.736v50.811c0 4.24-3.496 7.737-7.736 7.737H243.747c-4.24 0-7.736-3.485-7.736-7.737v-50.811c0-4.252 3.472-7.737 7.736-7.737zm-199.597 0h154.301c4.252 0 7.736 3.507 7.736 7.736v50.811c0 4.229-3.508 7.737-7.736 7.737h-154.3c-4.217 0-7.737-3.485-7.737-7.737v-50.811c0-4.252 3.484-7.737 7.736-7.737zm397.4-102.025h154.3c4.252 0 7.725 3.496 7.725 7.736v50.811c0 4.24-3.485 7.725-7.725 7.725h-154.3c-4.241 0-7.737-3.473-7.737-7.725V387.62c0-4.252 3.484-7.737 7.736-7.737zm-197.803 0h152.506c4.264 0 7.736 3.496 7.736 7.736v50.811c0 4.229-3.496 7.725-7.736 7.725H243.747c-4.24 0-7.736-3.473-7.736-7.725V387.62c0-4.252 3.472-7.737 7.736-7.737zm-199.597 0h154.301c4.252 0 7.736 3.507 7.736 7.736v50.811c0 4.217-3.508 7.725-7.736 7.725h-154.3c-4.217 0-7.737-3.473-7.737-7.725V387.62c0-4.252 3.484-7.737 7.736-7.737zm397.4-102.037h154.3c4.252 0 7.725 3.496 7.725 7.736v50.811c0 4.24-3.485 7.737-7.725 7.737h-154.3c-4.241 0-7.737-3.485-7.737-7.737v-50.811c0-4.252 3.484-7.737 7.736-7.737zm-197.803 0h152.506c4.264 0 7.736 3.496 7.736 7.736v50.811c0 4.24-3.496 7.737-7.736 7.737H243.747c-4.24 0-7.736-3.485-7.736-7.737v-50.811c0-4.252 3.472-7.737 7.736-7.737zm-199.597 0h154.301c4.252 0 7.736 3.52 7.736 7.736v50.811c0 4.229-3.508 7.737-7.736 7.737h-154.3c-4.217 0-7.737-3.485-7.737-7.737v-50.811c0-4.252 3.484-7.737 7.736-7.737zm397.4-102.025h154.3c4.252 0 7.725 3.496 7.725 7.736v50.811c0 4.24-3.485 7.725-7.725 7.725h-154.3c-4.241 0-7.737-3.473-7.737-7.725v-50.811c0-4.252 3.484-7.736 7.736-7.736zm-197.803 0h152.506c4.264 0 7.736 3.496 7.736 7.736v50.811c0 4.229-3.496 7.725-7.736 7.725H243.747c-4.24 0-7.736-3.473-7.736-7.725v-50.811c0-4.252 3.472-7.736 7.736-7.736z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon table";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
