// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 3333","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class DashboardOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M533 1730c0-49 18-92 53-127s78-53 127-53c50 0 92 18 128 53 35 35 53 78 53 127s-18 92-53 127-78 53-128 53c-49 0-92-18-127-53s-53-78-53-127zM1667 152c-105 0-206 10-305 30s-194 49-285 88c-93 40-180 87-260 140-80 54-155 115-223 184-69 68-130 143-184 223s-100 167-140 260c-1 1-1 2-2 4-38 90-67 184-87 281-20 98-30 200-30 305s10 206 30 305 49 194 88 285c40 93 87 180 140 260 54 80 115 155 184 223 68 69 143 130 223 184s167 100 260 140c1 1 2 1 4 2 90 38 184 67 281 87 98 20 200 30 305 30s206-10 305-30 194-49 285-88c93-40 180-87 260-140 80-54 155-115 223-184 69-68 130-143 184-223s100-167 140-260c1-1 1-2 2-4 38-90 67-184 87-281 20-98 30-200 30-305s-10-206-30-305-49-194-88-285c-40-93-87-180-140-260-54-80-115-155-184-223-68-69-143-130-223-184s-167-100-260-140c-1-1-2-1-4-2-90-38-184-67-281-87-98-20-200-30-305-30zM1332 32c108-22 220-33 335-33s226 11 335 33c107 21 210 53 311 96 1 1 3 1 4 2 100 43 195 95 284 154s171 127 246 202c76 75 143 157 202 246 60 89 111 184 154 284 44 102 76 207 98 315s33 220 33 335-11 226-33 335c-21 107-53 210-96 311-1 1-1 3-2 4-43 100-95 195-154 284s-127 171-202 246c-75 76-157 143-246 202-89 60-184 111-284 154-102 44-207 76-315 98s-220 33-335 33-226-11-335-33c-107-21-210-53-311-96-1-1-3-1-4-2-100-43-195-95-284-154s-171-127-246-202c-76-75-143-157-202-246-60-89-111-184-154-284-44-102-76-207-98-315S0 1781 0 1666s11-226 33-335c21-107 53-210 96-311 1-1 1-3 2-4 43-100 95-195 154-284s127-171 202-246c75-76 157-143 246-202 89-60 184-111 284-154 102-44 207-76 315-98zm93 1153l441 804c16 14 29 31 40 49 10 19 19 38 24 57 16 62 9 120-22 174s-77 90-140 108c-60 16-118 8-173-24s-90-78-106-138c-4-16-6-32-7-48s0-32 1-48l-58-934zm1376 544c0 49-18 92-53 127s-78 53-127 53c-50 0-92-18-128-53-35-35-53-78-53-127s18-92 53-127 78-53 128-53c49 0 92 18 127 53s53 78 53 127zm-329-807c36 35 53 78 53 129 0 50-18 93-53 129-35 34-78 50-129 50-50 0-92-17-126-50-35-35-53-78-53-129 0-50 18-93 53-129 34-34 76-51 126-51 51 0 93 17 129 51zm-804-335c51 0 94 18 129 53 34 35 52 77 52 127 0 49-17 92-52 127-34 35-77 53-129 53-49 0-92-18-127-53s-53-78-53-127c0-50 18-92 53-127 35-36 78-53 127-53zM864 922c35-34 78-51 129-51 50 0 92 17 126 51 35 35 53 78 53 129 0 50-18 93-53 129-34 34-76 50-126 50-51 0-93-17-129-50-36-35-53-78-53-129 0-50 18-93 53-129z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon dashboard-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
