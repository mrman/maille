// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ThumbsDownOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M326.213 558.775c2.93 14.835 10.571 24.366 21.047 29.209 8.516 3.933 18.414 4.335 28.725 1.618 8.953-2.35 18.367-7.134 27.39-14.008 24.426-18.59 47.292-53.623 50.222-97.43.897-13.465.65-27.886-.626-43.158-.84-10.099-2.162-20.705-3.91-31.784h103.903l.484-.047c16.89-.662 33.249-4.772 47.02-12 11.906-6.237 21.958-14.776 28.938-25.418 7.181-10.949 11.114-23.965 10.536-38.835-.437-11.055-3.39-23.032-9.414-35.812 3.46-14.386 5.032-29.8 2.823-43.323-1.866-11.504-6.343-21.733-14.386-29.315.413-19.016-2.126-34.95-7.24-48.745-5.256-14.173-13.134-25.937-23.245-36.355-.862-9.094-2.35-17.8-4.653-25.96-2.882-10.217-7.063-19.607-12.945-27.887l-.024.012c-17.717-24.98-31.89-24.414-54.331-23.493-3.118.119-6.473.26-11.682.26H301.598c-18.295 0-32.657 2.634-45.791 9.201-12.993 6.485-24.072 16.5-35.906 31.323l-3.036 8.623V342.513l10.311 2.775c26.221 7.087 46.843 29.47 62.86 56.292 16.464 27.58 28.015 59.895 35.905 85.998v68.54l.272 2.657zM20.823 367.269h151.478c11.457 0 20.835-9.366 20.835-20.823V69.498c0-11.457-9.378-20.835-20.835-20.835H20.823C9.366 48.663 0 58.041 0 69.498v276.948c0 11.457 9.366 20.823 20.823 20.823zM358.87 562.815c-2.256-1.04-4.075-3.661-5.15-8.114v-69.096l-.602-3.968c-8.374-27.98-20.906-63.39-39.331-94.265-17.327-29.02-39.863-53.942-69.143-65.458V110.376c8.032-9.52 15.437-15.992 23.528-20.031 8.953-4.465 19.477-6.272 33.426-6.272h203.247c3.626 0 8.362-.189 12.767-.366 13.312-.543 21.71-.886 30.71 11.811v.06c3.897 5.503 6.755 12.023 8.822 19.322 2.197 7.784 3.473 16.418 4.11 25.63l4.288 9.119c8.941 8.468 15.803 18.023 20.138 29.728 4.477 12.071 6.402 26.623 5.327 44.824l-.484 8.232 6.992 4.347c4.772 2.964 7.382 8.586 8.504 15.449 1.772 10.913-.083 24.342-3.508 36.791l1.087 10.028c5.645 10.784 8.35 20.41 8.68 28.855.332 8.74-1.936 16.335-6.082 22.665-4.358 6.638-10.795 12.048-18.52 16.11-10.016 5.233-22.075 8.257-34.713 8.812v-.035H415.884l3.094 16.429c2.988 15.886 5.126 31.04 6.33 45.402 1.182 14.079 1.418 27.118.627 39.036-2.34 34.985-20.257 62.705-39.379 77.257-6.094 4.642-12.154 7.795-17.622 9.236-4.122 1.075-7.607 1.134-10.063 0z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon thumbs-down-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
