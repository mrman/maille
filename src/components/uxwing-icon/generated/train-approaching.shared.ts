// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class TrainApproaching implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M422.367 531.337H220.455l-23.729 35.02h248.448l-22.807-35.02zm-247.94 67.938L146.837 640H85.83l99.12-148.667h-17.599c-24.036 0-43.69-19.666-43.69-43.702V43.701c0-24.035 19.654-43.7 43.69-43.7h308.933c24.036 0 43.701 19.665 43.701 43.7v403.93c0 24.036-19.665 43.702-43.7 43.702h-17.15l95.032 146.565H491.77l-25.158-38.623H174.427zm229.148-237.948h52.867c5.752 0 10.44 4.7 10.44 10.44v52.868c0 5.752-4.688 10.453-10.44 10.453h-52.867c-5.752 0-10.453-4.701-10.453-10.453v-52.867c0-5.74 4.701-10.441 10.453-10.441zm-221.979 0h52.867c5.74 0 10.44 4.7 10.44 10.44v52.868c0 5.752-4.7 10.453-10.44 10.453h-52.867c-5.752 0-10.453-4.701-10.453-10.453v-52.867c0-5.74 4.701-10.441 10.453-10.441zm30.91-277.043h216.026c23.209 0 42.201 18.992 42.201 42.213v66.438c0 23.209-18.992 42.19-42.201 42.19H212.506c-23.221 0-42.201-18.981-42.201-42.19v-66.438c0-23.22 18.98-42.213 42.2-42.213zm66.721-65.115h81.745c4.087 0 7.441 3.342 7.441 7.44v32.198c0 4.099-3.354 7.441-7.44 7.441h-81.746c-4.098 0-7.44-3.342-7.44-7.441V26.61c0-4.099 3.342-7.441 7.44-7.441z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon train-approaching";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
