// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class TaxiApproaching implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M614.311 300.429v318.866h-74.528v-49.264H100.218v49.264H25.69V300.43H0v-49.028h62.683l34.96-108.745c10.3-32.02 27.52-61.158 61.159-61.158h76.997V38.422c0-19.323-3.992-17.681 14.15-17.67h142.348c15.212 0 11.905-1.464 11.905 14.741v46.005h89.978c33.638 0 53.044 28.535 61.158 61.158L582.374 251.4H640v49.028h-25.69zM208.373 471.656h220.16v51.91h-220.16v-51.91zM50.647 350.733c56.103 1.772 90.415 25.288 99.237 74.257H50.647v-74.257zm531.656 0c-56.091 1.772-90.415 25.288-99.226 74.257h99.226v-74.257zM89.482 251.401h461.049l-19.76-91.182c-5.421-25.004-20.977-46.595-46.607-46.595H165.31c-25.618 0-38.764 22.181-46.595 46.595L89.482 251.4z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon taxi-approaching";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
