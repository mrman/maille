// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3347 3750","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class FemaleOutlineWithHair implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M763 2764c297 288 595 449 896 459 301 9 609-135 925-457-46-11-94-21-146-32l-7-1c-90-18-255-51-360-114-59-36-93-71-92-119-78 47-160 77-244 89-129 18-260-6-390-76 6 54-24 92-81 127-89 54-232 77-337 94-25 4-47 8-68 12-33 7-65 13-96 20zm398-1971c202 352 591 587 1314 614 21 61 14 126 0 191-297 1013-1056 1299-1582 265l-79-211c-161-329 139-195 249-428l46-202 52-229zm1423 998c276-1073 2-1527-501-1613C1976 61 1776 8 1635 0 417 62 362 1359 667 2213l299-66c88 127 181 226 276 297l3 5c31 51 12 86-28 111-74 45-207 66-304 82-28 5-53 9-71 12-320 62-529 137-661 293C49 3102 0 3332 0 3703c0 25 20 46 46 46h3256c25 0 46-20 46-46 0-381-52-611-192-765-139-154-360-226-699-294l-7-1c-85-17-241-48-330-103-46-28-72-63-47-105 27-21 53-45 79-70 133-132 249-319 339-553l96-21zm121 1006c-353 377-703 545-1049 535-347-10-685-200-1017-537-178 49-304 112-389 212-111 131-156 330-159 652h3164c-4-330-51-529-168-658-85-93-209-154-382-203z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon female-outline-with-hair";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
