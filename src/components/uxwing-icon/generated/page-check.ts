// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3195 3750","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PageCheck implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M962 2653c-53 0-96-44-96-97 0-54 43-97 96-97h626c53 0 96 44 96 97 0 54-43 97-96 97H962zm0-1364c-53 0-96-43-96-96s43-96 96-96h1270c53 0 96 43 96 96s-43 96-96 96H962zm1069 2026c-41-33-48-93-15-135 33-41 93-48 135-15l292 231 556-680c34-41 94-47 135-14 41 34 47 94 14 135l-615 753c-33 41-93 47-134 14l-366-290zm-297 242c53 0 96 43 96 96s-43 96-96 96H223c-62 0-118-25-158-66-41-40-66-96-66-158V223c0-62 25-118 66-158 41-41 96-66 158-66h2747c62 0 118 25 158 66 41 41 66 96 66 158v1992c0 53-43 96-96 96s-96-43-96-96V223c0-9-4-16-9-22-6-6-14-9-22-9H224c-9 0-16 4-22 9-6 6-9 14-9 22v3302c0 9 4 16 9 22 6 6 14 9 22 9h1511zM962 1971c-53 0-96-43-96-96s43-96 96-96h1270c53 0 96 43 96 96s-43 96-96 96H962z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon page-check";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
