// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 5919 4843","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class BriefcaseOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2741 2422h437c64 0 123 26 165 68s68 101 68 165v922c0 64-26 123-68 165s-101 68-165 68h-437c-64 0-123-26-165-68s-68-101-68-165v-922c0-64 26-123 68-165s101-68 165-68zM336 673h1517V238c0-66 27-125 70-168s103-70 168-70h1737c66 0 125 27 168 70s70 103 70 168v435h1517c93 0 177 38 237 99 61 61 99 145 99 237v3498c0 93-38 177-99 237-61 61-145 99-237 99H336c-93 0-177-38-237-99-61-61-99-145-99-237V1009c0-93 38-177 99-237 61-61 145-99 237-99zm3492-464H2091c-8 0-15 3-21 9-5 5-9 13-9 21v430h1796V239c0-8-3-15-9-21-5-5-13-9-21-9zM208 2680c767 220 1335 315 2102 351v209c-767-35-1335-128-2102-344v1611c0 35 14 67 38 90 23 23 55 38 90 38h5247c35 0 67-14 90-38 23-23 38-55 38-90V2896c-767 216-1300 308-2066 344v-209c767-36 1300-131 2066-351V1009c0-35-14-67-38-90-23-23-55-38-90-38H336c-35 0-67 14-90 38-23 23-38 55-38 90v1671zm2970-50h-437c-7 0-13 3-18 7-5 5-7 11-7 18v922c0 7 3 13 7 18 5 5 11 7 18 7h437c7 0 13-3 18-7 5-5 7-11 7-18v-922c0-7-3-13-7-18-5-5-11-7-18-7z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon briefcase-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
