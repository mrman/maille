// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class MaleGroup implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M447.726 153.947c23.445-1.618 48.638 2.114 64.217 11.232 17.74 10.382 28.253 28.536 28.253 57.344v25.193c0 11.327-.402 11.634 3.696 17.563 3.119 4.512 3.827 5.055 4.867 10.583 1.382 7.216 1.476 14.598 1.582 22.146.166 11.764-4.31 15.165-10.299 24.697l-25.453 40.583c-7.89 12.65-15.13 20.386-11.976 36.414 7.004 35.599 108.923 55.63 137.399 68.304v67.418h-82.844c0-12.732 4.677-75.91-2.362-81.107-85.49-63.06-198.628-27.993-112.194-162.7 6.85-10.925 7.287-15.59 11.8-28.299 8.302-23.362 7.251-72.804-6.686-109.37zm-255.452 0c-23.445-1.618-48.638 2.114-64.217 11.232-17.74 10.382-28.253 28.536-28.253 57.344v25.193c0 11.327.402 11.634-3.697 17.563-3.118 4.512-3.826 5.055-4.866 10.583-1.382 7.216-1.476 14.598-1.582 22.146-.166 11.764 4.31 15.165 10.299 24.697l25.453 40.583c7.89 12.65 15.13 20.386 11.976 36.414-7.004 35.599-108.923 55.63-137.399 68.304v67.418h82.844c0-12.732-4.677-75.91 2.362-81.107 85.49-63.06 198.628-27.993 112.194-162.7-6.85-10.925-7.287-15.59-11.8-28.299-8.302-23.362-7.251-72.804 6.686-109.37zm-87.828 381.489l.012-67.158c32.126-14.292 157.844-45.828 165.758-86.01 3.555-18.07-4.618-26.81-13.524-41.078l-28.725-45.792c-6.744-10.76-11.8-14.599-11.622-27.875.106-7.464.213-14.823 1.288-22.016 1.37-9.177 2.504-9.46 7.299-16.854 3.236-4.96 2.87-6.437 2.87-17.87v-28.43c0-32.859 12.106-53.41 32.492-65.044 29.87-17.055 90.427-17.044 120.131.342 20.009 11.717 31.867 32.21 31.867 64.702v28.43c0 12.791-.449 13.134 4.17 19.819 3.519 5.09 4.31 5.704 5.503 11.929 1.548 8.15 1.666 16.488 1.772 24.992.189 13.276-4.854 17.115-11.61 27.875l-28.725 45.792c-8.906 14.268-17.08 23.008-13.512 41.079 7.901 40.181 122.895 62.776 155.022 77.068v76.087l-420.466.012z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon male-group";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
