// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3333 3027","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class PaperclipThin implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2282 1688c33 33 34 87 0 121-33 33-87 34-121 0l-689-683-3-3c-105-115-221-144-310-119-32 9-61 25-85 46-23 21-42 47-53 78-30 79-12 186 87 298l1253 1253 5 5c164 168 359 202 515 150 60-20 114-53 159-95 44-42 79-95 100-154 55-157 14-366-196-584-192-193-373-391-552-586-354-387-699-764-1134-1099-167-128-375-166-565-132-83 15-163 44-233 84s-131 92-179 154c-88 114-131 264-99 441 53 297 221 465 424 667l63 63 645 651 218 220c33 33 33 88 0 121s-88 33-121 0l-218-220-645-651-62-62C263 1429 77 1244 14 894c-41-229 16-425 132-575 62-80 140-147 229-198 88-51 186-86 288-104 235-42 492 5 700 165 445 343 796 726 1155 1119 175 192 353 386 548 581l2 2c262 272 309 546 234 759-30 85-79 160-142 221-63 60-139 106-223 134-213 71-476 28-692-193-1-1-3-2-4-4L988 1548l-3-3c-151-167-174-341-123-476 21-56 56-105 100-144 43-39 96-68 154-83 146-40 326 0 481 168l686 681z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon paperclip-thin";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
