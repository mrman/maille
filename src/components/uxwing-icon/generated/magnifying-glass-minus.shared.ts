// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class MagnifyingGlassMinus implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M353.059 234.062c6.307-.035 12.26 1.63 17.303 4.547l-.011.012a33.12 33.12 0 0 1 5.267 3.72c1.583 1.37 3.083 2.965 4.43 4.666h.023a33.966 33.966 0 0 1 7.252 21.107 34.614 34.614 0 0 1-4.712 17.374 34.771 34.771 0 0 1-9.343 10.382 34.815 34.815 0 0 1-6.071 3.59v.012a34.37 34.37 0 0 1-14.173 3.13l-183.168 1.288a33.995 33.995 0 0 1-17.327-4.548l.011-.011a32.971 32.971 0 0 1-5.291-3.756c-1.594-1.37-3.07-2.953-4.394-4.642h-.023a33.92 33.92 0 0 1-7.252-21.107 34.483 34.483 0 0 1 4.712-17.339l-.023-.011a35.014 35.014 0 0 1 7.512-8.918h.023a34.507 34.507 0 0 1 10.11-6 34.608 34.608 0 0 1 11.965-2.209l183.18-1.287zM260.365 8.02h.083v.035c71.859.024 136.939 29.174 184.042 76.277 47.008 47.044 76.134 112.064 76.182 183.876h.035v.377h-.035c-.024 28.95-4.772 56.789-13.512 82.762-1.465 4.37-3 8.551-4.56 12.555v.036a260.163 260.163 0 0 1-28.901 53.681l151.478 136.147.094.083.815.756.06.059c8.55 8.138 13.204 19.04 13.794 30.107.58 10.937-2.822 22.122-10.299 31.098l-.059.083-.91 1.051-.188.19-.756.826-.083.118c-8.15 8.552-19.028 13.193-30.107 13.784-10.925.579-22.122-2.811-31.098-10.3l-.083-.059-1.051-.909-.154-.13-154.939-139.242a259.128 259.128 0 0 1-13.996 9.19c-6.425 3.932-13.063 7.641-19.855 11.019-34.902 17.398-74.316 27.201-115.997 27.201v.035h-.082v-.035c-71.871-.024-136.962-29.174-184.065-76.288C29.198 405.37.083 340.339.036 268.55H0v-.248h.036C.059 196.43 29.209 131.34 76.312 84.237 123.356 37.228 188.376 8.102 260.188 8.055v-.036h.177zm.083 58.394v.024h-.26v-.024c-55.654.024-106.1 22.654-142.62 59.163-36.52 36.496-59.162 87-59.174 142.726h.035v.248h-.035c.035 55.642 22.642 106.076 59.162 142.608 36.508 36.52 87 59.162 142.726 59.174v-.036h.26v.036c55.642-.036 106.064-22.642 142.596-59.163 36.52-36.508 59.162-86.989 59.174-142.703h-.036v-.259h.036c-.036-55.655-22.654-106.112-59.162-142.632-36.509-36.509-86.99-59.15-142.703-59.163z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon magnifying-glass-minus";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
