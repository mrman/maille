// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class CheckMarkBox implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M166.608 323.284c-22.583-19.547-25.04-53.705-5.504-76.288 19.547-22.583 53.705-25.052 76.288-5.504l84.38 73.158L546.844 76.667c20.48-21.65 54.639-22.595 76.289-2.126 21.65 20.48 22.606 54.639 2.126 76.288L367.174 423.726l-.898 1.028c-19.536 22.583-53.694 25.051-76.277 5.516L166.61 323.284zm-68.6-289.031H473.51v.189h.154l-92.387 95.044H97.938v-.177c-.543 0-1.217.39-1.843 1.016l-.094-.094-.095.094c-.531.532-.862 1.217-.862 1.913h.19V507.8h-.19c0 .542.402 1.228 1.028 1.854l-.095.094.095.083c.531.531 1.216.862 1.901.862v-.177h375.572v.177c.673 0 1.405-.39 2.031-1.016.532-.531.863-1.216.863-1.913h-.178v-92.978l95.234-97.985v190.962h-.189c-.012 27-11.032 51.52-28.76 69.237-17.599 17.599-42.024 28.536-69.001 28.56v.189H97.974v-.19c-26.954-.011-51.426-10.995-69.143-28.677l-.095.095C11.138 559.377.212 534.893.177 507.798h-.189v-375.56h.189c.012-26.965 10.996-51.426 28.678-69.142L28.76 63c17.599-17.61 42.071-28.536 69.178-28.56v-.188h.07z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon check-mark-box";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
