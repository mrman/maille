// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class StarOutlineHalf implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M335.39 25.087L417.23 216.7l207.569 18.626c9.153.791 15.933 8.858 15.141 18.012a16.51 16.51 0 0 1-5.622 11.09l.012.013-157.029 137.104 46.442 203.235c2.055 8.976-3.555 17.929-12.532 19.984-4.595 1.051-9.177.083-12.839-2.303L320 515.83 141.084 622.804c-7.901 4.724-18.13 2.15-22.854-5.752a16.612 16.612 0 0 1-1.937-12.26l-.012-.012 46.441-203.235L5.693 264.441c-6.921-6.059-7.63-16.583-1.57-23.504a16.568 16.568 0 0 1 11.645-5.658L222.794 216.7l81.887-191.719c3.602-8.48 13.394-12.425 21.85-8.81a16.612 16.612 0 0 1 8.86 8.917zm-15.827 49.926v404.745a16.587 16.587 0 0 1 8.988 2.362l153.994 92.056-39.969-174.9h.012c-1.31-5.775.496-12.07 5.268-16.252l135.19-118.029L405.29 249.05c-6.295-.2-12.201-3.969-14.835-10.134L320 73.949l-.437 1.063z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon star-outline-half";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
