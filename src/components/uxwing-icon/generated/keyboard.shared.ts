// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Keyboard implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M135.663 183.475h411.489c20.386 0 36.886.992 49.43 3.012 13.96 2.232 23.634 6.012 28.96 11.35l.119.107c5.327 5.326 9.118 14.976 11.35 28.89 1.996 12.484 2.989 28.89 2.989 49.158V432.95c0 20.032-1.087 36.26-3.225 48.65-2.398 13.937-6.402 23.705-11.953 29.256l-.07.036c-5.54 5.457-15.272 9.401-29.127 11.764-12.378 2.09-28.571 3.153-48.473 3.153H92.847c-19.347 0-35.15-1.18-47.386-3.508-13.654-2.61-23.422-6.862-29.257-12.697-5.846-5.846-10.086-15.602-12.685-29.244h-.012C1.17 468.148 0 452.345 0 433.069V275.992c0-18.969.898-34.5 2.693-46.56 1.985-13.24 5.256-22.866 9.792-28.819l.012.012c8.917-11.811 29.22-17.717 60.804-17.717l.271.035 9.118.26c.248-11.114 1.571-21.709 3.957-31.76a129.038 129.038 0 0 1 12.19-31.973l2.87-5.28h68.067l-14.244 16.63-1.016 1.194h.012c-6.32 7.535-11.067 15.791-14.174 24.685-2.881 8.209-4.452 17.15-4.689 26.776zm411.607 36.65H92.847c-15.792 0-28.252.673-37.335 2.043h-.047c-7.359 1.099-11.752 2.339-13.099 3.686-1.346 1.358-2.587 5.74-3.685 13.05-1.37 9.036-2.043 21.414-2.043 37.088v156.722c0 15.803.673 28.276 2.043 37.359 1.098 7.37 2.339 11.775 3.685 13.122 1.347 1.358 5.74 2.598 13.099 3.697h.047c9.083 1.358 21.532 2.055 37.335 2.055H547.27c15.78 0 28.252-.697 37.312-2.055 7.322-1.099 11.634-2.292 12.826-3.532l.083-.083c1.3-1.322 2.528-5.74 3.626-13.169 1.347-9.083 2.02-21.555 2.02-37.394V275.992c0-15.674-.673-28.04-2.032-37.087-1.098-7.311-2.35-11.693-3.697-13.052-1.346-1.346-5.728-2.586-13.05-3.685-9.036-1.37-21.415-2.043-37.088-2.043zm-454.541 29.21H144.096v40.806h20.976v-40.807h61.666v40.807h21.095v-40.807h61.678v40.807h20.976v-40.807h61.336v40.807h21.437v-40.807H474.583v40.807h20.977v-40.807h61.678V310.67H516.43v21.426h40.807V393.43H495.56v-40.819h-20.976v40.819H413.26v-40.819h-21.437v40.819h-61.336v-40.819h-20.976v40.819h-61.678v-40.819h-21.095v40.819h-61.666v-40.819h-20.976v40.819H82.642V332.095h40.583v-21.426H82.642V249.334H92.73zm93.202 82.417h20.291v-20.397h-20.291v20.397zm82.418 0h20.634v-20.397H268.35v20.397zm82.667 0h20.291v-20.397h-20.291v20.397zm82.76 0h20.292v-20.397h-20.291v20.397zM165.072 455.415V403.824h309.512v61.678H165.073v-10.087z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon keyboard";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
