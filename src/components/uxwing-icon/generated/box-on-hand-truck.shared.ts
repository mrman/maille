// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class BoxOnHandTruck implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M263.495 135.876L544.034 65.8c7.925-1.972 16.051 2.894 18.036 10.831l70.064 280.54c1.984 7.924-2.894 16.05-10.82 18.023l-280.55 70.076c-7.938 1.984-16.052-2.894-18.036-10.82l-70.064-280.538c-1.972-7.938 2.894-16.052 10.83-18.036zm24.378 335.709c46.49 0 84.202 37.701 84.202 84.202 0 46.5-37.713 84.213-84.202 84.213-46.5 0-84.213-37.713-84.213-84.213 0-46.5 37.713-84.202 84.213-84.202zm101.104 11.173l218.046-58.418 13.075-3.508 3.508 13.087 9.355 35.009 3.496 13.075-13.075 3.496-218.046 58.418-13.075 3.508-3.508-13.087-9.355-35.008-3.508-13.075 13.087-3.497zM25.465.201l43.43 7.571c49.879 8.682 75.072 13.701 91.371 26.646 17.552 13.95 21.497 32.73 29.115 69.084 1.382 6.59 2.94 14.02 4.984 22.914 1.89 8.244 4.252 17.846 7.37 29.374l77.434 284.744c1.96 7.217-2.303 14.658-9.531 16.618-.213.06-.426.118-.638.166l-34.418 9.33c-7.205 1.926-14.61-2.338-16.56-9.531h-.011l-35.197-129.44-42.237-155.304c-2.93-10.82-5.457-21.225-7.677-30.922l-.13-.614c-2.138-9.366-3.803-17.35-5.292-24.46-3.791-18.096-5.752-27.462-11.598-31.69-7.677-5.552-24.308-8.717-57.627-14.516L41.41 67.24 14.812 62.6c-7.37-1.252-12.32-8.256-11.068-15.615l6.107-35.717c1.252-7.37 8.244-12.33 15.614-11.067z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon box-on-hand-truck";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
