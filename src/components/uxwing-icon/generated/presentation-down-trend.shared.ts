// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class PresentationDownTrend implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M362.119 492.939l68.918 68.906c16.69.496 30.071 14.186 30.071 31.005 0 17.126-13.878 31.016-31.016 31.016-17.126 0-31.016-13.89-31.016-31.016 0-1.89.177-3.756.496-5.552l-59.93-59.93v44.056H296.71v-42.886l-60.91 60.922a32.08 32.08 0 0 1 .177 3.39c0 17.126-13.878 31.016-31.016 31.016-17.127 0-31.016-13.89-31.016-31.016 0-17.127 13.89-31.016 31.016-31.016.508 0 1.015.011 1.511.035l68.93-68.93H.024V97.962A14.676 14.676 0 0 1 0 97.24v-41.93c0-4.511 3.768-8.197 8.398-8.197H296.71v-30.98h42.934v30.98h291.96c4.63 0 8.398 3.686 8.398 8.197v41.93c0 .142 0 .272-.012.413V492.94h-277.87zM140.506 229.113L261.63 347.532c30.024-30.012 59.706-59.694 90.025-89.635l80.28 81.273-37.229 37.24 104.777.922V271.634l-35.787 35.776c-37.265-37.737-74.41-75.427-111.84-112.856l-90.817 89.705-88.772-86.918-31.76 31.772zm452.143-99.793H46.441v315.842h546.208V129.32z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon presentation-down-trend";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
