// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class MaleFemaleSymbols implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M374.52 128.966v.083c37.571 0 72.296 12.012 100.608 32.35l35.292-36.07-53.056-49.312L640 16.039l-47.115 186.38-52.193-49.63-34.383 37.548c25.182 29.941 40.324 68.587 40.335 110.836h.119v.035h-.119c-.011 47.6-19.228 90.674-50.35 121.785-31.123 31.099-74.151 50.351-121.75 50.351v.06h-.024v-.06c-38.942 0-74.812-12.91-103.62-34.63-22.452 16.146-49.004 27-77.811 30.697v53.446h75.768c12.178 0 22.052 9.874 22.052 22.04 0 12.188-9.874 22.062-22.052 22.062H193.09v39.13c0 9.875-9.863 17.883-22.052 17.883-12.177 0-22.063-8.008-22.063-17.882v-39.13H73.205c-12.165 0-22.04-9.875-22.04-22.064 0-12.165 9.875-22.04 22.04-22.04h75.769v-53.445c-38.445-4.937-72.863-22.595-98.883-48.603C19.205 389.91.082 347.165.082 299.885h-.094v-.035h.094c0-47.269 19.123-90.025 50.02-120.923 30.899-30.898 73.643-50.009 120.912-50.02v-.083h.035v.082c37.56 0 72.65 12.591 101.127 33.757 28.595-21.119 63.981-33.615 102.344-33.615v-.083zM238.845 407.202c-22.807-29.209-36.485-65.954-36.485-105.982h-.059v-.047h.06c0-40.831 14.208-78.332 37.854-107.812-19.914-12.993-43.69-20.516-69.166-20.516v.094H171.013v-.094c-34.972 0-66.709 14.244-89.729 37.264-23.02 23.02-37.264 54.757-37.264 89.741h.07V299.886h-.07c0 34.984 14.244 66.71 37.264 89.73 23.02 23.031 54.757 37.275 89.741 37.275v-.094H171.061v.094c24.886 0 48.142-7.24 67.784-19.689zm65.847-213.085c23.28 29.22 37.288 66.048 37.3 105.722h.083v.035h-.083c-.012 40.914-14.303 78.415-38.186 107.79 20.28 13.535 44.588 21.437 70.714 21.437v-.083h.024v.083c35.232 0 67.158-14.34 90.343-37.536 23.174-23.174 37.548-55.123 37.548-90.356h-.106v-.035h.106c0-35.22-14.374-67.182-37.512-90.332-23.221-23.185-55.147-37.548-90.403-37.548v.107-.107c-25.76 0-49.713 7.654-69.828 20.824zm-32.941 183.049c16.512-21.437 26.315-48.237 26.315-77.292h-.094V299.838h.094c-.012-28.441-9.413-54.768-25.323-75.98-16.382 21.495-26.162 48.319-26.162 77.315h.13v.047h-.13c0 28.418 9.378 54.686 25.17 75.946z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon male-female-symbols";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
