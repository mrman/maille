// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Dashboard implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M320 0c176.707 0 320 143.293 320 320S496.707 640 320 640 0 496.707 0 320 143.293 0 320 0zm-80.847 209.802l104.15 189.829a53.554 53.554 0 0 1 14.823 24.98c7.678 28.654-9.33 58.123-37.984 65.8-28.654 7.678-58.123-9.342-65.8-37.984a53.686 53.686 0 0 1-1.158-22.441l-14.031-220.184zm347.579 128.163c0 23.457-19.028 42.485-42.473 42.485-23.457 0-42.485-19.028-42.485-42.485 0-23.445 19.028-42.473 42.485-42.473 23.445 0 42.473 19.028 42.473 42.473zm-533.452 0c0 23.457 19.016 42.485 42.473 42.485s42.473-19.028 42.473-42.485c0-23.445-19.016-42.473-42.473-42.473S53.28 314.52 53.28 337.965zm77.576-189.581c-16.583 16.583-16.583 43.489 0 60.072 16.583 16.583 43.489 16.583 60.072 0 16.583-16.583 16.583-43.49 0-60.072-16.583-16.583-43.489-16.583-60.072 0zm189.38-79.194c23.457 0 42.473 19.016 42.473 42.473 0 23.445-19.016 42.473-42.473 42.473s-42.473-19.028-42.473-42.473c0-23.457 19.016-42.485 42.473-42.473zm188.908 79.194c16.595 16.583 16.595 43.489.012 60.072-16.583 16.583-43.489 16.583-60.071 0-16.595-16.583-16.595-43.49 0-60.072 16.582-16.583 43.488-16.583 60.06 0z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon dashboard";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
