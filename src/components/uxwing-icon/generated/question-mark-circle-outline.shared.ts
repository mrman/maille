// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class QuestionMarkCircleOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M571.46 320v-.07h.13c0-69.309-28.229-132.192-73.808-177.759-45.556-45.544-108.38-73.737-177.628-73.76v.13h-.224v-.13c-69.297 0-132.18 28.228-177.746 73.784C96.64 187.75 68.423 250.585 68.41 319.847h.13V320.07h-.13c0 46.264 12.532 89.67 34.347 126.9 43.666 74.516 124.584 124.595 217.1 124.62v-.13h.213v.13c69.308 0 132.191-28.241 177.758-73.82 7.5-7.5 14.47-15.379 20.824-23.552C551.736 431.675 571.46 378.147 571.46 320zm-215.294 64.678h-86.08v-8.599c0-14.586 1.571-26.527 4.914-35.67 3.354-9.212 8.291-17.468 14.834-25.074 6.591-7.571 21.414-20.835 44.34-39.898 12.212-10.016 18.354-19.11 18.354-27.343 0-8.41-2.48-14.8-7.37-19.442-4.913-4.57-12.343-6.91-22.37-6.91-10.772 0-19.583 3.58-26.564 10.631-7.051 7.087-11.527 19.524-13.464 37.205l-87.958-10.949c3.012-32.315 14.776-58.347 35.197-78 20.516-19.75 51.898-29.576 94.182-29.576 32.942 0 59.517 6.91 79.773 20.599 27.544 18.614 41.245 43.394 41.245 74.292 0 12.898-3.58 25.288-10.642 37.229-7.075 11.976-21.615 26.516-43.536 43.678-15.19 12.212-24.863 21.827-28.867 29.185-3.992 7.276-5.988 16.878-5.988 28.642zm-89.115 22.95h92.292v81.319h-92.292v-81.32zm372.82-87.699h.141V320c0 73.926-25.122 142.041-67.276 196.254-8.268 10.63-17.126 20.681-26.445 29.989-57.792 57.815-137.79 93.603-226.22 93.627v.142h-.212v-.142c-117.65-.047-220.526-63.662-276.015-158.352C16.076 434.13.143 378.926.131 320.07H-.01V319.846h.142c.036-88.383 35.812-168.345 93.592-226.125C151.515 35.93 231.512.153 319.93.13v-.142h.224V.13c88.371.047 168.321 35.8 226.101 93.58 57.816 57.792 93.592 137.789 93.616 226.219z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon question-mark-circle-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
