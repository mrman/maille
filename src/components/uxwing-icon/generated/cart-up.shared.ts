// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class CartUp implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M119.068 65.611h485.935c31.784 0 30.048 12.862 27.307 45.674l-18.047 230.389c-3.083 36.709-.473 27.815-39.39 33.378l-357.382 45.45 18.886 54.863c155.045-.024 229.975 0 384.95 0 5.232 19.5 12.283 51.19 17.504 72.922h-63.97l-6.13-22.205c-129.154 0-178.383-.012-307.585 0-70.56-1.181-63.473 17.894-80.399-43.052L59.434 42.225H1.181V0h102.12c4.76 17.776 11.338 47.658 15.767 65.611zM326.189 323.52h92.517v-93.804h83.221l-129.39-125.907-129.558 125.907h83.21v93.804zM280.81 541.247c27.26 0 49.36 22.11 49.36 49.383 0 27.272-22.1 49.37-49.36 49.37-27.271 0-49.382-22.099-49.382-49.37 0-27.272 22.11-49.383 49.383-49.383zm203.649 0c27.26 0 49.37 22.11 49.37 49.383 0 27.272-22.11 49.37-49.37 49.37-27.272 0-49.383-22.099-49.383-49.37 0-27.272 22.11-49.383 49.383-49.383z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon cart-up";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
