// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class Envelope implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M318.016 459.006l126.155-126.143 191.861 191.849H.001l191.861-191.849 126.155 126.143zM7.3 115.301l310.716 310.705 310.705-310.705H7.3zM0 478.176l164.72-164.719L0 148.738v329.437zm640 15.756L459.516 313.457 640 132.971v360.961z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon envelope";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
