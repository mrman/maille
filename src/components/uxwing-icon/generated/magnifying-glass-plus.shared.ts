// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class MagnifyingGlassPlus implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M353.839 232.007c6.449-.035 12.543 1.665 17.717 4.654l-.012.011a34.673 34.673 0 0 1 5.386 3.815 34.79 34.79 0 0 1 4.535 4.772h.024a34.747 34.747 0 0 1 7.43 21.615 35.3 35.3 0 0 1-4.832 17.787 35.523 35.523 0 0 1-9.555 10.642 36.107 36.107 0 0 1-6.224 3.673v.012a35.135 35.135 0 0 1-14.516 3.2l-58.926.426-.425 58.985a34.688 34.688 0 0 1-3.674 15.39 35.093 35.093 0 0 1-3.614 5.729l-.035.035a35.44 35.44 0 0 1-27.98 13.701v-.035c-3.249 0-6.379-.437-9.296-1.24a34.942 34.942 0 0 1-15.461-9.013c-4.205-4.216-7.347-9.531-8.917-15.425v-.035a35.044 35.044 0 0 1-1.146-9.213v-.024l.401-58.359-58.465.402a34.792 34.792 0 0 1-17.74-4.654l.012-.012a34.072 34.072 0 0 1-5.422-3.85c-1.63-1.406-3.142-3.024-4.5-4.748h-.023a34.772 34.772 0 0 1-7.43-21.614 35.288 35.288 0 0 1 4.831-17.753l-.024-.012c2.008-3.425 4.63-6.531 7.69-9.141h.023a35.353 35.353 0 0 1 10.359-6.142 35.305 35.305 0 0 1 12.248-2.256l58.937-.437.426-58.997a34.66 34.66 0 0 1 4.913-17.646l.024.012c1.193-1.996 2.492-3.815 3.874-5.35a35.143 35.143 0 0 1 4.842-4.477l.024-.023v.011c5.976-4.594 13.512-7.358 21.626-7.334a35.173 35.173 0 0 1 17.693 4.783 34.649 34.649 0 0 1 7.04 5.469l.023-.024a34.514 34.514 0 0 1 5.434 7.11 34.522 34.522 0 0 1 4.63 17.623l-.402 58.359 58.477-.402zM260.365 8.02h.083v.035c71.86.024 136.94 29.174 184.042 76.277 47.009 47.044 76.135 112.064 76.182 183.876h.036v.377h-.036c-.024 28.95-4.772 56.789-13.512 82.762-1.465 4.37-3 8.551-4.559 12.555v.036a260.163 260.163 0 0 1-28.902 53.681l151.478 136.147.095.083.815.756.059.059c8.55 8.138 13.204 19.04 13.795 30.107.579 10.937-2.823 22.122-10.3 31.098l-.058.083-.91 1.051-.189.19-.756.826-.082.118c-8.15 8.552-19.028 13.193-30.107 13.784-10.925.579-22.122-2.811-31.099-10.3l-.083-.059-1.05-.909-.154-.13-154.94-139.242a259.128 259.128 0 0 1-13.996 9.19c-6.425 3.932-13.063 7.641-19.854 11.019-34.902 17.398-74.316 27.201-115.997 27.201v.035h-.083v-.035c-71.87-.024-136.962-29.174-184.065-76.288C29.198 405.37.083 340.339.036 268.55H.001v-.248h.035C.06 196.43 29.21 131.34 76.313 84.237 123.357 37.228 188.377 8.102 260.189 8.055v-.036h.177zm.083 58.394v.024h-.26v-.024c-55.654.024-106.1 22.654-142.62 59.163-36.52 36.496-59.162 87-59.174 142.726h.036v.248h-.036c.036 55.642 22.642 106.076 59.162 142.608 36.509 36.52 87.001 59.162 142.726 59.174v-.036h.26v.036c55.643-.036 106.064-22.642 142.596-59.163 36.52-36.508 59.162-86.989 59.174-142.703h-.035v-.259h.035c-.035-55.655-22.654-106.112-59.162-142.632-36.508-36.509-86.99-59.15-142.703-59.163z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon magnifying-glass-plus";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
