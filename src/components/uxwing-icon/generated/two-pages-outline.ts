// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3774 4612","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class TwoPagesOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2708 4211l685-720h-685v720zm360-3865c0 63-51 115-115 115-63 0-115-51-115-115V230H229v3462h113c63 0 115 51 115 115 0 63-51 115-115 115H206c-57 0-108-23-145-60s-60-89-60-145V208c0-57 23-108 60-145S150 3 206 3h2657c56 0 108 23 145 60s60 88 60 145v140zm706 3053c0 52-35 97-83 110L2682 4570c-21 26-53 42-88 42H912c-57 0-108-23-145-60s-60-88-60-145V898c0-57 23-108 60-145s89-60 145-60h2657c56 0 108 23 145 60s60 88 60 145v2501zm-229-137V922H936v3462h1543V3377c0-63 51-115 115-115h951z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon two-pages-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
