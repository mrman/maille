// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class DesktopWindow implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M17.776 64.312h604.448c9.78 0 17.787 7.996 17.787 17.776v475.825c0 9.78-8.008 17.775-17.787 17.775H17.776c-9.78 0-17.788-7.996-17.788-17.775V82.088c0-9.78 8.008-17.776 17.788-17.776zm117.049 42.638c10.7 0 19.382 8.681 19.382 19.394 0 10.7-8.681 19.382-19.382 19.382-10.701 0-19.382-8.681-19.382-19.382 0-10.713 8.68-19.394 19.382-19.394zm402.017-2.587l13.122 14.446 13.122-14.434 10.252 9.331-14.008 15.402 14.008 15.414-10.252 9.33-13.122-14.433-13.122 14.433-10.252-9.33 14.008-15.414-14.008-15.402 10.252-9.342zm-461.25 2.587c10.7 0 19.382 8.681 19.382 19.394 0 10.7-8.681 19.382-19.382 19.382-10.701 0-19.394-8.681-19.394-19.382 0-10.713 8.693-19.394 19.394-19.394zm118.466 0c10.7 0 19.382 8.681 19.382 19.394 0 10.7-8.681 19.382-19.382 19.382-10.701 0-19.382-8.681-19.382-19.382 0-10.713 8.68-19.394 19.382-19.394zM73.194 182.801h486.112c9.224 0 16.76 7.76 16.76 16.76v308.342c0 9-7.76 16.76-16.76 16.76H73.194c-9 0-16.748-7.535-16.748-16.76V199.561c0-9.224 7.535-16.76 16.748-16.76z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon desktop-window";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
