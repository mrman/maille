// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 1440 3333","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class InfoIOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M12 3254v-487c0-44 36-80 80-80h98c3-2 6-5 11-10s9-10 11-14v-877c-2-4-6-9-12-15-1-1-2-2-3-4-3-3-5-5-7-6H80c-44 0-80-36-80-80v-518c0-44 36-80 80-80h956c70 0 123 30 159 89 28 46 42 110 42 192v1298c2 3 6 8 12 13 5 5 10 9 13 12h99c44 0 80 36 80 80v487c0 44-36 80-80 80H92c-44 0-80-36-80-80zm159-408v328h1110v-328h-25c-23 0-46-6-69-19-18-10-35-23-51-40-16-16-30-34-40-52-12-23-19-46-19-68V1364c0-52-6-89-19-110-5-8-12-12-23-12H159v359h37c23 0 47 7 70 20 16 9 31 21 46 37 2 1 3 3 5 5 16 17 28 34 37 51 11 22 17 44 17 66v887c0 21-5 41-15 62l-3 6c-9 17-21 34-37 51s-33 31-50 41c-23 13-46 19-70 19h-25zM722 904c-68 0-130-10-186-30-58-20-110-51-155-92-1-1-3-2-4-4-44-41-78-88-100-142-23-55-34-115-34-181s11-127 34-182c23-56 58-106 104-148l1-1c45-41 96-72 153-93S654 0 722 0c69 0 132 10 189 31 59 21 111 52 156 94 46 42 81 91 104 148 23 55 34 116 34 182s-11 127-34 181c-23 55-58 104-104 145-45 41-97 72-156 92-58 20-121 30-189 30zM588 724c39 14 84 21 134 21 52 0 97-7 137-21 38-13 72-33 101-60 28-25 50-55 64-89 15-35 22-75 22-120 0-46-7-87-22-122-14-34-35-64-64-90l-1-1c-29-27-63-47-101-61-40-14-85-21-137-21-50 0-95 7-133 21-37 13-70 33-99 60l-2 2c-29 26-50 57-64 90-15 35-22 76-22 122 0 45 7 85 22 120 14 32 34 61 60 85 1 1 3 2 4 3 29 27 63 47 101 60z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon info-i-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
