// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3886 3497","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class BellCancelOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M2527 3082c-11 58-31 112-59 160-29 50-67 95-114 135s-98 70-153 90-114 30-176 30-121-10-176-30-106-50-153-90-85-85-114-135-49-105-60-166c-5-29 14-58 44-63 1 0 10-1 10-1h899c30 0 54 24 54 54 0 5-1 10-2 15zM3759 468c45-21 98-1 118 44 21 45 1 98-44 118L125 2337c-45 21-98 1-118-44-21-45-1-98 44-118L3759 468zM2311 199c44 12 87 26 128 42 61 24 120 53 176 87 2 1 4 3 6 4 54 33 104 70 151 109 48 42 94 88 136 138 13 15 25 31 37 47l-166 76c-3-3-6-7-8-10-35-42-73-80-114-116-40-34-83-65-127-93l-6-3c-48-29-98-54-149-74-49-19-100-35-151-46-39-5-72-36-77-78-5-39-16-66-33-82-18-16-46-24-85-24s-66 7-84 22c-17 16-28 43-33 83-4 38-33 70-72 77-56 11-109 26-161 45-52 20-101 44-149 72-48 29-92 61-134 96-41 35-80 74-115 117-35 42-66 87-94 135-27 47-51 98-72 151-20 53-35 107-45 161s-15 112-15 171v189l-178 82v-271c0-68 6-136 18-203s30-132 54-193c23-62 52-121 84-177s70-109 111-159v-1c41-50 87-96 136-138s102-80 158-114c57-34 116-63 178-86 43-16 86-30 131-41 15-54 42-97 79-130 51-46 119-68 204-68 83 1 151 24 202 69 37 33 64 77 79 130zm851 955c7 51 10 103 10 155v219c0 70 1 139 1 206v1c0 60 4 121 12 182 7 60 18 118 32 174 14 55 33 108 57 159 25 52 54 104 90 154 35 49 78 98 130 146 54 50 116 98 187 146 41 27 52 83 24 123-17 26-45 39-74 39H415c-49 0-89-40-89-89 0-32 17-61 43-76 72-49 134-97 186-145s95-97 129-147c1-1 2-3 3-4 32-47 59-94 83-143l229-106c-17 59-38 117-65 173-28 60-62 119-100 175-1 2-2 4-4 5-41 59-93 117-155 175l-2 2h2699c-61-56-113-114-155-174-41-59-76-119-105-181-29-63-52-127-69-191-16-64-28-129-37-196-8-64-12-132-12-203-1-66-1-135-1-207 0-133-1-206-1-219 0-26-1-51-3-76l171-79z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon bell-cancel-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
