// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Airplane implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M198.652 621.563c0-55.1 30.284-81.036 70.123-110.93V366.478L7.145 482.712c0-100.608-34.24-93.237 47.116-144.356l.803-.496v-62.871h58.418v26.48l42.922-26.74v-70.442h58.418v34.052l53.953-33.615V55.891c0-20.93 22.737-38.043 50.528-38.043h.012c27.792 0 50.528 17.15 50.528 38.043v148.833l54.532 33.97v-34.407h58.418v70.796l42.934 26.73v-26.824h58.406v63.213c81.025 50.34 50.068 40.513 50.068 144.51L369.843 366.325V509.95c42.39 28.3 70.11 49.985 70.11 111.616l-66.697.58c-15.26-12.001-25.854-21.096-33.98-27.391-6.142 2.008-12.898 3.118-19.961 3.118h-.012c-7.985 0-15.58-1.417-22.312-3.933-8.752 6.378-20.338 15.815-37.547 28.205l-60.792-.579z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon airplane";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
