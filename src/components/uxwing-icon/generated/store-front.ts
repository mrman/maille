// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class StoreFront implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M115.076 0h415.033c9.72 0 17.681 7.96 17.681 17.693 0 9.732-7.96 17.693-17.681 17.693H115.076c-9.72 0-17.681-7.96-17.681-17.693C97.395 7.961 105.355 0 115.076 0zm510.467 224.826v21.366c.224 2.008.33 4.052.33 6.107 0 35.835-33.496 64.879-74.811 64.879-41.316 0-74.812-29.044-74.812-64.88 0-1.535.059-3.058.177-4.57h-2.988c.118 1.512.177 3.035.177 4.57 0 35.836-33.496 64.88-74.812 64.88-41.315 0-74.8-29.044-74.8-64.88 0-1.535.06-3.058.177-4.57h-2.988c.118 1.512.177 3.035.177 4.57 0 35.836-33.496 64.88-74.812 64.88-41.315 0-74.812-29.044-74.812-64.88 0-1.535.06-3.058.177-4.57h-1.866c.118 1.512.178 3.035.178 4.57 0 35.836-33.497 64.88-74.812 64.88-41.316 0-74.812-29.044-74.812-64.88 0-1.76.082-3.495.236-5.208v-22.264h604.696zM95.67 54.367h456.443l58.194 124.218H35.327L95.67 54.367zM25.63 562.578h63.13V363.076c1.04.06 2.067.094 3.107.13h.236l1.217.024h.212l.154.011h.827l.165.012h.72v.177H95.457v-.177c28.548-.012 54.934-9.106 75.533-24.213l.153.107.154.106.142.106.153.118.142.107.153.106.071.047.083.06.154.117.153.095.154.118.035.024.543.39.024.011.154.106.047.036.26.177.153.106.154.107.047.035.106.07.154.107.153.095.154.106.153.118.154.095.13.094.024.012.153.106.165.107.154.106.154.094.153.107.165.106.036.024 1.216.79.142.095.166.107.956.602.154.095 1.122.685.165.094.154.106.165.095.165.094.154.095.165.106.166.095.165.094v130.738H459.03v-127.76a121.128 121.128 0 0 0 12.957-7.855l.106-.083.048-.024.838-.59.071-.06.236-.165.236-.165.012-.012.425-.307.036-.023.047-.036.898-.65c20.693 15.355 47.28 24.627 76.1 24.627v.177h.058v-.177h.154v199.325h63.119c9.732 0 17.693 17.421 17.693 38.717 0 21.295-7.973 38.705-17.693 38.705H25.63c-9.71 0-17.682-17.41-17.682-38.705 0-21.296 7.961-38.717 17.682-38.717z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon store-front";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
