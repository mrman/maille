// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class ThumbsUpOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path fill-rule="nonzero" d="M326.213 81.237c2.93-14.835 10.571-24.366 21.047-29.209 8.516-3.933 18.414-4.335 28.725-1.618 8.953 2.35 18.367 7.134 27.39 14.008 24.426 18.59 47.292 53.623 50.222 97.43.897 13.465.65 27.887-.626 43.158-.84 10.099-2.162 20.705-3.91 31.784h103.903l.484.048c16.89.66 33.249 4.771 47.02 12 11.906 6.236 21.958 14.775 28.938 25.429 7.181 10.949 11.114 23.965 10.536 38.835-.437 11.044-3.39 23.02-9.414 35.8 3.46 14.386 5.032 29.8 2.823 43.323-1.866 11.504-6.343 21.733-14.386 29.316.413 19.016-2.126 34.949-7.24 48.744-5.256 14.174-13.134 25.938-23.245 36.355-.862 9.095-2.35 17.8-4.653 25.96-2.882 10.218-7.063 19.607-12.945 27.887l-.024-.012c-17.717 24.98-31.89 24.414-54.331 23.493-3.118-.118-6.473-.26-11.682-.26H301.587c-18.295 0-32.646-2.634-45.78-9.201-12.992-6.484-24.07-16.5-35.906-31.323l-3.035-8.622V297.51l10.311-2.775c26.22-7.087 46.831-29.48 62.847-56.304 16.465-27.58 28.016-59.894 35.918-85.997v-68.54l.272-2.658zM20.823 272.743h151.478c11.457 0 20.835 9.366 20.835 20.823v276.936c0 11.457-9.378 20.835-20.835 20.835H20.823C9.366 591.337 0 581.96 0 570.502V293.566c0-11.456 9.366-20.823 20.823-20.823zM358.87 77.197c-2.256 1.04-4.075 3.662-5.15 8.114v69.096l-.602 3.968c-8.374 27.981-20.906 63.39-39.343 94.265-17.327 29.032-39.863 53.954-69.13 65.47v211.526c8.03 9.52 15.436 15.992 23.527 20.031 8.941 4.465 19.465 6.272 33.414 6.272h203.258c3.626 0 8.362.189 12.768.366 13.31.543 21.709.886 30.709-11.811v-.06c3.897-5.503 6.756-12.023 8.823-19.322 2.196-7.784 3.472-16.418 4.11-25.63l4.287-9.119c8.941-8.468 15.804-18.023 20.138-29.728 4.477-12.071 6.402-26.623 5.327-44.824l-.484-8.232 6.992-4.347c4.772-2.964 7.382-8.586 8.504-15.449 1.772-10.913-.083-24.342-3.508-36.791l1.087-10.028c5.646-10.784 8.35-20.41 8.681-28.843.33-8.74-1.937-16.335-6.083-22.666-4.358-6.638-10.795-12.047-18.52-16.11-10.015-5.244-22.075-8.268-34.713-8.823v.035H415.883l3.094-16.429c2.988-15.886 5.126-31.04 6.33-45.402 1.182-14.079 1.418-27.118.627-39.036-2.339-34.985-20.256-62.705-39.379-77.257-6.094-4.642-12.153-7.795-17.622-9.236-4.122-1.075-7.606-1.134-10.063 0z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon thumbs-up-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
