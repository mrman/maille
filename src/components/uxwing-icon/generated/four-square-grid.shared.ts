// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","viewBox":"0 0 640 640","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class FourSquareGrid implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M19.69 344.992h249.356c10.654 0 19.359 10.347 19.359 22.985v242.082c0 12.65-8.705 22.996-19.359 22.996H19.69c-10.642 0-19.358-10.347-19.358-22.996V367.977c0-12.638 8.716-22.985 19.358-22.985zM370.952.815H620.31c10.654 0 19.359 10.347 19.359 22.996v242.082c0 12.65-8.705 22.985-19.359 22.985H370.953c-10.653 0-19.346-10.335-19.346-22.985V23.811c0-12.65 8.693-22.996 19.346-22.996zm-351.607 0h249.37c10.653 0 19.358 10.347 19.358 22.996v242.082c0 12.65-8.705 22.985-19.359 22.985H19.346C8.704 288.878 0 278.543 0 265.893V23.811C0 11.161 8.704.815 19.346.815zm351.937 350.307H620.64c10.642 0 19.359 10.347 19.359 22.996V616.2c0 12.638-8.717 22.985-19.359 22.985H371.283c-10.653 0-19.358-10.347-19.358-22.985V374.118c0-12.65 8.705-22.996 19.358-22.996z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon four-square-grid";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
