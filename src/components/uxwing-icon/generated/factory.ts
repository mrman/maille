// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Factory implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M16.477 250.007h38.977V22.181h100.418v227.826h37.005V76.406h77.186v173.601H370.21v-73.442l115.419 73.442h20.976v-73.442L622 250.007h1.512c9.06 0 16.489 7.43 16.489 16.488V601.33c0 9.06-7.43 16.489-16.489 16.489H16.477c-9.059 0-16.476-7.43-16.476-16.489V266.495c0-9.07 7.417-16.488 16.476-16.488zM394.99 411.548h-57.922a4.764 4.764 0 0 0-4.748 4.748v57.922a4.764 4.764 0 0 0 4.748 4.748h57.922a4.764 4.764 0 0 0 4.748-4.748v-57.922a4.764 4.764 0 0 0-4.748-4.748zm0-91.525h-57.922a4.764 4.764 0 0 0-4.748 4.748v57.922a4.764 4.764 0 0 0 4.748 4.748h57.922a4.764 4.764 0 0 0 4.748-4.748v-57.922a4.764 4.764 0 0 0-4.748-4.748zm39.106 0h57.922a4.764 4.764 0 0 1 4.748 4.748v57.922a4.764 4.764 0 0 1-4.748 4.748h-57.922a4.764 4.764 0 0 1-4.748-4.748v-57.922a4.764 4.764 0 0 1 4.748-4.748zm97.041 0h57.91c2.599 0 4.748 2.138 4.748 4.748v57.922c0 2.61-2.15 4.748-4.748 4.748h-57.91a4.756 4.756 0 0 1-4.748-4.748v-57.922a4.756 4.756 0 0 1 4.748-4.748zm0 91.525h57.91c2.599 0 4.748 2.138 4.748 4.748v57.922c0 2.61-2.15 4.748-4.748 4.748h-57.91a4.756 4.756 0 0 1-4.748-4.748v-57.922a4.756 4.756 0 0 1 4.748-4.748zm-97.04 0h57.921a4.764 4.764 0 0 1 4.748 4.748v57.922a4.764 4.764 0 0 1-4.748 4.748h-57.922a4.764 4.764 0 0 1-4.748-4.748v-57.922a4.764 4.764 0 0 1 4.748-4.748z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon factory";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
