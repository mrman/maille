// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","width":"64","height":"64","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd","viewBox":"0 0 640 640"};

export default class Crosshair implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M320 0c176.731 0 320 143.269 320 320S496.731 640 320 640 0 496.731 0 320 143.269 0 320 0zm32.634 94.796v101.01c0 8.208-6.661 14.882-14.87 14.882h-35.528c-8.209 0-14.87-6.674-14.87-14.883V94.796c-100.03 14.375-179.022 93.781-192.77 193.998h99.769c8.22 0 14.882 6.662 14.882 14.883v35.516c0 8.22-6.662 14.882-14.882 14.882H94.997c14.859 98.86 93.308 176.896 192.37 191.128v-98.127c0-8.221 6.66-14.883 14.87-14.883h35.527c8.21 0 14.87 6.662 14.87 14.883v98.127c99.061-14.233 177.51-92.27 192.37-191.128h-99.368c-8.22 0-14.882-6.662-14.882-14.882v-35.517c0-8.22 6.662-14.882 14.882-14.882h99.77c-13.749-100.218-92.742-179.624-192.77-193.998z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon crosshair";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
