// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3749 3749","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class LifeSaverOutline implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M1875 0c518 0 986 210 1326 549 339 339 549 808 549 1326s-210 986-549 1326c-339 339-808 549-1326 549s-986-210-1326-549C210 2862 0 2393 0 1875S210 889 549 549C888 210 1357 0 1875 0zm0 1034c91 0 178 14 260 41l661-654c-266-169-582-267-921-267-321 0-621 88-878 241l661 668c69-19 142-28 218-28zm403 103c71 39 135 87 191 143 63 63 117 137 157 218l641-634c-53-73-112-142-176-206-55-55-113-106-175-153l-639 632zm406 509c21 73 32 150 32 229 0 92-15 181-43 265l653 660c170-267 269-584 269-925 0-325-90-628-246-887l-665 658zm-73 635c-38 69-86 132-142 188-64 64-139 118-222 159l635 641c74-54 144-113 209-178 54-54 104-111 151-172l-631-638zm-511 404c-72 20-147 30-225 30-97 0-189-16-276-46l-658 651c269 174 589 275 933 275 323 0 626-89 884-244l-659-666zm-640-79c-66-37-126-83-179-136-67-67-123-145-164-231l-643 636c55 78 117 150 184 218 51 51 106 100 164 144l637-630zm-397-517c-18-68-27-140-27-213 0-95 16-186 45-272L429 945c-173 268-273 587-273 930 0 319 87 619 239 875l668-662zm81-625c38-67 84-128 138-182 66-66 143-121 228-162L875 478c-76 55-148 116-215 182-52 52-101 108-147 167l631 638zm1224-80c-126-126-300-204-492-204s-366 78-492 204-204 300-204 492 78 366 204 492 300 204 492 204 366-78 492-204 204-300 204-492-78-366-204-492z" fill-rule="nonzero"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon life-saver-outline";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
