// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

// This file was built automatically via the convert-ux-wing-svg.js script (v0.0.1)
// This icon was obtained from UXWing.com

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { GeneralIconAttrs, GeneralIconAttrName } from "../../../types";

// tslint:disable-next-line
const SVG_PROPS = {"xmlns":"http://www.w3.org/2000/svg","viewBox":"0 0 3184 3184","shape-rendering":"geometricPrecision","text-rendering":"geometricPrecision","image-rendering":"optimizeQuality","fill-rule":"evenodd","clip-rule":"evenodd"};

export default class CheckMarkSquare implements ClassComponent<GeneralIconAttrs> {
    protected readonly defaults: {[key: string]: any} = {
      height: "1em",
      width: "1em",
      fill: "black",
    };

    public view(vnode: CVnode<GeneralIconAttrs>) {
        return m(
          "svg",
          {...this.genAttrs(vnode)},
          // tslint:disable-next-line
          m.trust('<path d="M188 0h2808c52 0 99 21 133 55s55 81 55 133v2808c0 52-21 99-55 133s-81 55-133 55H188c-52 0-99-21-133-55s-55-81-55-133V188c0-52 21-99 55-133S136 0 188 0zm642 1763l440 399c10 9 22 16 34 21 13 5 26 7 40 6 13 0 26-3 38-9 12-5 23-12 32-22l1-1 1-1 942-986c10-10 17-22 22-35s7-27 7-40c0-14-3-27-9-40-5-12-13-24-24-33-10-10-22-17-35-22s-27-7-40-7c-14 0-27 3-40 9-12 5-24 13-33 24l-869 910-2 2-3-2-362-328c-11-10-22-17-35-21-13-5-27-7-40-6-14 1-27 4-39 10s-23 14-33 24l-1 1c-9 11-16 22-21 35s-7 27-6 40c1 14 4 27 10 39s14 23 24 33z"/>'),
        );
    }

  private genAttrs(vnode: CVnode<GeneralIconAttrs>) {
    const overrides = Object.values(GeneralIconAttrName)
      .reduce((acc: any, name: GeneralIconAttrName) => {
        if (!name) { return acc; }
        if (vnode && vnode && vnode.attrs[name]) {
          acc[name] = vnode.attrs[name];
          return acc;
        }

        if (name in this.defaults) {
          acc[name] = this.defaults[name];
        }

        return acc;
      }, {});

    const attrs: {[key: string]: any} = Object.assign(
      {},
      SVG_PROPS,
      overrides,
    );

    if (!("style" in attrs) || !attrs.style) { attrs.style = {}; }

    if (attrs.style && typeof attrs.style !== "object") {
      throw new Error("attr.style must be an object for icons");
    }

    attrs.style["vertical-align"] = "middle";
    attrs.style["min-width"] = "1.25em";

    const additionalClasses = "maille maille-uxwing-icon check-mark-square";
    attrs.className = attrs.className ? (attrs.className + " " + additionalClasses) : additionalClasses;

    return attrs;
  }
}
