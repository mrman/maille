import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import {
  CardImageOptions,
  CardImagePosition,
  Size,
} from "../../types";

interface CardAttrs {
  rounded?: boolean;
  shadowed?: boolean;
  shadowedOnHover?: boolean;
  bordered?: boolean;
  size?: Size;
  className?: string;

  image?: CardImageOptions;

  footer?: Children;
  header?: Children;
}

class Card implements ClassComponent<CardAttrs> {
  public view(vnode: Vnode<CardAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-card"]);

    // Add styling classes
    if (vnode.attrs.rounded) { classes.add("rounded"); }
    if (vnode.attrs.bordered) { classes.add("bordered"); }
    if (vnode.attrs.size) { classes.add(`size-${vnode.attrs.size}`); }
    if (vnode.attrs.shadowed) { classes.add("shadowed"); }
    if (vnode.attrs.shadowedOnHover) { classes.add("shadowed-on-hover"); }

    // Add classnames from the vnode if present
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    const className = [...classes].join(" ");

    // Build card component
    return m(
      "div",
      {className},
      [
        this.buildHeader(vnode),
        this.buildImage(vnode),
        m(".maille-card-content", vnode.children),
        this.buildFooter(vnode),
      ],
    );
  }

  protected buildFooter(vnode: Vnode<CardAttrs>): Children {
    if (!vnode.attrs.footer) { return null; }
    return m(".maile-card-footer", vnode.attrs.footer);
  }

  protected buildHeader(vnode: Vnode<CardAttrs>): Children {
    if (!vnode.attrs.header) { return null; }
    return m(".maile-card-header", vnode.attrs.header);
  }

  protected buildImage(vnode: Vnode<CardAttrs>): Children {
    if (!vnode || !vnode.attrs || !vnode.attrs.image) { return null; }
    const image = vnode.attrs.image;

    const imageClasses: string[] = [];
    if (image.fullWidth) { imageClasses.push("full-width"); }

    const className = imageClasses.join(" ");

    return m(
      ".maille-card-image-container",
      [
        m("img.maille-card-image-img", {className, src: image.src}),
      ],
    );
  }

}

export default Card;
