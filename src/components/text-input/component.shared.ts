// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { Size, InputUpdateFn, KeyupFn, KeyboardKeyCode, NoOpFn } from "../../types";

export interface TextInputAttrs {
  id?: string;
  className?: string;
  type?: string;
  size?: Size;
  rounded?: boolean;
  disabled?: boolean;
  placeholder?: string;

  name?: string;
  value?: string;

  onChange?: InputUpdateFn;
  onEnterPress?: InputUpdateFn;
}

class TextInput implements ClassComponent<TextInputAttrs> {
  protected value: string;

  constructor(vnode?: CVnode<TextInputAttrs>) {
    if (vnode) {
      if (vnode.attrs.value) { this.value = vnode.attrs.value; }
    }
  }

  public view(vnode: Vnode<TextInputAttrs>) {
    const classes = ["maille", "maille-text-input"];

    const inputType = vnode.attrs.type || "text";
    const onChange = vnode.attrs.onChange || NoOpFn;
    const onEnterPress = vnode.attrs.onEnterPress || NoOpFn;
    const size = vnode.attrs.size || Size.Medium;

    // If outlined, add the outline class
    if (vnode.attrs.rounded) { classes.push("rounded"); }
    if (vnode.attrs.disabled) { classes.push("disabled"); }
    classes.push(`size-${size}`);
    const className = classes.join(" ");

    return m(
      "input",
      {
        id: vnode.attrs.id,
        className,
        type: inputType,
        name: vnode.attrs.name,
        disabled: vnode.attrs.disabled,
        placeholder: vnode.attrs.placeholder,
        onkeyup: (e: any) => {
          // Don't redraw, too many will cause dropped events
          e.redraw = false;

          // If it's disabled don't do anything
          if (vnode.attrs.disabled) { return; }

          if (e.keyCode === KeyboardKeyCode.Enter) {
            onEnterPress(this.value, e);
            return;
          }

          if (!e.target) { return; }

          this.value = e.target.value;
          onChange(this.value, e);
        },
        value: this.value,
      },
    );
  }

}

export default TextInput;
