// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { CSSColor, CSSSize } from "../../types";

interface ColumnOptions {
  container?: {attrs?: object};
  content: Children;
}

interface TwoColumnLayoutAttrs {
  className?: string;

  // Components to put in different spots of the nav bar
  lhs?: ColumnOptions;
  rhs?: ColumnOptions;
}

class TwoColumnLayout implements ClassComponent<TwoColumnLayoutAttrs> {
  public buildLHS(vnode: Vnode<TwoColumnLayoutAttrs>): Children {
    let lhsAttrs = {};
    if (vnode.attrs.lhs && vnode.attrs.lhs.container && vnode.attrs.lhs.container.attrs) {
      lhsAttrs = {...vnode.attrs.lhs.container.attrs};
    }

    const lhsContent = vnode.attrs.lhs && vnode.attrs.lhs.content ? vnode.attrs.lhs.content : [];

    return m(".maille-two-column-layout-lhs-container", lhsAttrs, lhsContent);
  }

  public buildRHS(vnode: Vnode<TwoColumnLayoutAttrs>): Children {
    let rhsAttrs = {};
    if (vnode.attrs.rhs && vnode.attrs.rhs.container && vnode.attrs.rhs.container.attrs) {
      rhsAttrs = {...vnode.attrs.rhs.container.attrs};
    }

    const rhsContent = vnode.attrs.rhs && vnode.attrs.rhs.content ? vnode.attrs.rhs.content : [];

    return m(".maille-two-column-layout-rhs-container", rhsAttrs, rhsContent);
  }

  public view(vnode: Vnode<TwoColumnLayoutAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-two-column-layout"]);

    // Use list of state-managed classes to build className
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    const className = [...classes].join(" ");

    return m("div", {className}, [
      this.buildLHS(vnode),
      this.buildRHS(vnode),
    ]);
  }
}

export default TwoColumnLayout;
