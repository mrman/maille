// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { CSSColor, CSSSize } from "../../types";

interface NavBarAttrs {
  id?: string;
  className?: string;

  rounded?: boolean;

  // Components to put in different spots of the nav bar
  lhs?: Children;
  rhs?: Children;
  center?: Children;
}

class NavBar implements ClassComponent<NavBarAttrs> {
  public view(vnode: Vnode<NavBarAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-nav-bar"]);

    if (vnode.attrs.rounded) { classes.add("rounded"); }

    // Add classnames from the vnode if present
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    const className = [...classes].join(" ");
    const id = vnode.attrs.id;

    // Build the child containers
    const children: Children[] = [];
    if (vnode.attrs.lhs) { children.push(m(".maille-nav-bar-lhs-container", vnode.attrs.lhs)); }
    if (vnode.attrs.center) { children.push(m(".maille-nav-bar-center-container", vnode.attrs.center)); }
    if (vnode.attrs.rhs) { children.push(m(".maille-nav-bar-rhs-container", vnode.attrs.rhs)); }

    return m("div", {id, className}, children);
  }
}

export default NavBar;
