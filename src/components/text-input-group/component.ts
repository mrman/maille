import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode, VnodeDOM, ChildArray, Children } from "mithril";
import { MithrilEvent, NoOpFn, KeyboardKeyCode, InputUpdateFn, KeyupFn, ButtonClickFn } from "../../types";

import { default as TextInput, TextInputAttrs } from "../text-input/component";
import { default as Button, ButtonAttrs } from "../button/component";

export interface InputGroupAttrs {
  id?: string;
  className?: string;
  outlined?: boolean;
  rounded?: boolean;
  disabled?: boolean;

  inputAttrs: object;

  buttonAttrs: object;
  buttonChildren: Children;
}

class InputGroup implements ClassComponent<InputGroupAttrs> {
  protected value: string;

  public view(vnode: Vnode<InputGroupAttrs>) {
    const disabled = vnode.attrs.disabled || false;
    const rounded = vnode.attrs.rounded || false;
    const outlined = vnode.attrs.outlined || false;

    const classes = new Set(["maille", "maille-text-input-group", "flex"]);
    if (rounded) { classes.add("rounded"); }
    if (outlined) { classes.add("outlined"); }
    if (disabled) { classes.add("disabled"); }
    const className = [...classes].join(" ");

    const id = vnode.attrs.id;
    const inputAttrs: TextInputAttrs = Object.assign(
      {},
      vnode.attrs.inputAttrs,
      {disabled, rounded},
    );

    const buttonAttrs: ButtonAttrs = Object.assign(
      {},
      vnode.attrs.buttonAttrs,
      { disabled, rounded, outlined},
    );

    const buttonChildren = vnode.attrs.buttonChildren || [];

    // Build the container
    return m("span", {id, className}, [
      m(TextInput, inputAttrs),
      m(Button, buttonAttrs, buttonChildren),
    ]);
  }

}

export default InputGroup;
