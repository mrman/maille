// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { CSSColor, CSSSize } from "../../types";

interface RowOptions {
  container?: {attrs?: object};
  content: Children;
}

interface ThreeRowLayoutAttrs {
  className?: string;

  // Components to put in different spots of the nav bar
  header?: RowOptions;
  body?: RowOptions;
  footer?: RowOptions;
}

class ThreeRowLayout implements ClassComponent<ThreeRowLayoutAttrs> {
  public view(vnode: Vnode<ThreeRowLayoutAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-three-row-layout"]);

    // Build list of classes
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    const className = [...classes].join(" ");

    const children: Children = [
      this.buildHeader(vnode),
      this.buildBody(vnode),
      this.buildFooter(vnode),
    ];

    return m("div", {className}, children);
  }

  protected buildHeader(vnode: Vnode<ThreeRowLayoutAttrs>): Children {
    if (!vnode.attrs.header) { return null; }
    const header = vnode.attrs.header;

    // Build header content
    const headerAttrs = Object.assign(
      {},
      header.container && header.container.attrs ? header.container.attrs : {},
    );
    const headerContent = header.content ? header.content : [];

    return m(".maille-three-row-layout-header-container", headerAttrs, headerContent);
  }

  protected buildBody(vnode: Vnode<ThreeRowLayoutAttrs>): Children {
    if (!vnode.attrs.body) { return null; }
    const body = vnode.attrs.body;

    // Build body content
    const bodyAttrs = Object.assign(
      {},
      body.container && body.container.attrs ? body.container.attrs : {},
    );
    const bodyContent = body.content ? body.content : [];

    return m(".maille-three-row-layout-body-container", bodyAttrs, bodyContent);
  }

  protected buildFooter(vnode: Vnode<ThreeRowLayoutAttrs>): Children {
    if (!vnode.attrs.footer) { return null; }
    const footer = vnode.attrs.footer;

    // Build footer content
    const footerAttrs = Object.assign(
      {},
      footer.container && footer.container.attrs ? footer.container.attrs : {},
    );
    const footerContent = footer.content ? footer.content : [];

    return m(".maille-three-row-layout-footer-container", footerAttrs, footerContent);
  }

}

export default ThreeRowLayout;
