// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { CSSColor, CSSSize } from "../../types";

interface DividerAttrs {
  width?: CSSSize;
  className?: string;
}

class Divider implements ClassComponent<DividerAttrs> {
  public view(vnode: Vnode<DividerAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-divider"]);

    // Build list of classes
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    const className = [...classes].join(" ");

    const width = vnode.attrs.width || "80%";
    const style = {width: vnode.attrs.width};

    return m("hr", {style, className}, vnode.children);
  }
}

export default Divider;
