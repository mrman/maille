import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ClassComponent, CVnode, Vnode, VnodeDOM, ChildArray, Children } from "mithril";
import { MithrilEvent } from "../../types";

import RadioInput from "../radio-input";

export type RadioInputGroupOnChangeFn = (value: any, e: MithrilEvent) => any;

export interface RadioInputGroupAttrs {
  id?: string;
  className?: string;
  bordered?: boolean;
  rounded?: boolean;
  disabled?: boolean;
  inputStyle?: RadioInputStyle;

  name?: string;
  value?: any;

  // If on click returns a Promise, then it will be used to set the loading indicator after completion
  onchange?: RadioInputGroupOnChangeFn;

  options: RadioInputOption[];
}

export interface RadioInputOption {
  value: any;
  checked?: boolean;
  rounded?: boolean;
  label: Children;
}

let DEFAULT_NAME_ID = 0;

export enum RadioInputStyle {
  Default = "default",
  Button = "button",
}

class RadioInputGroup implements ClassComponent<RadioInputGroupAttrs> {
  protected value: any = undefined;
  protected name: string;
  protected options: RadioInputOption[] = [];
  protected children: ChildArray = [];
  protected changedOnce: boolean = false;
  protected disabled: boolean = false;
  protected rounded: boolean = false;
  protected inputStyle: RadioInputStyle = RadioInputStyle.Default;

  constructor(vnode?: CVnode<RadioInputGroupAttrs>) {
    if (vnode) {
      if (vnode.attrs.onchange) { this.internalOnChange = vnode.attrs.onchange; }
      if (vnode.attrs.value) { this.value = vnode.attrs.value; }
      if (vnode.attrs.options) { this.options = vnode.attrs.options; }
      if (vnode.attrs.inputStyle) { this.inputStyle = vnode.attrs.inputStyle; }
      if (typeof vnode.attrs.disabled !== "undefined") { this.disabled = vnode.attrs.disabled; }
      if (typeof vnode.attrs.rounded !== "undefined") { this.rounded = vnode.attrs.rounded; }
    }

    this.name = vnode && vnode.attrs.name ? vnode.attrs.name : `autoname-${++DEFAULT_NAME_ID}`;

  }

  public view(vnode: Vnode<RadioInputGroupAttrs>) {
    const classes = new Set(["maille", "maille-radio-input-group", "flex"]);

    // Fix the name if it's changed
    if (vnode.attrs.name) { this.name = vnode.attrs.name; }
    if (vnode.attrs.disabled) { this.disabled = vnode.attrs.disabled; }
    if (typeof vnode.attrs.rounded !== "undefined") { this.rounded = vnode.attrs.rounded; }
    if (vnode.attrs.value !== this.value) { this.value = vnode.attrs.value; }

    // If bordered, add the outline class
    if (this.rounded) { classes.add("rounded"); }
    if (this.disabled) { classes.add("disabled"); }
    if (vnode.attrs.bordered) { classes.add("bordered"); }

    // Build children InputGroup elements
    if (vnode.attrs.options && vnode.attrs.options !== this.options) { this.options = vnode.attrs.options; }
    this.children = this.buildChildren(this.options);

    // Build the container
    return this.buildContainer([...classes], vnode);
  }

  protected internalOnChange: RadioInputGroupOnChangeFn = () => undefined;

  protected buildContainer(classes: string[], vnode: Vnode<RadioInputGroupAttrs>): Children {
    return m(
      "div",
      {
        id: vnode.attrs.id,
        className: classes.join(" "),
        disabled: vnode.attrs.disabled,
      },
      this.children,
    );
  }

  protected buildChild(option: RadioInputOption, checked: boolean, idx: number): Children {
    return m(
      RadioInput,
      {
        value: option.value,
        rounded: option.rounded,
        name: this.name,
        disabled: this.disabled,
        checked,
        //  When any radio input we assume it's when it's checked
        oninput: (_: boolean, value: any, event: MithrilEvent) => {
          this.onchange(event, value);
        },
      },
      option.label,
    );
  }

  protected buildChildren(options: RadioInputOption[]): ChildArray {
    let foundChecked = false;

    // Build list of RadioInputs
    return options
      .map((o, idx) => {
        // Figure out of the radio input is currently selected
        const initiallyChecked = o.checked && !this.changedOnce;
        const checked = initiallyChecked || (this.value && o.value === this.value && !foundChecked);
        if (checked) { foundChecked = true; }

        return this.buildChild(o, checked, idx);
      });
  }

  protected getSelectedValue(e?: MithrilEvent): any {
    if (!e) { return; }

    // Return early if target is missing
    const target = e.target;
    if (!e.target) { return; }

    // ONE of the ones just came in here
    // need to figure out which one it is
    const selectedChild: Children = this.children.find(c => {
      if (!c) { return false; }
      if (typeof c !== "object") { return false; }
      if (!("dom" in c)) { return false; }

      const domObj = (c as VnodeDOM<any, any>).dom;
      // Since the dom object will actually be the label that is around the input itself,
      // we have to check if the children of the label contains the input
      return Array.from(domObj.children).includes(target as Element);
    });

    // Exit early if somehow we don't have the selected child
    if (!selectedChild) { return; }
    // Exit early if the child isn't an object
    if (typeof selectedChild !== "object") { return; }
    // Doesn't contain attributes (it should)
    if (!("attrs" in selectedChild)) { return; }

    return selectedChild.attrs.value;
  }

  // OnChange handler that enhanced passed in onchange function
  protected onchange(e: MithrilEvent, value: any) {
    // Mark that a change has happened once
    this.changedOnce = true;

    // Return early if we don't have any children somehow
    if (!this.children) { return; }

    // Set the current value of the group to the child's value
    this.value = this.getSelectedValue(e);

    // If there is no onchange then exit early
    if (!this.internalOnChange) { return Promise.resolve(); }

    // Perform onchange logic, converting the result to a promise if it isn't one
    const result = Promise.resolve(this.internalOnChange(this.value, e));

    // Resolve the promise and then reset the loading status
    return result
      .then(res => {
        // Trigger redraw that should change the radio input's state
        m.redraw();
        return res;
      });
  }

}

export default RadioInputGroup;
