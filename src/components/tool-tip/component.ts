import m from "mithril"; // +standalone
// declare var m: any; // +shared

import { ChildArray, Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { ToolTipActivation, ToolTipPosition } from "../../types";

interface ToolTipCloseOptions {
  key: any;
  fn?: (key: any) => void;
  elements?: Children;
}

interface ToolTipAttrs {
  title?: Children;
  body?: Children;
  rounded?: boolean;

  activation: ToolTipActivation;
  position: ToolTipPosition;

  className?: string;
  closeOptions?: ToolTipCloseOptions;
}

class ToolTip implements ClassComponent<ToolTipAttrs> {

  public view(vnode: Vnode<ToolTipAttrs>) {
    const classes: Set<string> = new Set(["maille", "maille-tool-tip"]);
    const wrapperClasses: Set<string> = new Set();

    const activation: ToolTipActivation = vnode.attrs.activation || ToolTipActivation.Hover;
    const position: ToolTipPosition = vnode.attrs.position || ToolTipPosition.Bottom;
    const {rounded} = vnode.attrs;

    // Add class names if provided
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    // If outlined, add the outline class
    if (rounded) { classes.add("rounded"); }

    // Add activation method class
    if (activation) { wrapperClasses.add(activation); }

    // Add position class
    if (position) { wrapperClasses.add(position); }

    // Build the final className for the tooltip itself and the wrapper
    const className = [...classes].join(" ");
    const wrapperClassName = [...wrapperClasses].join(" ");

    const {title, body, closeOptions} = vnode.attrs;

    const titleElements = [
      m(".maille-tooltip-title", title),
    ];

    // Add close options if configured
    if (closeOptions) {
      const closeHandler = () => {
        if (closeOptions.fn) {
          closeOptions.fn(closeOptions.key);
        }
      };

      const closeElement = closeOptions.elements || m.trust("&times;");
      titleElements.push(m("span.maille-tooltip-close-container", {onclick: closeHandler}, closeElement));
    }

    return m(
      ".maille.maille-tooltip",
      {className: wrapperClassName},
      [
        vnode.children,
        m(".maille.maille-tooltip-inner", {...vnode.attrs, className}, [
          m(".maille-tooltip-title", titleElements),
          m(".maille-tooltip-body", body),
        ]),
      ]);
  }

}

export default ToolTip;
