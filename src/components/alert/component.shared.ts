// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { Children } from "mithril";
import { ClassComponent, Vnode } from "mithril";
import { AlertType } from "../../types";

interface AlertCloseOptions {
  key: any;
  fn?: (key: any) => void;
  elements?: Children;
}

interface AlertAttrs {
  title?: Children;
  description?: Children;
  type?: AlertType;
  rounded?: boolean;
  footer?: Array<Vnode<any, any>>;

  className?: string;
  closeOptions?: AlertCloseOptions;
}

class Alert implements ClassComponent<AlertAttrs> {
  public view(vnode: Vnode<AlertAttrs>) {
    // Retrieve relevant attributes
    const {title, closeOptions, description, footer, rounded} = vnode.attrs;
    const alertType = vnode.attrs.type || AlertType.Default;

    // Build list of classes, combine with vnode
    const classes: Set<string> = new Set(["maille", "maille-alert"]);

    // If outlined, add the outline class
    if (rounded) { classes.add("rounded"); }
    // Add the alert type
    classes.add(alertType);

    // Add in className classes provided by vnode
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    // Build classname
    const className = [...classes].join(" ");

    // Build header elements
    const headerElements = [
      m(".maille-alert-title", title),
      description ? m(".maille-alert-description", description) : null,
    ];

    // Add close options if configured
    if (closeOptions) {
      const closeHandler = () => {
        if (closeOptions.fn) {
          closeOptions.fn(closeOptions.key);
        }
      };

      const closeElement = closeOptions.elements || m.trust("&times;");
      headerElements.push(m("span.maille-alert-close-container", {onclick: closeHandler}, closeElement));
    }

    return m(".maille.maille-alert", {...vnode.attrs, className}, [
      m(".maille-alert-header", headerElements),
      m(".maille-alert-body", vnode.children),
      footer ? m(".maille-alert-footer", footer) : null,
    ]);
  }

}

export default Alert;
