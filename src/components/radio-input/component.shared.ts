// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import { MithrilEvent, Size } from "../../types";

export type RadioInputOnInputFn = (checked: boolean, value: any, e: MithrilEvent) => any;

interface RadioInputAttrs {
  id?: string;
  className?: string;
  size?: Size;
  rounded?: boolean;
  round?: boolean;
  checked?: boolean;
  disabled?: boolean;
  name?: string;

  value?: any;

  // If on click returns a Promise, then it will be used to set the loading indicator after completion
  oninput?: RadioInputOnInputFn;
}

class RadioInput implements ClassComponent<RadioInputAttrs> {
  protected checked: any = false;
  protected value: any = undefined;

  constructor(vnode?: CVnode<RadioInputAttrs>) {
    if (vnode) {
      if (vnode.attrs.oninput) { this.internalOnInput = vnode.attrs.oninput; }
      if (vnode.attrs.value) { this.value = vnode.attrs.value; }
    }

    this.checked = vnode && "checked" in vnode.attrs ? vnode.attrs.checked : false;
  }

  public view(vnode: Vnode<RadioInputAttrs>) {
    const classes = ["maille", "maille-radio-input"];

    // If outlined, add the outline class
    if (vnode.attrs.rounded) { classes.push("rounded"); }
    if (vnode.attrs.round) { classes.push("round"); }
    if (vnode.attrs.disabled) { classes.push("disabled"); }

    // Update the value if it's changed
    if (vnode.attrs.value !== this.value) { this.value = vnode.attrs.value; }

    // Add additional styling classes
    const size = vnode.attrs.size || Size.Medium;
    classes.push(`size-${size}`);

    const className = classes.join(" ");

    const checked = "checked" in vnode.attrs ? vnode.attrs.checked : this.checked;

    const name = vnode.attrs.name;

    return m("label", {className}, [
      // The radio input itself
      m(
        "input[type='radio']",
        {
          id: vnode.attrs.id,
          checked,
          name,
          disabled: vnode.attrs.disabled,
          oninput: (e: MithrilEvent) => this.oninput(e),
        },
      ),

      // Label
      vnode.children,
    ]);
  }

  protected internalOnInput: RadioInputOnInputFn = () => undefined;

  // OnInput handler that enhanced passed in oninput function
  protected oninput(e: MithrilEvent) {
    this.checked = true;

    // If there is no oninput then exit early
    if (!this.internalOnInput) { return Promise.resolve(); }

    // Perform oninput logic, converting the result to a promise if it isn't one
    const result = Promise.resolve(this.internalOnInput(this.checked, this.value, e));

    // Resolve the promise and then reset the loading status
    return result
      .then(res => {
        // Trigger redraw that should change the radio input's state
        m.redraw();
        return res;
      });
  }

}

export default RadioInput;
