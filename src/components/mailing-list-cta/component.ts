import { default as m, Vnode, ClassComponent, Children } from "mithril";
import * as isEmail from "isemail";

import { Envelope, Rocket } from "../uxwing-icon";
import Card from "../card/component";
import Divider from "../divider/component";
import Button from "../button/component";

import { Size, ButtonType, KeyboardKeyCode } from "../../types";

export type SubscribeFn = (email: string) => void;

export interface SubscriptionCTAArgs {
  subscribeFn?: (email: string) => void;
  subscribeForm?: {
    action?: string;
  };

  errorMsg?: string;
  brief?: string;

  // Extra attributes for the form
  formAttrs?: object;

  // Extra attributes for the maille button
  mailleButtonAttrs: object;
  mailleButtonContents: Children;
}

const DEFAULT_ERROR_MSG = "Please enter a valid email address";
const DEFAULT_BRIEF = "Get updates emailed straight to your inbox";
const DEFAULT_MAILLE_BUTTON_CONTENTS = [ m(Envelope, {fill: "white"}), m("span.xs-margin-left", "Subscribe")];

export default class SubscriptionCTA implements ClassComponent<SubscriptionCTAArgs> {
  protected emailAddress: string = "";
  protected submitAttempted: boolean = false;
  protected subscribeFn: SubscribeFn = () => undefined;

  public view(vnode: Vnode<SubscriptionCTAArgs>): Vnode {
    if (vnode.attrs.subscribeForm && vnode.attrs.subscribeFn) {
      throw new Error("Both attrs.subscribeForm and attrs.subscribeFn should not be specified");
    }

    if (!vnode.attrs.subscribeForm && !vnode.attrs.subscribeFn) {
      throw new Error("Either attrs.subscribeForm (can be empty object) or attrs.subscribeFn must be specified");
    }

    // Update the internal subscribefn if it's changed
    if (vnode.attrs.subscribeFn) { this.subscribeFn = vnode.attrs.subscribeFn; }
    const brief = vnode.attrs.brief || DEFAULT_BRIEF;

    // If not email show error
    const invalidEmailErrorHolder: Children = [];
    if (this.submitAttempted && this.emailAddress && !isEmail.validate(this.emailAddress)) {
      const errorMsg = vnode.attrs.errorMsg ? vnode.attrs.errorMsg : DEFAULT_ERROR_MSG;
      invalidEmailErrorHolder.push(m("p.no-margin.error", errorMsg));
    }

    let buttonAttrs = {};
    let formContainerTag = "div";
    const formAttrs = Object.assign({}, vnode.attrs.formAttrs);
    // If a form is specified, some options change
    if (vnode.attrs.subscribeForm) {
      buttonAttrs = {type: "submit"};
      formContainerTag = "form";
      Object.assign(
        formAttrs,
        vnode.attrs.subscribeForm,
        {method: "POST"},
      );
    }

    const mailleButtonAttrs = vnode.attrs.mailleButtonAttrs || {};
    const mailleButtonContents: Children = vnode.attrs.mailleButtonContents || DEFAULT_MAILLE_BUTTON_CONTENTS;

    return m(
      Card,
      {
        className: "iblk maille maille-mailing-list-cta",
        rounded: true,
        bordered: true,
      },
      m(".inner-container.flex.sm-pad", [
        m("p", brief),

        m(formContainerTag, formAttrs, [
          m(
            "input.email-address",
            {
              type: "email",
              name: "email",
              placeholder: "your-email@domain.com",
              onchange: (e: any) => this.emailAddress = e.target.value,
              onkeyup: (e: any) => {
                // If subscribeForm was specified don't do any of this
                // native form functionality will do the work
                if (vnode.attrs.subscribeForm) { return; }

                e.redraw = false;
                if (e.keyCode === KeyboardKeyCode.Enter) {
                  e.redraw = true;
                  this.doSubscribe();
                }
              },
            },
          ),

          m(
            Button,
            {
              className: "subscribe",
              attrs: {
                type: "submit",
                ...buttonAttrs,
              },
              type: ButtonType.Primary,
              size: Size.Medium,
              ...mailleButtonAttrs,
              onclick: () => {
                // If subscribeForm was specified don't do any of this
                // native form functionality will do the work
                if (vnode.attrs.subscribeForm) { return; }

                this.doSubscribe();
              },
            },
            mailleButtonContents,
          ),
        ]),

        invalidEmailErrorHolder,
      ]),
    );
  }

  private doSubscribe() {
    this.submitAttempted = true;

    // If an email address isn't present or valid then return early
    if (!this.emailAddress || !isEmail.validate(this.emailAddress)) { return; }

    this.subscribeFn(this.emailAddress);
  }

}
