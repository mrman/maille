// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ChildArray, Children } from "mithril";
import { ClassComponent, CVnode, Vnode } from "mithril";
import { Size, TagType } from "../../types";

interface TagCloseOptions {
  key: any;
  fn?: (key: any) => void;
  elements?: Children;
}

interface TagAttrs {
  text?: string;
  type?: TagType;
  className?: string;
  style?: object;
  size?: Size;
  outlined?: boolean;
  rounded?: boolean;

  closeOptions?: TagCloseOptions;
}

class Tag implements ClassComponent<TagAttrs> {
  public view(vnode: Vnode<TagAttrs>) {
    const {closeOptions, text, size} = vnode.attrs;
    const tagType = vnode.attrs.type || TagType.Default;
    const tagSize = vnode.attrs.size || Size.Medium;

    const classes = new Set(["maille", "maille-tag"]);

    // Add class names if provided by the user
    if (vnode.attrs.className) {
      vnode.attrs.className.split(" ").forEach(c => classes.add(c));
    }

    // Add additional styling classes
    classes.add(tagType);
    classes.add(`size-${tagSize}`);
    if (vnode.attrs.outlined) { classes.add("outlined"); }
    if (vnode.attrs.rounded) { classes.add("rounded"); }

    // Build the className
    const className = [...classes].join(" ");

    // Build children
    let children = [m("span", vnode.attrs.text)] as ChildArray;

    // Use passed in children if available
    if (vnode.children && "length" in (vnode.children as any) && (vnode.children as any).length > 0) {
      children = vnode.children as ChildArray;
    }

    // Add removal times button on the right if close options are provided
    if (closeOptions && closeOptions.fn) {
      const closeHandler = () => {
        if (closeOptions.fn) {
          closeOptions.fn(closeOptions.key);
        }
      };

      children.push(
        m(
          "a",
          {onclick: closeHandler},
          closeOptions.elements || [m("span", {style: {cursor: "pointer"}}, m.trust(" &times;"))],
        ),
      );
    }

    const attrs = {
      style: vnode.attrs.style,
      className,
    };

    return m("span.maille.maille-tag", attrs, children);
  }

}

export default Tag;
