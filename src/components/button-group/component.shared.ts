// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY

//import m from "mithril"; // +standalone
 declare var m: any; 

import { ClassComponent, CVnode, Vnode } from "mithril";
import Button from "../button";

import { MithrilEvent } from "../../types";

export interface ButtonGroupAttrs {
  id?: string;
  className?: string;
}

class ButtonGroup implements ClassComponent<ButtonGroupAttrs> {
  public view(vnode: Vnode<ButtonGroupAttrs>) {
    return m(
      ".maille.maille-button-group",
      {
        id: vnode.attrs.id,
        className: vnode.attrs.className,
      },
      m(".maille-button-group-inner", vnode.children),
    );
  }
}

export default ButtonGroup;
