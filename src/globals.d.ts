// Allow imports of images
// https://github.com/Microsoft/TypeScript-React-Starter/issues/12
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.png";
declare module "*.gif";
declare module "*.svg";
