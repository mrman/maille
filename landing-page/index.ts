import m from "mithril";

import Alert from "../src/components/alert";
import Button from "../src/components/button";
import ButtonGroup from "../src/components/button-group";
import Card from "../src/components/card";
import Divider from "../src/components/divider";
import MailingListCTA from "../src/components/mailing-list-cta";
import NavBar from "../src/components/nav-bar";
import RadioInput from "../src/components/radio-input";
import RadioInputButtonGroup from "../src/components/radio-input-button-group";
import RadioInputGroup from "../src/components/radio-input-group";
import Switch from "../src/components/switch";
import Tag from "../src/components/tag";
import ThreeRowLayout from "../src/components/three-row-layout";
import ToolTip from "../src/components/tool-tip";
import TwoColumnLayout from "../src/components/two-column-layout";
import TextInput from "../src/components/text-input";
import TextInputGroup from "../src/components/text-input-group";

import chainmailSmallImageHref from "../static/images/mushroom-chain-mail-small.jpg";
import interFontSampleSvgHref from "../static/images/inter-font-sample.svg";
import mailleLogoSvgHref from "../static/images/maille-scale-logo.svg";

import * as UXWingIcon from "../src/components/uxwing-icon";
import * as CustomIcon from "../src/components/custom-icon";

import {
  AlertType,
  ButtonType,
  Size,
  TagType,
  ToolTipActivation,
  ToolTipPosition,
} from "../src/types";
import { waitMs } from "../src/util";

import "../static/fonts/Inter-3.1.0/inter.css";
import "./index.css";

import {version} from "../package.json";

const MAILLE_FONTLESS_JS_DOWNLOAD_LINK = "https://mrman.gitlab.io/maille/target/maille.fontless.min.js";
const MAILLE_JS_DOWNLOAD_LINK = "https://mrman.gitlab.io/maille/target/maille.min.js";
const MAILLE_CSS_DOWNLOAD_LINK = "https://mrman.gitlab.io/maille/target/maille.min.css";
const EXAMPLE_SINGLE_IMPORTS = [
  "https://mrman.gitlab.io/maille/target/button.shared.min.js",
  "https://mrman.gitlab.io/maille/target/button.shared.min.css",
  "https://mrman.gitlab.io/maille/target/uxwing-icon-globe-outline.shared.min.js",
  "https://mrman.gitlab.io/maille/target/three-row-layout.shared.min.js",
  "https://mrman.gitlab.io/maille/target/three-row-layout.shared.min.css",
];

const DemoApp = {
  view: () => m("main", [
    m(TwoColumnLayout, {
      className: "main-container",

      ////////////////////
      // Left Hand Side //
      ////////////////////
      lhs: {
        container: {attrs: {id: "main-container-lhs"}},
        content: [
          m(".maille-lhs-nav", [
            m("h2", `Maille (v${version})`),
            m("div.left-aligned-text", [
              m("ul.unstyled.section-list", [
                m("li.section-item", m("a.plain[href=#section-quick-start]", "Quick start")),

                m("li.section-item", m("a.plain[href=#section-styling]", "Styling")),

                m("li.section-item", m("a.plain[href=#section-font]", "Font")),

                m("li.section-item", [
                  m("a.plain[href=#section-components]", "Components"),
                  m("ul.unstyled", [
                    m("li", m("a.plain[href=#components-button]", m("code", "Button"))),
                    m("li", m("a.plain[href=#components-button-group]", m("code", "ButtonGroup"))),
                    m("li", m("a.plain[href=#components-uxwing-icon]", m("code", "UXWingIcon"))),
                    m("li", m("a.plain[href=#components-custom-icon]", m("code", "CustomIcon"))),
                    m("li", m("a.plain[href=#components-tag]", m("code", "Tag"))),
                    m("li", m("a.plain[href=#components-alert]", m("code", "Alert"))),
                    m("li", m("a.plain[href=#components-tool-tip]", m("code", "ToolTip"))),
                    m("li", m("a.plain[href=#components-switch]", m("code", "Switch"))),
                    m("li", m("a.plain[href=#components-text-input]", m("code", "TextInput"))),
                    m("li", m("a.plain[href=#components-text-input-group]", m("code", "TextInputGroup"))),
                    m("li", m("a.plain[href=#components-radio-input]", m("code", "RadioInput"))),
                    m("li", m("a.plain[href=#components-radio-input-group]", m("code", "RadioInputGroup"))),
                    m("li", [
                      m("a.plain[href=#components-radio-input-button-group]", m("code", "RadioInputButtonGroup"))
                    ]),
                    m("li", m("a.plain[href=#components-divider]", m("code", "Divider"))),
                    m("li", m("a.plain[href=#components-card]", m("code", "Card"))),
                    m("li", m("a.plain[href=#components-nav-bar]", m("code", "NavBar"))),
                    m("li", m("a.plain[href=#components-two-column-layout]", m("code", "TwoColumnLayout"))),
                    m("li", m("a.plain[href=#components-three-row-layout]", m("code", "ThreeRowLayout"))),
                    m("li", m("a.plain[href=#components-mailing-list-cta]", m("code", "MailingListCTA"))),
                  ]),
                ]),

              ]), // /ul.section-list
            ]),
          ]),
        ],
      },

      /////////////////////
      // Right Hand Side //
      /////////////////////
      rhs: {
        content: [
          m("#topnav", [
            m(
              NavBar,
              {
                id: "topnav-main",
                className: "sm-margin-bottom",
                lhs: m(".nav-bar-item", m("a.plain[href=#]", "Maille")),
                center: [
                  m(".nav-bar-item", m("a.plain[href=#section-styling]", "Quickstart")),
                  m(".nav-bar-item", m("a.plain[href=#section-styling]", "Styling")),
                  m(".nav-bar-item", m("a.plain[href=#section-font]", "Font")),
                  m(".nav-bar-item", m("a.plain[href=#section-components]", "Components")),
                ],
                rhs: [],
              }),
          ]),

          m(".jumbotron", [
            m("img", {src: mailleLogoSvgHref}),
            m("h1", "Maille"),
            m("p.no-margin", [
              m("span", "Maile is a "),
              m("a.plain[href=https://en.wikipedia.org/wiki/Free_and_open-source_software]", "FOSS"),
              m("span", " component library for "),
              m("a.plain[href=https://mithril.js.org]", "MithrilJS"),
              m("span", "."),
            ]),
          ]),

          ////////////////////////
          // Quickstart Section //
          ////////////////////////
          m("a.header-offset#section-quick-start"),
          m(".section-title", m("h2.underlined", "Quick start")),
          m(".section-container", [
            m("h3", "Import all of Maille"),
            m("p", [
              "You can use Maille on your page with the usual <script> (don't forget your ",
              m("a", {href: "https://www.srihash.org/"}, "subresource integrity hashes"),
              "!) and <link> tags  the minified JS and CSS:"
            ]),
            m("pre.code-block", `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="path/to/maille.min.css"/>
  </head>
  <body>
    <script src="path/to/mithril.min.js"></script>
    <script src="path/to/maille.min.js" integrity="sha384-<generated hash here>" crossorigin="anonymous"></script>
  </body>
</html>
`),

            m("p", "Download the latest completely (including SVG fonts) minified JS and CSS for Maille at the links below:"),
            m("ul", [
              m("li", [m(`a[href=${MAILLE_JS_DOWNLOAD_LINK}]`, MAILLE_JS_DOWNLOAD_LINK), m("span"," (~636KB)")]),
              m("li", [m(`a[href=${MAILLE_CSS_DOWNLOAD_LINK}]`, MAILLE_CSS_DOWNLOAD_LINK), m("span"," (~13KB)")]),
            ]),
            m("p", "The much lighter 'fontless' build which excludes the components housing SVG icons can also be downloaded:"),
            m("ul", [
              m("li", [m(`a[href=${MAILLE_FONTLESS_JS_DOWNLOAD_LINK}]`, MAILLE_FONTLESS_JS_DOWNLOAD_LINK), m("span", " (~52KB)")]),
            ]),

            m("h3", "Use pieces of Maille"),
            m("p", [
              "Single components from Maille can also be used individually, by using ",
              m("code", "import"),
              " :",
            ]),
            m("pre.code-block", `import { Alert } from "maille";
// import Alert from "maille/components/alert";

import { AlertType } from "maille/types";

// Render the Alert component to the document body
m.render(
  document.body,
  m(
    Alert,
    {title: "Info", type: AlertType.Info, description: "Alert description"},
    m("p", "A detailed explanation, anything can go here"),
  ),
);
`),

            m("h3", "Using pieces of Maille (Vanilla JS)"),
            m("p", "Vanilla JS projects can also use pieces of Maille:"),
            m("pre.code-block", `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="path/to/maille.alert.min.css"/>
  </head>
  <body>
    <script src="path/to/mithil.min.js"></script>
    <script src="path/to/alert.shared.min.js"></script>
    <script>
    var Alert = MAILLE_ALERT.default;

    // Render the component
    m.render(
      document.body,
      m(
        Alert,
        {title: "Info", type: "info", description: "Alert description"},
        m("p", "A detailed explanation, anything can go here"),
      ),
    );
    </script>
  </body>
</html>
`),
            m("p", [
              "With the code above, you can use the Alert component"
              " (via ", m("code", "MAILLE_ALERT.default"), " )",
              " without importing other pieces of Maille, in a simple browser context",
              " (without Typescript, dynamic loading, etc).",
            ]),

            m("p", [
              "Here are some examples of script paths you could include:",
              m("ul", EXAMPLE_SINGLE_IMPORTS.map(href => m("li", m("a", {href}, href)))),
              "When importing pieces of maille for use with a 'shared' copy of Mithril, filenames are of the format ",
              m("code", "<component>.shared.min.[js|css]"),
              " as appropriate.",
            ]),

            m("h3", "Importing Maille into your modern Javascript project"),
            m("p", "If you're working on a modern javascript project (for example with ParcelJS) you can add maille to your project by importing it:"),
            m("pre.code-block", `$ yarn add maille#${version}`)
            m("p", "Then you can use it from your pre-transpilation source code:"),
            m("pre.code-block", `import { ClassComponent, Vnode } from "mithril";

import { Alert } from "maille";
// import Alert from "maille/components/alert";

import { Envelope } from "maille/components/uxwing-icon"

interface ExampleAttrs = { ... };

class Example implements ClassComponent<ExampleAttrs> {

  public view(vnode: Vnode<CardAttrs>) {
    // ... other code ...

    // Build example component
    return m(
      "div",
      {className: "example-component"},
      [
        m(
          Alert,
          {title: "Default", description: "Alert description"},
          [m(Envelope), m("p", "A detailed explanation, anything can go here")],
        ),
        m("p", "The rest of the content for the component"),
      ],
    );
  }
}

export default Example;
`),

          m("p", [
            "You can find out what's new in recent versions of Maille by visiting ",
            m("a[href=https://gitlab.com/mrman/maille/blob/master/CHANGELOG.md]", "CHANGELOG.md on GitLab"),
          ]),

          ]),

          /////////////////////
          // Styling Section //
          /////////////////////
          m("a.header-offset#section-styling"),
          m(".section-title", m("h2.underlined", "Styling")),
          m(".section-container", [
            m("p", "You can customize the styling for Maille by using CSS variables at the top level, like so:"),
            m("pre.code-block", `:root {\n
  --maille-primary-color-bg: blue;
  --maille-primary-color-fg: white;
}
`),

            m("p", [
              "Outside of using CSS variables, Maille components have sensible classes like ",
              m("code", ".maille.maille-button"),
              " and ",
              m("code", ".maille.maille-alert"),
              ". ",
            ]),

            m("p", [
              "These classes (along with classes passed to the component via the ",
              m("code", "className"),
              " attribute) can be used to target elements for styilng as necessary.",
             ]),

          ]),

          //////////////////
          // Font Section //
          //////////////////
          m("a.header-offset#section-font"),
          m(".section-title", m("h2.underlined", "Font")),
          m("p", [
            "Use of the system fonts is encouraged, but ",
            m("a[href=https://rsms.me/inter/]", "Inter"),
            " is a great font to consider as well, which is why it's recommended for use with Maille.",
            " This page's ",
            m("code", "@font-face"),
            " property is set to Inter, and falls back to system fonts.",
          ]),
          m("p", "Here's a taste of what Inter looks like:"),
          m("img.quarter-height", {src: interFontSampleSvgHref}),

          // Component listing
          m("a.header-offset#section-components"),
          m(".section-title", m("h2.underlined", "Components")),
          // Buttons
          m("code.section-title#components-button", "m(Button)"),
          m("p.button-preview", [
            m(Button, "Default"),
            m(Button, {rounded: true}, "Rounded"),
            m(Button, {outlined: true}, "Outlined"),
            m(Button, {disabled: true}, "Disabled"),
            m(
              Button,
              {
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),
          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, "Default"),

m(Button, {rounded: true}, "Rounded"),

m(Button, {outlined: true}, "Outlined"),

m(Button, {disabled: true}, "Disabled"),

m(
  Button,
  {
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          m("p.button-preview", [
            m(Button, {type: ButtonType.Primary}, "Primary"),
            m(Button, {type: ButtonType.Primary, rounded: true}, "Primary (Rounded)"),
            m(Button, {type: ButtonType.Primary, outlined: true}, "Primary (Outlined)"),
            m(Button, {type: ButtonType.Primary, disabled: true}, "Primary (Disabled)"),
            m(
              Button,
              {
                type: ButtonType.Primary,
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, {type: ButtonType.Primary}, "Primary"),

m(Button, {type: ButtonType.Primary, rounded: true}, "Primary (Rounded)"),

m(Button, {type: ButtonType.Primary, outlined: true}, "Primary (Outlined)"),

m(Button, {type: ButtonType.Primary, disabled: true}, "Primary (Disabled)"),

m(
  Button,
  {
    type: ButtonType.Primary,
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          m("p.button-preview", [
            m(Button, {type: ButtonType.Success}, "Success"),
            m(Button, {type: ButtonType.Success, rounded: true}, "Success (Rounded)"),
            m(Button, {type: ButtonType.Success, outlined: true}, "Success (Outlined)"),
            m(Button, {type: ButtonType.Success, disabled: true}, "Success (Disabled)"),
            m(
              Button,
              {
                type: ButtonType.Success,
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, {type: ButtonType.Success}, "Success")

m(Button, {type: ButtonType.Success, rounded: true}, "Success (Rounded)")

m(Button, {type: ButtonType.Success, outlined: true}, "Success (Outlined)")

m(Button, {type: ButtonType.Success, disabled: true}, "Success (Disabled)")

m(
  Button,
  {
    type: ButtonType.Success,
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          m("p.button-preview", [
            m(Button, {type: ButtonType.Warning}, "Warning"),
            m(Button, {type: ButtonType.Warning, rounded: true}, "Warning (Rounded)"),
            m(Button, {type: ButtonType.Warning, outlined: true}, "Warning (Outlined)"),
            m(Button, {type: ButtonType.Warning, disabled: true}, "Warning (Disabled)"),
            m(
              Button,
              {
                type: ButtonType.Warning,
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, {type: ButtonType.Warning}, "Warning")

m(Button, {type: ButtonType.Warning, rounded: true}, "Warning (Rounded)")

m(Button, {type: ButtonType.Warning, outlined: true}, "Warning (Outlined)")

m(Button, {type: ButtonType.Warning, disabled: true}, "Warning (Disabled)")

m(
  Button,
  {
    type: ButtonType.Warning,
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          m("p.button-preview", [
            m(Button, {type: ButtonType.Error}, "Error"),
            m(Button, {type: ButtonType.Error, rounded: true}, "Error (Rounded)"),
            m(Button, {type: ButtonType.Error, outlined: true}, "Error (Outlined)"),
            m(Button, {type: ButtonType.Error, disabled: true}, "Error (Disabled)"),
            m(
              Button,
              {
                type: ButtonType.Error,
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, {type: ButtonType.Error}, "Error")

m(Button, {type: ButtonType.Error, rounded: true}, "Error (Rounded)")

m(Button, {type: ButtonType.Error, outlined: true}, "Error (Outlined)")

m(Button, {type: ButtonType.Error, disabled: true}, "Error (Disabled)")

m(
  Button,
  {
    type: ButtonType.Error,
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          m("p.button-preview", [
            m(Button, {type: ButtonType.Info}, "Info"),
            m(Button, {type: ButtonType.Info, rounded: true}, "Info (Rounded)"),
            m(Button, {type: ButtonType.Info, outlined: true}, "Info (Outlined)"),
            m(Button, {type: ButtonType.Info, disabled: true}, "Info (Disabled)"),
            m(
              Button,
              {
                type: ButtonType.Info,
                loadingSettings: {children: [m("span", "Loading...")]},
                // redraw only the children instead of triggering a global m.redraw()
                componentSettings: {redrawChildrenOnly: true},
                onclick: () => waitMs(1000),
              },
              "Loading (1s)",
            ),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Button, {type: ButtonType.Info}, "Info")

m(Button, {type: ButtonType.Info, rounded: true}, "Info (Rounded)")

m(Button, {type: ButtonType.Info, outlined: true}, "Info (Outlined)")

m(Button, {type: ButtonType.Info, disabled: true}, "Info (Disabled)")

m(
  Button,
  {
    type: ButtonType.Info,
    loadingSettings: {children: [m("span", "Loading...")]},
    // redraw only the children instead of triggering a global m.redraw()
    componentSettings: {redrawChildrenOnly: true},
    onclick: () => waitMs(1000),
  },
  "Loading (1s)",
),
`),

          // Button Groups
          m("code.section-title#components-button-group", "m(ButtonGroup)"),
          m(ButtonGroup, [
            m(Button, {type: ButtonType.Primary}, "Primary"),
            m(Button, {type: ButtonType.Success}, "Success"),
            m(Button, {type: ButtonType.Warning}, "Warning"),
            m(Button, {type: ButtonType.Error}, "Error"),
            m(Button, {type: ButtonType.Info}, "Info"),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(ButtonGroup, [
  m(Button, {type: ButtonType.Primary}, "Primary"),
  m(Button, {type: ButtonType.Success}, "Success"),
  m(Button, {type: ButtonType.Warning}, "Warning"),
  m(Button, {type: ButtonType.Error}, "Error"),
  m(Button, {type: ButtonType.Info}, "Info"),
]),
`),

          m(ButtonGroup, [
            m(Button, {type: ButtonType.Primary, rounded: true}, "Primary"),
            m(Button, {type: ButtonType.Success, rounded: true}, "Success"),
            m(Button, {type: ButtonType.Warning, rounded: true}, "Warning"),
            m(Button, {type: ButtonType.Error, rounded: true}, "Error"),
            m(Button, {type: ButtonType.Info, rounded: true}, "Info"),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(ButtonGroup, [
  m(Button, {type: ButtonType.Primary, rounded: true}, "Primary"),
  m(Button, {type: ButtonType.Success, rounded: true}, "Success"),
  m(Button, {type: ButtonType.Warning, rounded: true}, "Warning"),
  m(Button, {type: ButtonType.Error, rounded: true}, "Error"),
  m(Button, {type: ButtonType.Info, rounded: true}, "Info"),
]),
`),

          // UXWing Icons
          m("code.section-title#components-uxwing-icon", "m(UXWingIcon)"),
          m("p.note", ["These fonts were created by ", m("a[href=https://uxwing.com]", "UXWing")]),
          m(".flex.wrapped",
            Object.keys(UXWingIcon)
            .map((k) => m("span.icon-demo-container", [
              m((UXWingIcon as any)[k]),
              m("code.sm-margin-left", `m(${k})`),
            ])),
           ),
          m("p", "All corporate logos and trademarks are property of their respective entities"),

          // Custom Icons
          m("code.section-title#components-custom-icon", "m(CustomIcon)"),
          m(".flex.wrapped",
            Object.keys(CustomIcon)
            .map((k) => m("span.icon-demo-container", [
              m((CustomIcon as any)[k]),
              m("code.sm-margin-left", `m(${k})`),
            ])),
           ),
          m("p", "All corporate logos and trademarks are property of their respective entities"),

          // Tags
          m("code.section-title#components-tag", "m(Tag)"),
          m("h4", "Tag Sizing"),

          m("p", [
            m(Tag, {text: "Mini", rounded: true, size: Size.Mini}),
            m(Tag, {text: "Small", rounded: true, size: Size.Small}),
            m(Tag, {text: "Medium (default)", rounded: true}),
            m(Tag, {text: "Large", rounded: true, size: Size.Large}),
            m(Tag, {text: "XLarge", rounded: true, size: Size.XLarge}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Mini", rounded: true, size: Size.Mini}),

m(Tag, {text: "Small", rounded: true, size: Size.Small}),

m(Tag, {text: "Medium (default)", rounded: true}),

m(Tag, {text: "Large", rounded: true, size: Size.Large}),

m(Tag, {text: "XLarge", rounded: true, size: Size.XLarge}),
`),

          m("h4", "Tag Types"),

          m("p", [
            m(Tag, {text: "Default"}),
            m(Tag, {text: "Default (Outlined)", outlined: true}),
            m(Tag, {text: "Default (Rounded)", rounded: true}),
            m(Tag, {text: "Default (Rounded + Outlined)", rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Default"}),

m(Tag, {text: "Default (Outlined)", outlined: true}),

m(Tag, {text: "Default (Rounded)", rounded: true}),

m(Tag, {text: "Default (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("p", [
            m(Tag, {text: "Primary", type: TagType.Primary}),
            m(Tag, {text: "Primary (Outlined)", type: TagType.Primary, outlined: true}),
            m(Tag, {text: "Primary (Rounded)", type: TagType.Primary, rounded: true}),
            m(Tag, {text: "Primary (Rounded + Outlined)", type: TagType.Primary, rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Primary"}),

m(Tag, {text: "Primary (Outlined)", outlined: true}),

m(Tag, {text: "Primary (Rounded)", rounded: true}),

m(Tag, {text: "Primary (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("p", [
            m(Tag, {text: "Success", type: TagType.Success}),
            m(Tag, {text: "Success (Outlined)", type: TagType.Success, outlined: true}),
            m(Tag, {text: "Success (Rounded)", type: TagType.Success, rounded: true}),
            m(Tag, {text: "Success (Rounded + Outlined)", type: TagType.Success, rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Success"}),

m(Tag, {text: "Success (Outlined)", outlined: true}),

m(Tag, {text: "Success (Rounded)", rounded: true}),

m(Tag, {text: "Success (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("p", [
            m(Tag, {text: "Error", type: TagType.Error}),
            m(Tag, {text: "Error (Outlined)", type: TagType.Error, outlined: true}),
            m(Tag, {text: "Error (Rounded)", type: TagType.Error, rounded: true}),
            m(Tag, {text: "Error (Rounded + Outlined)", type: TagType.Error, rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Error"}),

m(Tag, {text: "Error (Outlined)", outlined: true}),

m(Tag, {text: "Error (Rounded)", rounded: true}),

m(Tag, {text: "Error (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("p", [
            m(Tag, {text: "Warning", type: TagType.Warning}),
            m(Tag, {text: "Warning (Outlined)", type: TagType.Warning, outlined: true}),
            m(Tag, {text: "Warning (Rounded)", type: TagType.Warning, rounded: true}),
            m(Tag, {text: "Warning (Rounded + Outlined)", type: TagType.Warning, rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Warning"}),

m(Tag, {text: "Warning (Outlined)", outlined: true}),

m(Tag, {text: "Warning (Rounded)", rounded: true}),

m(Tag, {text: "Warning (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("p", [
            m(Tag, {text: "Info", type: TagType.Info}),
            m(Tag, {text: "Info (Outlined)", type: TagType.Info, outlined: true}),
            m(Tag, {text: "Info (Rounded)", type: TagType.Info, rounded: true}),
            m(Tag, {text: "Info (Rounded + Outlined)", type: TagType.Info, rounded: true, outlined: true}),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {text: "Info"}),

m(Tag, {text: "Info (Outlined)", outlined: true}),

m(Tag, {text: "Info (Rounded)", rounded: true}),

m(Tag, {text: "Info (Rounded + Outlined)", rounded: true, outlined: true}),
`),

          m("h4", "Tags with custom color/style"),
          m("p", m(
            Tag,
            {rounded: true, style: {backgroundColor: "darkviolet", borderColor: "darkviolet"}},
            "custom color",
          )),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(
  "p",
  m(
    Tag,
    {
      rounded: true,
      style: {backgroundColor: "darkviolet", borderColor: "darkviolet"},
    },
    "custom color",
  )
),
`),

          m("h4", "Tags with custom child elements"),
          m("p", [
            m(Tag, {type: TagType.Info, rounded: true}, [
              m(UXWingIcon.Tag, {fill: "white"}),
              m("span", " tag icon"),
            ]),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {type: TagType.Info, rounded: true}, [
  m(UXWingIcon.Tag, {fill: "white"}),
  m("span", " tag icon"),
])
`),

          m("h4", "Tags with removal button"),
          m("p", [
            m(Tag, {
              closeOptions: {
                fn: (key: string) => alert(`close clicked on tag with key [${key}]`),
                key: 1,
              },
              rounded: true,
              text: "default close",
              type: TagType.Primary,
            }),
            m(Tag, {
              closeOptions: {
                // TODO: After login attribute passthrough fix, shouldn't have to use a span to get spacing
                // https://gitlab.com/mrman/maille/issues/54
                elements: [m("span", " "), m(UXWingIcon.CheckMark, {fill: "white"})],
                fn: (key: string) => alert(`close clicked on tag with key [${key}]`),
                key: 2,
              },
              rounded: true,
              text: "custom close",
              type: TagType.Success,
            }),
          ]),

          m("pre.code-block", `// the component(s) above correspond to the code below

m(Tag, {
  closeOptions: {
    fn: (key: string) => alert(\`close clicked on tag with key [\${key}]\`),
    key: 1,
  },
  rounded: true,
  text: "default close",
  type: TagType.Primary,
})

m(Tag, {
  closeOptions: {
    elements: [m("span", " "), m(UXWingIcon.CheckMark, {fill: "white"})],
    fn: (key: string) => alert(\`close clicked on tag with key [\${key}]\`),
    key: 2,
  },
  rounded: true,
  text: "custom close",
  type: TagType.Success,
})

`),

          // Alerts
          m("code.section-title#components-alert", "m(Alert)"),
          m("p", [
            m(
              Alert,
              {title: "Default", description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Default", description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

            m(
              Alert,
              {title: "Primary", type: AlertType.Primary, description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Primary", type: AlertType.Primary, description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

            m(
              Alert,
              {title: "Success", type: AlertType.Success, description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Success", type: AlertType.Success, description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

            m(
              Alert,
              {title: "Error", type: AlertType.Error, description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Error", type: AlertType.Error, description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

            m(
              Alert,
              {title: "Warning", type: AlertType.Warning, description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Warning", type: AlertType.Warning, description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

            m(
              Alert,
              {title: "Info", type: AlertType.Info, description: "Alert description"},
              m("p", "A detailed explanation, anything can go here"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {title: "Info", type: AlertType.Info, description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
),
`),

          ]),

          m("h4", "Alert with close button"),
          m(
            Alert,
            {
              closeOptions: {
                fn: (key: string) => alert(`Close clicked on Alert with key [${key}]`),
                key: "some-relevant-key",
              },
              description: "This one is closable, (try clicking the top right X)",
              title: "Info",
              type: AlertType.Info,
            },
            m("p", "A detailed explanation, anything can go here"),
          ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Alert,
  {
    closeOptions: {
      fn: (key: string) => alert(\`Close clicked on Alert with key [\${key}]\`),
      key: "some-relevant-key",
    },
    description: "This one is closable, (try clicking the top right X)",
    title: "Info",
    type: AlertType.Info,
  },
  m("p", "A detailed explanation, anything can go here"),
),
`),

          // Tool tips
          m("code.section-title#components-tool-tip", "m(ToolTip)"),
          m("p.tooltip-section", [
            m(
              ToolTip,
              {
                activation: ToolTipActivation.Hover,
                body: m("p.no-margin", "This is the bottom-mounted tooltip text"),
                position: ToolTipPosition.Top,
                title: "Bottom",
              },
              m("p.sm-margin-horiz", "Hover over this <p> element to see a bottom-mounted tooltip"),
            ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  ToolTip,
  {
    activation: ToolTipActivation.Hover,
    body: m("p.no-margin", "This is the bottom-mounted tooltip text"),
    position: ToolTipPosition.Top,
    title: "Bottom",
  },
  m("p.sm-margin-horiz", "Hover over this <p> element to see a bottom-mounted tooltip"),
),
`),

            m(
              ToolTip,
              {
                activation: ToolTipActivation.Hover,
                body: m("p.no-margin", "This is the top-mounted tooltip text"),
                position: ToolTipPosition.Bottom,
                title: "Top",
              },
              m("p.sm-margin-horiz", "Hover over this <p> element to see a top-mounted tooltip"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  ToolTip,
  {
    activation: ToolTipActivation.Hover,
    body: m("p.no-margin", "This is the top-mounted tooltip text"),
    position: ToolTipPosition.Bottom,
    title: "Top",
  },
  m("p.sm-margin-horiz", "Hover over this <p> element to see a top-mounted tooltip"),
),
`),
          ]),

          // Switch
          m("code.section-title#components-switch", "m(Switch)"),
          m("p.switch-section", [
            m(
              Switch,
              {
                  className: "xs-margin-right",
                  on: false,
                  onclick: (state: boolean) => alert(`Switch state changed to [${state}]`),
              },
              "regular switch",
            ),

            m(
              Switch,
              {
                  className: "xs-margin-right",
                  disabled: true,
                  on: false,
                  onclick: (state: boolean) => alert(`This should never fire!`),
              },
              "disabled switch (off position)",
            ),

            m(
              Switch,
              {
                  className: "xs-margin-right",
                  disabled: true,
                  on: true,
                  onclick: (state: boolean) => alert(`This should never fire!`),
              },
              "disabled switch (on position)",
            ),

          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Switch,
  {
      className: "xs-margin-right",
      on: false,
      onclick: (state: boolean) => alert(\`Switch state changed to [\${state}]\`),
  },
  "regular switch",
),

m(
  Switch,
  {
      className: "xs-margin-right",
      disabled: true,
      on: false,
      onclick: (state: boolean) => alert(\`This should never fire!\`),
  },
  "disabled switch (off position)",
),

m(
  Switch,
  {
      className: "xs-margin-right",
      disabled: true,
      on: true,
      onclick: (state: boolean) => alert(\`This should never fire!\`),
  },
  "disabled switch (on position)",
),
`),

          // TextInput
          m("code.section-title#components-text-input", "m(TextInput)"),
          m("p.switch-section", [
            m("h4", "Text input with placeholder"),
            m(
              TextInput,
              {
                className: "xs-margin-right",
                placeholder: "placeholder"
                onEnterPress: (value: string) => alert(`TextInput saw an enter press!, value is [${value}]`),
              },
            ),

            m("h4", "Rounded input"),
            m(
              TextInput,
              {
                className: "xs-margin-right",
                rounded: true,
                onEnterPress: (value: string) => alert(`TextInput saw an enter press!, value is [${value}]`),
                value: "initial value",
              },
            ),

            m("h4", "Disabled text input"),
            m(
              TextInput,
              {
                className: "xs-margin-right",
                value: "Disabled"
                disabled: true,
                rounded: true,
                onChange: (value: string) => alert("This should never fire!"),
                onEnterPress: () => alert("This should never fire!"),
              },
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  TextInput,
  {
    className: "xs-margin-right",
    placeholder: "placeholder"
    onInputChange: (value: string) => console.log(\`Plain TextInput value changed to [\${value}].\`),
    onEnterPress: () => alert("TextInput saw an enter press!"),
  },
),

m(
  TextInput,
  {
    className: "xs-margin-right",
    rounded: true,
    onChange: (value: string) => console.log(\`Rounded input value changed to [\${value}]\`),
    onEnterPress: () => alert("TextInput saw an enter press!"),
    value: "initial value",
  },
),

m(
  TextInput,
  {
    className: "xs-margin-right",
    value: "Disabled"
    disabled: true,
    rounded: true,
    onChange: (value: string) => alert("This should never fire!"),
    onEnterPress: () => alert("This should never fire!"),
  },
),
`),


          // TextInputGroup
          m("code.section-title#components-text-input-group", "m(TextInputGroup)"),
          m("p.switch-section", [
            m("h4", "Text input with info button"),
            m(
              TextInputGroup,
              {
                className: "xs-margin-right",

                inputAttrs: {
                  placeholder: "placeholder",
                  onInputChange: (value: string) => console.log(`Plain TextInputGroup value changed to [${value}].`),
                  onEnterPress: () => alert("TextInputGroup saw an enter press!"),
                },

                buttonAttrs: {
                  type: ButtonType.Primary,
                  onclick: () => alert("Button group with placeholder clicked"),
                },
                buttonChildren: "Primary",
              },
            ),

            m("h4", "Rounded input group"),
            m(
              TextInputGroup,
              {
                className: "xs-margin-right",
                rounded: true,

                inputAttrs: {
                  onChange: (value: string) => console.log(`Rounded input value changed to [${value}]`),
                  onEnterPress: () => alert("TextInputGroup saw an enter press!"),
                  value: "initial value",
                },

                buttonAttrs: {
                  type: ButtonType.Success
                  onclick: () => alert("Rounded button group clicked"),
                },
                buttonChildren: "Success",
              },
            ),

            m("h4", "Disabled text input group"),
            m(
              TextInputGroup,
              {
                rounded: true,
                className: "xs-margin-right",
                disabled: true,

                inputAttrs: {
                  value: "Disabled"
                  onChange: (value: string) => alert("This should never fire!"),
                  onEnterPress: () => alert("This should never fire!"),
                }

                buttonAttrs: {
                  type: ButtonType.Info,
                  onclick: () => alert("This should never fire!"),
                },
                buttonChildren: "Info",
              },
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  TextInputGroup,
  {
    className: "xs-margin-right",

    inputAttrs: {
      placeholder: "placeholder",
      onInputChange: (value: string) => console.log(\`Plain TextInputGroup value changed to [\${value}].\`),
      onEnterPress: () => alert("TextInputGroup saw an enter press!"),
    },

    buttonAttrs: {
      type: ButtonType.Primary,
      onclick: () => alert("Button group with placeholder clicked"),
    },
    buttonChildren: "Primary",
  },
),

m(
  TextInputGroup,
  {
    className: "xs-margin-right",
    rounded: true,

    inputAttrs: {
      onChange: (value: string) => console.log(\`Rounded input value changed to [\${value}]\`),
      onEnterPress: () => alert("TextInputGroup saw an enter press!"),
      value: "initial value",
    },

    buttonAttrs: {
      type: ButtonType.Success
      onclick: () => alert("Rounded button group clicked"),
    },
    buttonChildren: "Success",
  },
),

m(
  TextInputGroup,
  {
    rounded: true,
    className: "xs-margin-right",
    disabled: true,

    inputAttrs: {
      value: "Disabled"
      onChange: (value: string) => alert("This should never fire!"),
      onEnterPress: () => alert("This should never fire!"),
    }

    buttonAttrs: {
      type: ButtonType.Info,
      onclick: () => alert("This should never fire!"),
    },
    buttonChildren: "Info",
  },
),
`),


          // RadioInput
          m("code.section-title#components-radio-input", "m(RadioInput)"),
          m("p.no-margin", [
            "NOTE: Due to radio inputs semantics there is no way to de-select a radio input, see ",
            m("code", "RadioInputGroup"),
          ]),
          m("p.switch-section", [
            m(
              RadioInput,
              {
                className: "xs-margin-right",
                value: "this is value"
                onchange: (checked: boolean, value: any) => {
                  const msg = `RadioInput value is ${checked ? "checked" : "not checked"}, value is ${JSON.stringify(value)}.`
                  alert(msg);
                }
              },
              "radio input",
            ),

            m(
              RadioInput,
              {
                className: "xs-margin-right",
                disabled: true,
                checked: true,
                onchange: (state: boolean) => alert("It's already checked, so this should never fire!"),
              },
              "radio input that is already checked",
            ),

            m(
              RadioInput,
              {
                className: "xs-margin-right",
                disabled: true,
                onchange: (state: boolean) => alert("This should never fire!"),
              },
              "disabled radio input",
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInput,
  {
    className: "xs-margin-right",
    value: "this is value"
    onchange: (checked: boolean, value: any) => {
      const msg = \`RadioInput value is \${checked ? "checked" : "not checked"}, value is \${JSON.stringify(value)}.\`
      alert(msg);
    }
  },
  "radio input",
),

m(
  RadioInput,
  {
    className: "xs-margin-right",
    disabled: true,
    checked: true,
    onchange: (state: boolean) => alert("It's already checked, so this should never fire!"),
  },
  "radio input that is already checked",
),

m(
  RadioInput,
  {
    className: "xs-margin-right",
    disabled: true,
    onchange: (state: boolean) => alert("This should never fire!"),
  },
  "disabled radio input",
),
`),


          // RadioInputGroup
          m("code.section-title#components-radio-input-group", "m(RadioInputGroup)"),
          m("p.switch-section", [

            m("h4", "Basic radio input group"),
            m(
              RadioInputGroup,
              {
                options: [
                  {value: "option-1", label: "Option 1" },
                  {value: "option-2", label: "Option 2" },
                  {value: "option-3", label: "Option 3" },
                ],
                onchange: (value: any) => {
                  const msg = `RadioInputGroup option selected, value is ${JSON.stringify(value)}.`
                  alert(msg);
                }
              },
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInputGroup,
  {
    options: [
      {value: "option-1", label: "Option 1" },
      {value: "option-2", label: "Option 2" },
      {value: "option-3", label: "Option 3" },
    ],
    onchange: (value: any) => {
      const msg = \`RadioInputGroup option selected, value is \${JSON.stringify(value)}.\`
      alert(msg);
    }
  },
),
`),

            m("h4", "Bordered & rounded"),
            m(
              RadioInputGroup,
              {
                options: [
                  {value: "option-1", label: "Option 1" },
                  {value: "option-2", label: "Option 2" },
                  {value: "option-3", label: "Option 3" },
                ],
                bordered: true,
                rounded: true,
                onchange: (value: any) => {
                  const msg = `RadioInputGroup option selected, value is ${JSON.stringify(value)}.`
                  alert(msg);
                }
              },
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInputGroup,
  {
    options: [
      {value: "option-1", label: "Option 1" },
      {value: "option-2", label: "Option 2" },
      {value: "option-3", label: "Option 3" },
    ],
    bordered: true,
    rounded: true,
    onchange: (value: any) => {
      const msg = \`RadioInputGroup option selected, value is \${JSON.stringify(value)}.\`
      alert(msg);
    }
  },
),
`),

            m("h4", "With a pre-selection"),
            m(
              RadioInputGroup,
              {
                options: [
                  {value: "option-1", label: "Option 1" },
                  {value: "pre-selected", checked: true, label: "Pre-selected" },
                  {value: "option-3", label: "Option 3" },
                ],
              },
              onchange: (value: any) => {
                const msg = `RadioInputGroup option selected, value is ${JSON.stringify(value)}.`
                alert(msg);
              }
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInputGroup,
  {
    options: [
      {value: "option-1", label: "Option 1" },
      {value: "pre-selected", checked: true, label: "Pre-selected" },
      {value: "option-3", label: "Option 3" },
    ],
  },
  onchange: (value: any) => {
    const msg = \`RadioInputGroup option selected, value is \${JSON.stringify(value)}.\`
    alert(msg);
  }
),
`),


            m("h4", "Disabled radio input group"),
            m(
              RadioInputGroup,
              {
                options: [
                  {value: "option-1", label: "Option 1" },
                  {value: "option-2", label: "Option 2" },
                  {value: "option-3", label: "Option 3" },
                ],
                disabled: true,
                onchange: () => alert("This should never fire!"),
              }
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInputGroup,
  {
    options: [
      {value: "option-1", label: "Option 1" },
      {value: "option-2", label: "Option 2" },
      {value: "option-3", label: "Option 3" },
    ],
    disabled: true,
    onchange: () => alert("This should never fire!"),
  }
),
`),

          // RadioInputButtonGroup
          m("code.section-title#components-radio-input-button-group", "m(RadioInputButtonGroup)"),
          m("p.switch-section", [
            m(
              RadioInputButtonGroup,
              {
                options: [
                  {value: "option-1", checked: true, label: "Option 1" },
                  {value: "option-2", label: "Option 2" },
                  {value: "option-3", label: "Option 3" },
                ],
                rounded: true,
                onchange: (value: any) => {
                  const msg = `RadioInputButtonGroup option selected, value is ${JSON.stringify(value)}.`
                  alert(msg);
                }
              },
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  RadioInputButtonGroup,
  {
    options: [
      {value: "option-1", checked: true, label: "Option 1" },
      {value: "option-2", label: "Option 2" },
      {value: "option-3", label: "Option 3" },
    ],
    rounded: true,
    onchange: (value: any) => {
      const msg = \`RadioInputButtonGroup option selected, value is \${JSON.stringify(value)}.\`
      alert(msg);
    }
  },
),
`),

          // Divider
          m("code.section-title#components-divider", "m(Divider)"),
          m("p.divider-section", [
            m("h4.no-margin", "Plain divider"),
            m(Divider, {className: "sm-margin-bottom"}),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(Divider, {className: "sm-margin-bottom"}),
`),

            m("h4", "Divider with text"),
            m(
              Divider,
              m("span.xs-padding-horiz", {style: {backgroundColor: "white"}}, "padded textual child element"),
            ),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Divider,
  m("span.xs-padding-horiz", {style: {backgroundColor: "white"}}, "padded textual child element"),
),
`),
          ]),

          // Card
          m("code.section-title#components-card", "m(Card)"),
          m("h4", "Card sizes"),
          m("p.card-section.flex.wrapped", [
            m(
              Card,
              {rounded: true, bordered: true, size: Size.Small},
              [
                m(".lg-text.sm-pad", "Small Size"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is small size"),
                ]),
              ],
            ),

            m(
              Card,
              {rounded: true, bordered: true, size: Size.Medium},
              [
                m(".lg-text.sm-pad", "Medium Size"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is medium size"),
                ]),
              ],
            ),

            m(
              Card,
              {rounded: true, bordered: true, size: Size.Large},
              [
                m(".lg-text.sm-pad", "Large Size"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is large size"),
                ]),
              ],
            ),

            m(
              Card,
              {rounded: true, bordered: true, size: Size.XLarge},
              [
                m(".lg-text.sm-pad", "XLarge Size"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is xlarge size"),
                ]),
              ],
            ),

          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Card,
  {rounded: true, bordered: true, size: Size.Small},
  [
    m(".lg-text.sm-pad", "Small Size"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is small size"),
    ]),
  ],
),

m(
  Card,
  {rounded: true, bordered: true, size: Size.Medium},
  [
    m(".lg-text.sm-pad", "Medium Size"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is medium size"),
    ]),
  ],
),

m(
  Card,
  {rounded: true, bordered: true, size: Size.Large},
  [
    m(".lg-text.sm-pad", "Large Size"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is large size"),
    ]),
  ],
),

m(
  Card,
  {rounded: true, bordered: true, size: Size.XLarge},
  [
    m(".lg-text.sm-pad", "XLarge Size"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is xlarge size"),
    ]),
  ],
),
`),

          m("h4", "Card examples"),
          m("p.card-section.flex.wrapped", [
            m(
              Card,
              {rounded: true, shadowed: true, size: Size.Large},
              [
                m(".lg-text.sm-pad", "Rounded & Shadowed"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is rounded and has a shadow"),
                ]),
              ],
            ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
   Card,
   {rounded: true, shadowed: true, size: Size.Large},
   [
     m(".lg-text.sm-pad", "Rounded & Shadowed"),
     m(".maille-card-body.sm-pad", [
       m("p", "This card is rounded and has a shadow"),
     ]),
   ],
 ),
`),

            m(
              Card,
              {rounded: true, bordered: true, shadowedOnHover: true, size: Size.Large},
              [
                m(".lg-text.sm-pad", "Rounded & Bordered"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is rounded and has a border"),
                  m("p", "This card also shows a shadow when it's hovered over"),
                ]),
              ],
            ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Card,
  {rounded: true, bordered: true, shadowedOnHover: true, size: Size.Large},
  [
    m(".lg-text.sm-pad", "Rounded & Bordered"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is rounded and has a border"),
      m("p", "This card also shows a shadow when it's hovered over"),
    ]),
  ],
),
`),

            m(
              Card,
              {
                rounded: true,
                bordered: true,
                image: {fullWidth: true, src: chainmailSmallImageHref},
                size: Size.XLarge,
              },
              [
                m(".lg-text.sm-pad", "Rounded with an image"),
                m(".maille-card-body.sm-pad", [
                  m("p", "This card is rounded and has an image"),
                ]),
              ],
            ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Card,
  {
    rounded: true,
    bordered: true,
    image: {fullWidth: true, src: chainmailSmallImageHref},
    size: Size.XLarge,
  },
  [
    m(".lg-text.sm-pad", "Rounded with an image"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is rounded and has an image"),
    ]),
  ],
),
`),

            m(
              Card,
              {
                bordered: true,
                size: Size.XLarge,
                rounded: true,
                header: m(".maille-card-header.sm-pad", {style: {backgroundColor: "black", color: "white"}}, [
                  m("h2.no-margin", "Header & Footer"),
                ]),
                footer: m(".maille-card-footer", [
                  m(
                    Button,
                    {className: "full-width no-margin no-rounded-top", type: ButtonType.Info, rounded: true},
                    "Footer Button with some extra style classes",
                  ),
                ]),
              },
              [
                m(".maille-card-body.sm-pad", [
                  m("p", "This card has a header and footer instead of a title with larger text"),
                  m("p", "This card's footer is styled to fit the card as smoothly as possible"),
                ]),
              ],
            ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  Card,
  {
    bordered: true,
    size: Size.XLarge,
    rounded: true,
    header: m(".maille-card-header.sm-pad", {style: {backgroundColor: "black", color: "white"}}, [
      m("h2.no-margin", "Header & Footer"),
    ]),
    footer: m(".maille-card-footer", [
      m(
        Button,
        {className: "full-width no-margin no-rounded-top", type: ButtonType.Info, rounded: true},
        "Footer Button with some extra style classes",
      ),
    ]),
  },
  [
    m(".maille-card-body.sm-pad", [
      m("p", "This card has a header and footer instead of a title with larger text"),
      m("p", "This card's footer is styled to fit the card as smoothly as possible"),
    ]),
  ],
),
`),
          ]),

          // NavBar
          m("code.section-title#components-nav-bar", "m(NavBar)"),
          m("p.nav-bar-section.flex.vertical", [
            m(
              NavBar,
              {
                className: "navbar sm-margin-bottom",
                lhs: m("div", "Basic NavBar"),
                center: [
                  m(".nav-bar-item", m("span", "Center-Nav-1")),
                  m(".nav-bar-item", m("span", "Center-Nav-2")),
                ],
                rhs: [
                  m(".nav-bar-item", m("span", "RHS-Nav-1")),
                  m(".nav-bar-item", m("span", "RHS-Nav-2")),
                ],
              }),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  NavBar,
  {
    className: "navbar sm-margin-bottom",
    lhs: m("div", "Basic NavBar"),
    center: [
      m(".nav-bar-item", m("span", "Center-Nav-1")),
      m(".nav-bar-item", m("span", "Center-Nav-2")),
    ],
    rhs: [
      m(".nav-bar-item", m("span", "RHS-Nav-1")),
      m(".nav-bar-item", m("span", "RHS-Nav-2")),
    ],
  })
`),

            m(
              NavBar,
              {
                className: "navbar",
                rounded: true,
                lhs: m("div", "Rounded NavBar"),
                center: [
                  m(".nav-bar-item", m("span", "Center-Nav-1")),
                  m(".nav-bar-item", m("span", "Center-Nav-2")),
                ],
                rhs: [
                  m(".nav-bar-item", m("span", "RHS-Nav-1")),
                  m(".nav-bar-item", m("span", "RHS-Nav-2")),
                ],
              }),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  NavBar,
  {
    className: "navbar",
    rounded: true,
    lhs: m("div", "Rounded NavBar"),
    center: [
      m(".nav-bar-item", m("span", "Center-Nav-1")),
      m(".nav-bar-item", m("span", "Center-Nav-2")),
    ],
    rhs: [
      m(".nav-bar-item", m("span", "RHS-Nav-1")),
      m(".nav-bar-item", m("span", "RHS-Nav-2")),
    ],
  }),
`),

          ]),

          // Two Column Layout
          m("code.section-title#components-two-column-layout", "m(TwoColumnLayout)"),
          m("p", "NOTE: Container is bordered & shrunk to 90% of page to showcase columns"),
          m("p.two-column-layout-section.flex.centered", [
            m(".bordered-example-container.two-column-layout-container-example-1",
              m(
                TwoColumnLayout,
                {
                  className: "two-column-layout sm-margin-bottom",
                  lhs: {
                    container: {attrs: {style: "flex-basis: 30%"}},
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "LHS (30% width)"),
                      m("p", "content for the left hand side goes here"),
                    ]),
                  },

                  rhs: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "RHS (width unspecified)"),
                      m("p", "content for the right hand side goes here"),
                    ]),
                  },
                }),
             ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

 m(
   TwoColumnLayout,
   {
     className: "two-column-layout sm-margin-bottom",
     lhs: {
       container: {attrs: {style: "flex-basis: 30%"}},
       content: m(".flex.vertical", [
         m("h2.no-margin", "LHS (30% width)"),
         m("p", "content for the left hand side goes here"),
       ]),
     },
     rhs: {
       content: m(".flex.vertical", [
         m("h2.no-margin", "RHS (width unspecified)"),
         m("p", "content for the right hand side goes here"),
       ]),
     },
   }),
)
`),

          // Three Row Layout
          m("code.section-title#components-three-row-layout", "m(ThreeRowLayout)"),
          m("p", "NOTE: Container is bordered, shrunk to 90% of page and 500px height to showcase rows"),
          m("p.three-row-layout-section.flex.vertical.centered", [
            m(".bordered-example-container.three-row-layout-container-example-1",
              m(
                ThreeRowLayout,
                {
                  className: "three-row-layout sm-margin-bottom",
                  header: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Header"),
                      m("p", "content for the header goes here"),
                    ]),
                  },

                  body: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Body"),
                      m("p", "content for the body goes here"),
                    ]),
                  },

                  footer: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Footer"),
                      m("p", "content for the footer goes here"),
                    ]),
                  },
                }),
             ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  ThreeRowLayout,
  {
    className: "three-row-layout sm-margin-bottom",
    header: {
      content: m(".flex.vertical", [
        m("h2.no-margin", "Header"),
        m("p", "content for the header goes here"),
      ]),
    },
    body: {
      content: m(".flex.vertical", [
        m("h2.no-margin", "Body"),
        m("p", "content for the body goes here"),
      ]),
    },
    footer: {
      content: m(".flex.vertical", [
        m("h2.no-margin", "Footer"),
        m("p", "content for the footer goes here"),
      ]),
    },
  }),
`),

            m(".bordered-example-container.three-row-layout-container-example-2",
              m(
                ThreeRowLayout,
                {
                  className: "three-row-layout sm-margin-bottom",
                  body: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Body (no header)"),
                      m("p", "content for the body goes here"),
                    ]),
                  },

                  footer: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Footer"),
                      m("p", "content for the footer goes here"),
                    ]),
                  },
                }),
             ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(".bordered-example-container.three-row-layout-container-example-2",
  m(
    ThreeRowLayout,
    {
      className: "three-row-layout sm-margin-bottom",
      body: {
        content: m(".flex.vertical", [
          m("h2.no-margin", "Body (no header)"),
          m("p", "content for the body goes here"),
        ]),
      },
      footer: {
        content: m(".flex.vertical", [
          m("h2.no-margin", "Footer"),
          m("p", "content for the footer goes here"),
        ]),
      },
   }),
)
`),

            m(".bordered-example-container.three-row-layout-container-example-3",
              m(
                ThreeRowLayout,
                {
                  className: "three-row-layout sm-margin-bottom",
                  header: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Header"),
                      m("p", "content for the header goes here"),
                    ]),
                  },
                  body: {
                    content: m(".flex.vertical", [
                      m("h2.no-margin", "Body (no footer)"),
                      m("p", "content for the body goes here"),
                    ]),
                  },
                }),
             ),

            m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(".bordered-example-container.three-row-layout-container-example-3",
  m(
    ThreeRowLayout,
    {
      className: "three-row-layout sm-margin-bottom",
      header: {
        content: m(".flex.vertical", [
          m("h2.no-margin", "Header"),
          m("p", "content for the header goes here"),
        ]),
      },
      body: {
        content: m(".flex.vertical", [
          m("h2.no-margin", "Body (no footer)"),
          m("p", "content for the body goes here"),
        ]),
      },
    }),
 ),
`),

          ]),

          // MailingListCTA
          m("code.section-title#components-mailing-list-cta", "m(MailingListCTA)"),
          m("h4", "With a simple HTML <form>"),
          m("p.mailing-list-cta-section", [
            m(
              MailingListCTA,
              {
                brief: "Enter your email here for updates, powered by a simple HTML <form>!",
                errorMsg: "Oh no, that doesn't look like a valid email address.",
                subscribeForm: {action: "/"}
              },
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  MailingListCTA,
  {
    brief: "Enter your email here for updates!",
    errorMsg: "Oh no, that doesn't look like a valid email address.",
    subscribeForm: {action: "/"}
  },
),
`), // /MailingListCTA

          m("h4", "With custom HTML <form> attributes"),
          m("p.mailing-list-cta-section", [
            m(
              MailingListCTA,
              {
                brief: "Enter your email here for updates, with a custom onsubmit (and customized button)!",
                errorMsg: "Oh no, that doesn't look like a valid email address.",
                subscribeForm: {action: undefined},
                mailleButtonAttrs: {type: "success"},
                formAttrs: {className: "your-form-class", onsubmit: () => alert("Your function triggered!")},
                mailleButtonContents: [
                  m(UXWingIcon.Rocket, {fill: "white"}),
                  m("span.xs-margin-left", "Customized!"),
                ],
              },
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  MailingListCTA,
  {
    brief: "Enter your email here for updates, with a custom onsubmit (and customized button)!",
    errorMsg: "Oh no, that doesn't look like a valid email address.",
    subscribeForm: {},
    mailleButtonAttrs: {type: "success"},
    formAttrs: {className: "your-form-class", onsubmit: () => alert("Your function triggered!")},
    mailleButtonContents: [
      m(UXWingIcon.Rocket, {fill: "white"}),
      m("span.xs-margin-left", "Customized!"),
    ],
    formAttrs: {submit : "return confirm('Do you want to submit this form?');"}
  },
),
`), // /MailingListCTA

          m("h4", "With a custom JS callback"),
          m("p.mailing-list-cta-section", [
            m(
              MailingListCTA,
              {
                brief: "Enter your email here for updates!",
                errorMsg: "Oh no, that doesn't look like a valid email address.",
                subscribeFn: (email: string) => {
                  alert(`Received call to subscribe email [${email}]`);
                }
              },
            ),
          ]),

          m("pre.code-block.full-width", `// the component(s) above correspond to the code below

m(
  MailingListCTA,
  {
    brief: "Enter your email here for updates!",
    errorMsg: "Oh no, that doesn't look like a valid email address.",
    subscribeFn: (email: string) => {
      alert(\`Received call to subscribe email [\${value}]\`);
    }
  },
),
`), // /MailingListCTA


        ], // /rhs on ThreeRowLayout
      },

    }), // /TwoColumnLayout

    // Gitlab corner (generated with https://gitlab-corners.bryce.io/)
    // tslint:disable-next-line
    m.trust(`<style>.gitlab-corner-wrapper{overflow:hidden;width:100px;height:100px;position:absolute;top:0;right:0}.gitlab-corner{position:absolute;top:-16px;right:-50px;transform:rotate(45deg);background:#548;border:44px solid #548;border-bottom:none;border-top:#548 solid 16px}.gitlab-corner svg{width:60px;height:60px;margin-bottom:-4px}.cls-1{fill:#fc6d26}.cls-2{fill:#e24329}.cls-3{fill:#fca326}.gitlab-corner:hover .cls-1{animation:cycle .6s}.gitlab-corner:hover .cls-2{animation:cycleMid .6s}.gitlab-corner:hover .cls-3{animation:cycleEnd .6s}@keyframes cycle{100%,15%,60%{fill:#fc6d26}30%,75%{fill:#e24329}45%,90%{fill:#fca326}}@keyframes cycleMid{100%,15%,60%{fill:#e24329}30%,75%{fill:#fca326}45%,90%{fill:#fc6d26}}@keyframes cycleEnd{100%,15%,60%{fill:#fca326}30%,75%{fill:#fc6d26}45%,90%{fill:#e24329}}@media (max-width:500px){.gitlab-corner:hover .cls-1,.gitlab-corner:hover .cls-2,.gitlab-corner:hover .cls-3{animation:none}.gitlab-corner .cls-1{animation:cycle .6s}.gitlab-corner .cls-2{animation:cycleMid .6s}.gitlab-corner .cls-3{animation:cycleEnd .6s}}</style><div class="gitlab-corner-wrapper"><a href="https://gitlab.com/mrman/maille" class="gitlab-corner" aria-label="View source on GitLab"><svg id="logo_art" data-name="logo art" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 586 559"><g id="g44"><path id="path46" class="cls-1" d="M461.17,301.83l-18.91-58.12L404.84,128.43a6.47,6.47,0,0,0-12.27,0L355.15,243.64H230.82L193.4,128.43a6.46,6.46,0,0,0-12.26,0L143.78,243.64l-18.91,58.19a12.88,12.88,0,0,0,4.66,14.39L293,435,456.44,316.22a12.9,12.9,0,0,0,4.73-14.39"/></g><g id="g48"><path id="path50" class="cls-2" d="M293,434.91h0l62.16-191.28H230.87L293,434.91Z"/></g><g id="g56"><path id="path58" class="cls-1" d="M293,434.91,230.82,243.63h-87L293,434.91Z"/></g><g id="g64"><path id="path66" class="cls-3" d="M143.75,243.69h0l-18.91,58.12a12.88,12.88,0,0,0,4.66,14.39L293,435,143.75,243.69Z"/></g><g id="g72"><path id="path74" class="cls-2" d="M143.78,243.69h87.11L193.4,128.49a6.47,6.47,0,0,0-12.27,0l-37.35,115.2Z"/></g><g id="g76"><path id="path78" class="cls-1" d="M293,434.91l62.16-191.28H442.3L293,434.91Z"/></g><g id="g80"><path id="path82" class="cls-3" d="M442.24,243.69h0l18.91,58.12a12.85,12.85,0,0,1-4.66,14.39L293,434.91l149.2-191.22Z"/></g><g id="g84"><path id="path86" class="cls-2" d="M442.28,243.69h-87.1l37.42-115.2a6.46,6.46,0,0,1,12.26,0l37.42,115.2Z"/></g></svg></a></div>`)

  ]),
};

// Render the demo page
m.mount(document.body, DemoApp);
