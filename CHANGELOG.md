# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.8] - 2020-02-26
### Added
- Add TextInput, InputGroup components [#71](https://gitlab.com/mrman/maille/issues/71)

### Changed
- Add `min-width: 1.25em` for all UXWingIcon components [#74](https://gitlab.com/mrman/maille/issues/74)
- Remove uses of `className` in constructors, remove constructors where possible [#73](https://gitlab.com/mrman/maille/issues/73)


## [0.1.7] - 2020-02-23
### Changed
- HOTFIX: Add missing method and field name attr to mailing-list-cta
- HOTFIX: Add note about SRI hash to landing page
- HOTFIX: Add built shared files
- HOTFIX: Update Makefile for custom-icons and new components


## [0.1.6] - 2020-02-23
### Added
- Add email call to action molecule [#75](https://gitlab.com/mrman/maille/issues/75)
- Radio button component [#4](https://gitlab.com/mrman/maille/issues/4)

### Changed
- HOTFIX: Set `word-break: keep-all` for Tag
- Add style passthrough for Tag [#72](https://gitlab.com/mrman/maille/issues/72)


## [0.1.5] - 2020-02-01
### Changed
- HOTFIX: Remove unused class members from `Card`


## [0.1.4] - 2020-02-01
### Changed
- HOTFIX: Updated CHANGELOG
- HOTFIX: Fixed GeneralIconAttrs type enabling the keys to be optional
- HOTFIX: Fix missing `size` default for maille card


## [0.1.3] - 2020-02-01
### Added
- CustomIcon component [#67](https://gitlab.com/mrman/maille/issues/67)

### Changed
- Increase attribute passthrough for generated icon classes [#54](https://gitlab.com/mrman/maille/issues/54)
- Enable customizable fill for generated UXWingIcon [#65](https://gitlab.com/mrman/maille/issues/65)
- Add blurb about CHANGELOG to landing page [#66](https://gitlab.com/mrman/maille/issues/66)
- Update import example on landing page [#68](https://gitlab.com/mrman/maille/issues/68)
- Fix misalignment of Button [#70](https://gitlab.com/mrman/maille/issues/70)
- Improve responsive styling of ButtonGroup [#69](https://gitlab.com/mrman/maille/issues/69)


## [0.1.2] - 2020-01-28
### Added
- CHANGELOG.md

### Changed
- Fix for `Button` component always using optimized render mode [#64](https://gitlab.com/mrman/maille/issues/64)
- Fixed default export line in documentation
- Fix the yarn add command used in Makefile
- Only deploy GitLab pages on version tag events

[Unreleased]: https://gitlab.com/mrman/maille/compare/v0.1.8...HEAD
[0.1.8]: https://gitlab.com/mrman/maille/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/mrman/maille/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/mrman/maille/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/mrman/maille/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/mrman/maille/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/mrman/maille/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/mrman/maille/compare/v0.1.1...v0.1.2
