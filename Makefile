.PHONY: all dev-setup install clean \
				build build-landing \
				serve lint \
				ts-build ts-watch \
				css-build \
				test ts-build-test ts-build-test-watch test-unit test-int test-e2e \
				package package-shared package-shared-basic package-shared-uxwing-icon uxwing-icon-svgs

YARN ?= yarn
ENTR ?= entr
NODE ?= node

COMPONENTS_PATH ?= src/components
TARGET_PATH ?= target
PACKAGE_PATH ?= dist
UXWING_SCRIPT_OPTS ?=

RELEASE_VERSION ?= $(shell grep version package.json | cut -d ' ' -f 4 | tr -d ,)

all: install build

print-release-version:
	@echo -n $(RELEASE_VERSION)

dev-setup:
	cp .dev/git/hooks/pre-push .git/hooks

install:
	$(YARN) install

clean:
	@echo "=> Removing output directories..."
	rm -rf dist
	rm -rf dist-landing
	rm -rf dist-test
	rm -rf public
	rm -rf target
	rm -rf src/components/**/*.shared.ts

build: build-landing ts-build css-build

build-landing:
	$(YARN) build-landing

ts-build:
	@echo "=> Perfoming Typescript build..."
	$(YARN) ts-build

css-build:
	@echo "=> Copying CSS from src to dist..."
	cd src && find . -name "*.css" -exec cp --parents {} ../dist \;

ts-watch:
	$(YARN) ts-build-watch

# Expecting the public URL for the gitlab pages to be https://<user>.gitlab.io/maille/...
build-gitlab-pages: package
	$(YARN) build-landing -- --public-url "/maille" -d public
	cp -r target public/

serve:
	$(YARN) serve

lint:
	$(YARN) lint

lint-watch:
	find src/ -name "*.ts" | $(ENTR) -rc $(YARN) lint

uxwing-icon-svgs:
	$(NODE) .dev/scripts/convert-all-ux-wing-svgs.js $(UXWING_SCRIPT_OPTS)

###########
# Testing #
###########

test: test-unit test-int test-e2e

ts-build-test:
	$(YARN) ts-build-test

ts-build-test-watch:
	$(YARN) ts-build-test --watch

test-unit: ts-build-test
	$(YARN) test-unit

test-int: ts-build-test
	$(YARN) test-int

test-e2e: ts-build-test
	$(YARN) test-e2e

test-package: test-package-shared-basic

test-package-shared-basic: ts-build-test package-shared-basic
	@echo -e "=> Testing shared basic components..."
	$(YARN) test-package-shared-basic

#############
# Packaging #
#############

package: package-shared package-complete

package-complete:
	$(YARN) build-complete-js
	$(YARN) build-fontless-js
	$(YARN) build-complete-css

# uxwing-icon is not included in the components list, they're handled specially by package-shared-uxwing-icon
BASIC_COMPONENTS ?= alert button button-group card mailing-list-cta custom-icon divider mailing-list-cta nav-bar radio-input radio-input-button-group radio-input-group switch tag three-row-layout tool-tip two-column-layout text-input text-input-group

package-shared: package-shared-basic package-shared-uxwing-icon package-shared-custom-icon

package-shared-basic:
	@echo -e "=> Packaging shared basic components: [${BASIC_COMPONENTS}]..."
	@for component in $(BASIC_COMPONENTS) ; do \
		sed '1i\// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY\n' src/components/$$component/component.ts > src/components/$$component/component.shared.ts ; \
		sed -i 's#^\(.*\)// +standalone#//\1// +standalone#g' src/components/$$component/component.shared.ts ; \
		sed -i 's#^//\(.*\)// +shared#\1#g' src/components/$$component/component.shared.ts ; \
		$(YARN) parcel build \
			$(COMPONENTS_PATH)/$$component/component.shared.ts \
			--global MAILLE_`echo $$component | tr a-z A-Z | tr - _` \
			-d $(TARGET_PATH) \
			-o "$$component.shared.min.js" ; \
		$(YARN) parcel build \
			$(COMPONENTS_PATH)/$$component/style.css \
			--global MAILLE_`echo $$component | tr a-z A-Z | tr - _` \
			-d $(TARGET_PATH) \
			-o "$$component.shared.min.css" ; \
	done

package-shared-uxwing-icon:
	@echo -e "=> Packaging all generated uxwing-icon component..."
	@for path in `ls src/components/uxwing-icon/generated/*.ts | grep -v "shared"` ; do \
		echo -e "==> Packaging icon in [$$path]" ; \
		[[ $$path =~ "shared" ]] && continue ; \
		sed '1i\// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY\n' $$path > `echo $$path | sed 's/.ts$$/.shared.ts/g'`; \
		sed -i 's#^\(.*\)// +standalone#//\1// +standalone#g' `echo $$path | sed 's/.ts$$/.shared.ts/g'` ; \
		sed -i 's#^//\(.*\)// +shared#\1#g' `echo $$path | sed 's/.ts$$/.shared.ts/g'` ; \
		$(YARN) parcel build \
			`echo $$path | sed 's/.ts$$/.shared.ts/g'` \
			--global MAILLE_UX_WING_ICON_`basename $$path | sed 's/.shared.ts$$//g' | tr a-z A-Z | tr - _` \
			-d $(TARGET_PATH) \
			-o uxwing-icon-`basename $$path | sed 's/.ts$$/.shared.min.ts/g'` ; \
	done

package-shared-custom-icon:
	@echo -e "=> Packaging all generated custom-icon component..."
	@for path in `ls src/components/custom-icon/*.ts | grep -v "shared"` ; do \
		echo -e "==> Packaging icon in [$$path]" ; \
		[[ $$path =~ "shared" ]] && continue ; \
		sed '1i\// THIS FILE WAS AUTO-GENERATED FOR PACKAGING, DO NOT MODIFY\n' $$path > `echo $$path | sed 's/.ts$$/.shared.ts/g'`; \
		sed -i 's#^\(.*\)// +standalone#//\1// +standalone#g' `echo $$path | sed 's/.ts$$/.shared.ts/g'` ; \
		sed -i 's#^//\(.*\)// +shared#\1#g' `echo $$path | sed 's/.ts$$/.shared.ts/g'` ; \
		$(YARN) parcel build \
			`echo $$path | sed 's/.ts$$/.shared.ts/g'` \
			--global MAILLE_UX_WING_ICON_`basename $$path | sed 's/.shared.ts$$//g' | tr a-z A-Z | tr - _` \
			-d $(TARGET_PATH) \
			-o custom-icon-`basename $$path | sed 's/.ts$$/.shared.min.ts/g'` ; \
	done

package-npm: ts-build
	cp package.json dist
	cp README.md dist

##############
# Publishing #
##############

publish: lint build test package-npm
	$(YARN) publish $(PACKAGE_PATH)
