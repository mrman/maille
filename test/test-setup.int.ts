import { JSDOM } from "jsdom";
import { Test } from "tape";
const test = require("tape");

const jsdom = require("jsdom");
const DEFAULT_DOM: any = new jsdom.JSDOM("", {
  // So we can get `requestAnimationFrame`
  pretendToBeVisual: true,
});

/**
 * Set up a given JSDOM dom object for use with mithril
 *
 * @param {jsdom.JSDOM} [dom] - the JSDOM, an empty DOM if not provided
 * @returns {jsdom.JSDOM}
 */
function setupMithrilDOM(dom: JSDOM = DEFAULT_DOM) {
  // Fill in the globals Mithril needs to operate. Also, the first two are often
  // useful to have just in tests.
  (global as any).window = dom.window;
  (global as any).document = dom.window.document;
  (global as any).requestAnimationFrame = dom.window.requestAnimationFrame;

  // Mocking for Mithil components
  (global as any).window = require("mithril/test-utils/browserMock.js")();

  return {dom, global};
}

// Require Mithril to make sure it loads properly.
require("mithril");
const mq = require("mithril-query")

// Helper function that when executed runs a test for cleaning up the DOM created
const cleanupDOM = (dom: JSDOM) => test("cleaning up JSDOM", (t: Test) => {
  dom.window.close();
  t.end();
});

export {
  // Module re-exports
  Test,
  test,
  mq,

  // utilities
  cleanupDOM,
  setupMithrilDOM,
}
