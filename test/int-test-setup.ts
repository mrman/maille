import { JSDOM, default as jsdom } from "jsdom";
import { Test, default as test } from "tape";
import { cleanupDOM } from "./utils";

const DEFAULT_DOM: any = new jsdom.JSDOM("", {
  // So we can get `requestAnimationFrame`
  pretendToBeVisual: true,
});

// Fill in the globals Mithril needs to operate. Also, the first two are often
// useful to have just in tests.
(global as any).window = DEFAULT_DOM.window;
(global as any).document = DEFAULT_DOM.window.document;
(global as any).requestAnimationFrame = DEFAULT_DOM.window.requestAnimationFrame;

// Require Mithril to make sure it loads properly.
require("mithril");
const mq = require("mithril-query");

// Mocking for Mithil components
(global as any).window = require("mithril/test-utils/browserMock.js")();

export {
  Test,
  test,
  mq,
  cleanupDOM,

  DEFAULT_DOM,
}
