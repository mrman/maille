import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Switch from "../../src/components/switch/component";

test("Switch can be created", (t: Test) => {
    t.assert(new Switch(), "created Switch is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
