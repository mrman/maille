import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "two-column-layout",
  PATHS.components.twoColumnLayout.minified,
  `
m(
  MAILLE_TWO_COLUMN_LAYOUT.default,
  {
    className: "two-column-layout sm-margin-bottom",
    lhs: {
      container: {attrs: {style: "flex-basis: 30%"}},
      content: m(".flex.vertical", m("p", "lhs content")),
    },
    rhs: {
      content: m(".flex.vertical", m("p", "rhs content")),
    },
  }
)
`,
  (t: Test, dom: JSDOM) => {
    const layoutNode = dom.window.document.querySelector(".maille.maille-two-column-layout");
    if (!layoutNode) { throw new Error("Two column layout node is missing"); };

    const lhsNode = layoutNode.querySelector(".maille-two-column-layout-lhs-container");
    t.assert(lhsNode && lhsNode.textContent === "lhs content", "LHS is present and correct");

    const rhsNode = layoutNode.querySelector(".maille-two-column-layout-rhs-container");
    t.assert(rhsNode && rhsNode.textContent === "rhs content", "RHS is present and correct");
  },
);
