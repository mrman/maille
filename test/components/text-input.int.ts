import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import TextInput from "../../src/components/text-input/component";

test("TextInput can be created", (t: Test) => {
    t.assert(new TextInput(), "created TextInput is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
