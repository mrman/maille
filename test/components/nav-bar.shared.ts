import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "nav-bar",
  PATHS.components.navBar.minified,
  `
m(
  MAILLE_NAV_BAR.default,
  {
    className: "navbar sm-margin-bottom",
    lhs: m("div", "Basic NavBar"),
    center: [
      m(".nav-bar-item", m("span", "Center-Nav-1")),
      m(".nav-bar-item", m("span", "Center-Nav-2")),
    ],
    rhs: [
      m(".nav-bar-item", m("span", "RHS-Nav-1")),
      m(".nav-bar-item", m("span", "RHS-Nav-2")),
    ],
  }),
`,
  (t: Test, dom: JSDOM) => {
    const navBarNode = dom.window.document.querySelector(".maille.maille-nav-bar");
    if (!navBarNode) { throw new Error("Nav Bar node is missing"); };

    const navItemCount = navBarNode.querySelectorAll(".nav-bar-item").length;
    t.equals(navItemCount, 4, "4 navs items are present")
  },
);
