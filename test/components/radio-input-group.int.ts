import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import RadioInputGroup from "../../src/components/radio-input-group/component";

test("RadioInputGroup can be created", (t: Test) => {
    t.assert(new RadioInputGroup(), "created RadioInputGroup is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
