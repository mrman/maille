import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "alert",
  PATHS.components.alert.minified,
  `
m(
  MAILLE_ALERT.default,
  {title: "Info", type: "info", description: "Alert description"},
  m("p", "A detailed explanation, anything can go here"),
)
`,
  (t: Test, dom: JSDOM) => {
    const alertNode = dom.window.document.querySelector(".maille.maille-alert");
    if (!alertNode) { throw new Error("Alert node is missing"); };

    const titleNode = alertNode.querySelector(".maille-alert-title");
    t.assert(titleNode && titleNode.textContent === "Info", "title is correct");

    const descriptionNode = alertNode.querySelector(".maille-alert-description");
    t.assert(descriptionNode && descriptionNode.textContent === "Alert description", "description is correct");
  },
);
