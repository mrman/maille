import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "tag",
  PATHS.components.tag.minified,
  `
m("p", [
  m(MAILLE_TAG.default, {text: "Mini", rounded: true, size: "mini"}),
  m(MAILLE_TAG.default, {text: "Small", rounded: true, size: "sm"}),
  m(MAILLE_TAG.default, {text: "Medium (default)", rounded: true}),
  m(MAILLE_TAG.default, {text: "Large", rounded: true, size: "lg"}),
  m(MAILLE_TAG.default, {text: "XLarge", rounded: true, size: "xlg"}),
]),
`,
  (t: Test, dom: JSDOM) => {
    const tagNodes = dom.window.document.querySelectorAll(".maille.maille-tag");
    if (!tagNodes) { throw new Error("Tag node is missing"); };

    t.equals(tagNodes.length, 5, "content is correct");
  },
);
