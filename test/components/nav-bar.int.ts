import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import NavBar from "../../src/components/nav-bar/component";

test("NavBar can be created", (t: Test) => {
    t.assert(new NavBar(), "created NavBar is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
