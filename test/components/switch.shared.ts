import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "switch",
  PATHS.components.switch.minified,
  `
m(
  MAILLE_SWITCH.default,
  {
      className: "xs-margin-right",
      on: false,
      onclick: function(state) { alert('Switch state changed'); },
  },
  "regular switch",
),
`,
  (t: Test, dom: JSDOM) => {
    const switchNode = dom.window.document.querySelector(".maille.maille-switch");
    if (!switchNode) { throw new Error("Switch node is missing"); };

    t.equals(switchNode.textContent, "regular switch", "content is correct");
  },
);
