import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "divider",
  PATHS.components.divider.minified,
  `
m(
  MAILLE_DIVIDER.default,
  m("span.xs-padding-horiz", {style: {backgroundColor: "white"}}, "child element"),
),
`,
  (t: Test, dom: JSDOM) => {
    const dividerNode = dom.window.document.querySelector(".maille.maille-divider");
    if (!dividerNode) { throw new Error("Divider node is missing"); };

    const contentNode = dividerNode.querySelector("span");
    t.assert(contentNode && contentNode.textContent === "child element", "content is correct");
  },
);
