import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import ToolTip from "../../src/components/tool-tip/component";

test("ToolTip can be created", (t: Test) => {
    t.assert(new ToolTip(), "created ToolTip is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
