import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "three-row-layout",
  PATHS.components.threeRowLayout.minified,
  `
m(
  MAILLE_THREE_ROW_LAYOUT.default,
  {
    className: "three-row-layout sm-margin-bottom",
    header: {
      content: m(".flex.vertical", m("p", "header content")),
    },
    body: {
      content: m(".flex.vertical", m("p", "body content")),
    },
    footer: {
      content: m(".flex.vertical", m("p", "footer content")),
    },
  }
)
,
`,
  (t: Test, dom: JSDOM) => {
    const layoutNode = dom.window.document.querySelector(".maille.maille-three-row-layout");
    if (!layoutNode) { throw new Error("Three row layout node is missing"); };

    const headerNode = layoutNode.querySelector(".maille-three-row-layout-header-container");
    t.assert(headerNode && headerNode.textContent === "header content", "header is present & correct");

    const bodyNode = layoutNode.querySelector(".maille-three-row-layout-body-container");
    t.assert(bodyNode && bodyNode.textContent === "body content", "body is present & correct");

    const footerNode = layoutNode.querySelector(".maille-three-row-layout-footer-container");
    t.assert(footerNode && footerNode.textContent === "footer content", "footer is present & correct");
  },
);
