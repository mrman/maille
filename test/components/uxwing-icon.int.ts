import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import * as UXWingIcon from "../../src/components/uxwing-icon";

test("All UXWing Icons can be created", (t: Test) => {
    Object
        .keys(UXWingIcon)
        .map((k) => t.assert(new (UXWingIcon as any)[k](), `UXWingIcon [${k}] is non-null after creation`));
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
