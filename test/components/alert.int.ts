import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Alert from "../../src/components/alert/component";

test("Alert can be created", (t: Test) => {
    t.assert(new Alert(), "created Alert is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
