import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "button",
  PATHS.components.button.minified,
  `
m(
  MAILLE_BUTTON.default,
  {type: "primary", rounded: true, outlined: true},
  "Test Button",
)
`,
  (t: Test, dom: JSDOM) => {
    const buttonNode = dom.window.document.querySelector(".maille.maille-button");
    if (!buttonNode) { throw new Error("Button node is missing"); };

    const contentNode = buttonNode.querySelector("span");
    t.assert(contentNode && contentNode.textContent === "Test Button", "content is correct");
  },
);
