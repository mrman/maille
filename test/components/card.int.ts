import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Card from "../../src/components/divider/component";

test("Card can be created", (t: Test) => {
    t.assert(new Card(), "created Card is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
