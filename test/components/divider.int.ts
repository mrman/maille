import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Divider from "../../src/components/divider/component";

test("Divider can be created", (t: Test) => {
    t.assert(new Divider(), "created Divider is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
