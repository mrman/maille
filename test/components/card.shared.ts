import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "card",
  PATHS.components.card.minified,
  `
m(
  MAILLE_CARD.default,
  {rounded: true, bordered: true, size: "sm"},
  [
    m(".lg-text.sm-pad", "Small Size"),
    m(".maille-card-body.sm-pad", [
      m("p", "This card is small size"),
    ]),
  ],
)
`,
  (t: Test, dom: JSDOM) => {
    const cardNode = dom.window.document.querySelector(".maille.maille-card");
    if (!cardNode) { throw new Error("Card node is missing"); };

    const contentNode = cardNode.querySelector("p");
    t.assert(contentNode && contentNode.textContent === "This card is small size", "content is correct");
  },
);
