import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "button-group",
  [PATHS.components.button.minified, PATHS.components.buttonGroup.minified],
  `
m(
  MAILLE_BUTTON_GROUP.default,
  [
    m(MAILLE_BUTTON.default, {type: "primary"}, "Primary"),
    m(MAILLE_BUTTON.default, {type: "success"}, "Success"),
    m(MAILLE_BUTTON.default, {type: "warning"}, "Warning"),
    m(MAILLE_BUTTON.default, {type: "error"}, "Error"),
    m(MAILLE_BUTTON.default, {type: "info"}, "Info"),
  ]
),
`,
  (t: Test, dom: JSDOM) => {
    const buttonGroupNode = dom.window.document.querySelector(".maille.maille-button-group");
    if (!buttonGroupNode) { throw new Error("Button Group node is missing"); };

    const buttonNodeCount = buttonGroupNode.querySelectorAll(".maille.maille-button").length;
    t.equals(buttonNodeCount, 5, "5 buttons are present")
  },
);
