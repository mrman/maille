import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import RadioInput from "../../src/components/radio-input/component";

test("RadioInput can be created", (t: Test) => {
    t.assert(new RadioInput(), "created RadioInput is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
