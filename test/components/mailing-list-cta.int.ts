import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import MailingListCTA from "../../src/components/mailing-list-cta/component";

test("MailingListCTA can be created", (t: Test) => {
    t.assert(new MailingListCTA(), "created MailingListCTA is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
