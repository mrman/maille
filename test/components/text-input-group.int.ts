import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import TextInputGroup from "../../src/components/text-input-group/component";

test("TextInputGroup can be created", (t: Test) => {
    t.assert(new TextInputGroup(), "created TextInputGroup is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
