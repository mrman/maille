import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import ThreeRowLayout from "../../src/components/three-row-layout/component";

test("ThreeRowLayout can be created", (t: Test) => {
    t.assert(new ThreeRowLayout(), "created ThreeRowLayout is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
