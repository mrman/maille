import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Button from "../../src/components/button/component";

test("Button can be created", (t: Test) => {
    t.assert(new Button(), "created Button is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
