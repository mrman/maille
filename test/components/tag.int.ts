import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import Tag from "../../src/components/tag/component";

test("Tag can be created", (t: Test) => {
    t.assert(new Tag(), "created Tag is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
