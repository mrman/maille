import { DEFAULT_DOM, cleanupDOM, test, Test, mq } from "../int-test-setup";

import TwoColumnLayout from "../../src/components/two-column-layout/component";

test("TwoColumnLayout can be created", (t: Test) => {
    t.assert(new TwoColumnLayout(), "created TwoColumnLayout is non-null");
    t.end();
});

// Test to cleanup DOM
cleanupDOM(DEFAULT_DOM);
