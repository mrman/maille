import { Test, default as test } from "tape";
import { JSDOM } from "jsdom";
import { PATHS, runComponentSharedPackageTest } from "../utils";

runComponentSharedPackageTest(
  "tool-tip",
  PATHS.components.toolTip.minified,
  `
m(
  MAILLE_TOOL_TIP.default,
  {
    activation: "hover",
    body: m("p.no-margin", "tooltip text"),
    position: "top",
    title: "Bottom",
  },
  m("p.sm-margin-horiz", "Hover here"),
)
,
`,
  (t: Test, dom: JSDOM) => {
    const toolTipNode = dom.window.document.querySelector(".maille.maille-tool-tip");
    if (!toolTipNode) { throw new Error("Tool tip node is missing"); };

    // Ensure the tooltip is not visible
    t.not(toolTipNode.querySelector(".maille.maille-tooltip-inner"), "tooltip inner is present");
  },
);
