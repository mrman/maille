import { JSDOM, VirtualConsole, default as jsdom } from "jsdom";
import { Test, default as test } from "tape";
import { waitMs } from "../src/util";

// Helper function that when executed runs a test for cleaning up the DOM created
export const cleanupDOM = (dom: JSDOM) => test("cleaning up JSDOM", (t: Test) => {
  dom.window.close();
  t.end();
});

// Paths to resources (relative to project root)
export const PATHS = {
  vendor: {
    mithril: {minified: "node_modules/mithril/mithril.min.js"},
  },
  components: {
    alert: {minified: "target/alert.shared.min.js"},
    button: {minified: "target/button.shared.min.js"},
    buttonGroup: {minified: "target/button-group.shared.min.js"},
    navBar: {minified: "target/nav-bar.shared.min.js"},
    divider: {minified: "target/divider.shared.min.js"},
    card: {minified: "target/card.shared.min.js"},
    switch: {minified: "target/switch.shared.min.js"},
    tag: {minified: "target/tag.shared.min.js"},
    threeRowLayout: {minified: "target/three-row-layout.shared.min.js"},
    toolTip: {minified: "target/tool-tip.shared.min.js"},
    twoColumnLayout: {minified: "target/two-column-layout.shared.min.js"},
  }
}

// Default JSDOM options
export const JSDOM_OPTIONS: jsdom.Options = {
  resources: "usable",
  runScripts: "dangerously",
  pretendToBeVisual: true,
};

/**
 * This function generates a tape test that runs a single shared packaging (where mithril is included separately from a small component) test
 *
 * @param {string} name - Name of the component
 * @param {string} jsPath - Path to the minified JS for the component
 * @param {string} renderArgumentRawJs - The text (javascript) that will be spliced into a HTML page, as the second argument to m.render. (ex. `m.render(document.body, <renderArgumentRawJS>);`)
 * @param {Function<void | Promise<void>>} check - The check that will be performed after the tests run, which will be provided a tape test function
 */
export function runComponentSharedPackageTest(
  name: string,
  jsPath: string | string[],
  renderArgumentRawJS: string,
  check: (test: Test, dom: JSDOM) => void | Promise<void>,
) {
  test(`[${name}] renders properly in shared package mode`, (t: Test) => {
    // Set up virtual console to catch errors
    const virtualConsole = new VirtualConsole();
    virtualConsole.on("error", (err: Error) => t.fail(`JS Error: ${err}`));
    virtualConsole.on("log", (str: any) => console.log(str));
    virtualConsole.on("jsdomError", (err: Error) => t.fail(`JSDOM Error: ${err}`))

    const jsPaths = typeof jsPath === 'string' ?  [jsPath] : jsPath;
    const scriptTags = jsPaths.map(path => `<script src="file://${path}"></script>`).join("\n");

    const dom = new JSDOM(
      `
<!DOCTYPE html>
<html>
<body>
<script src="file://${PATHS.vendor.mithril.minified}"></script>
${scriptTags}
<script>
// Render the component
m.render(document.body, ${renderArgumentRawJS});
</script>
</body>
</html>
`,
      {...JSDOM_OPTIONS, virtualConsole},
    );

    // Wait for DOM & scripts to load
    waitMs(100)
    // Run the provided check function
      .then(() => check(t, dom))
    // Clean up test
      .then(() => cleanupDOM(dom))
      .then(() => t.end())
      .catch((err: Error) => {
        cleanupDOM(dom);
        t.end(err);
      })
  });
}
