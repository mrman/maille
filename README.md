# Maille: A component library for Mithril #

Maille is a component library built for [Mithril JS](https://mithriljs.org).

Learn more about Maille (as well as see a site built with Maille) @ [`maille`'s homepage](https://mrman.gitlab.io/maille).

## Quickstart ##

Maille can be used with modern build tools and without, usually just a `<script>` or `import` away.

## With ParcelJS ##

The examples below assume you are using [`ParcelJS`](https://parceljs.org), or any sufficiently powerful and convenient build toooling.

`index.ts`:
```ts
import m from "mithril";

import "maille/components/alert/style.css";
import Alert from "maille/components/alert";

const App = {
  view: (vnode) => {
    return   m("main", [
      m("h1", "New Maille Project"),
      m("h2", "Thanks for using Maille!"),
      m(
        Alert,
        {title: "Default", description: "Alert description"},
        m("p", "A detailed explanation, anything can go here"),
      ),
    ]);
  }
}
```

## With Vanilla JS (no build tooling) ##

Maille can be used without build tooling -- no compilation step needed, just include the styles and javascript:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Import all Maille styles at once -->
    <link rel="stylesheet" href="https://mrman.gitlab.io/maille/target/maille.min.css"/>

    <!-- You can also import maille styles one by one, here are some examples -->
    <!-- <link rel="stylesheet" href="https://mrman.gitlab.io/maille/target/button.shared.min.css"/> -->
  </head>
  <body>
    <!-- All in one (includes Mithril, Fonts, etc) -->
    <script src="https://mrman.gitlab.io/maille/target/maille.min.js"></script>

    <!-- If you'd prefer the lighter SVG font-less build, which contains Mithril -->
    <!-- <script src="https://mrman.gitlab.io/maille/target/maille.fontless.min.js"></script> -->

    <!-- Your code goes here -->
    <script type="text/javascript">
      // m is assumed to be mithril
      // maille.min.js exports a global called MAILLE
      m.mount(
        document.body,
        m("div", [
          m("h1", "Your Web Site"),
          m(MAILLE.Button, {rounded: true}, "Rounded"),
        ]),
      );
    </script>
  </body>
</html>
```

For more information on how can use Maille, check out [`maille`'s homepage](https://mrman.gitlab.io/maille).

## Development ##

To get started developing `maille`, to the following:

0. Clone this repository
1. `make` (installs required packages, performs a build)
2. `make serve` (serves the landing page with [`parcel`](https://parceljs.org)

The landing page should serve as an example for every component that is in Maille.

Some more useful `Makefile` targets:

- `make ts-build` (builds the project into `dist`)
- `make ts-watch` (build
