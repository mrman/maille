# Styling #

`maille` components are styled by using [CSS Variables (AKA CSS custom properties)](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties).

## Variables ##

| Variable                             | Default | Description                                                              |
|--------------------------------------|---------|--------------------------------------------------------------------------|
| `--maille-primary-color-bg`           | `gray` | Primary background color (ex. used on primary buttons)                   |
| `--maille-primary-color-fg`           | `white` | Primary foreground color                                                 |
| `--maille-primary-color-fg-constrast` | `white` | Primary foreground color to be used for constrast against the background |

