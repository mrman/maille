# Floating LHS column #

If you want to create a two column layout but with a *fixed* left hand side (for showing navigation, for example), you can use the following recipe:

Javascript:
```js
    m(TwoColumnLayout, {
      className: "container",

      ////////////////////
      // Left Hand Side //
      ////////////////////
      lhs: {
        container: {attrs: {id: "container-lhs"}},
        content: [
          m(".container-lhs-nav", [
            m("h2", "LHS Title"),
          ]),
        ],
      },

      /////////////////////
      // Right Hand Side //
      /////////////////////
      rhs: {
        content: [ ... ]
      },
     });
```

CSS:
```css
#container-lhs {
  flex-basis: 20%;
  flex-shrink: 0;
}

#container-lhs .maille-lhs-nav {
  text-align: center;
  min-height: 100%;
  width: 20%;
  overflow-y: auto;
  position: fixed;
}
```
